<?php

//returns the maximum in a 2d array / matrix
function getMatrixMax($matrix)
{
	$result = 0;

	$dimx = sizeof($matrix[0]);
	$dimy = sizeof($matrix);
	for($x = 0; $x < $dimx; $x++)
	{
		for($y = 0; $y < $dimy; $y++)
		{
			//echo "S: ".$result." V: ".$matrix[$y][$x]."\n";
			if($result < $matrix[$y][$x])
				$result = $matrix[$y][$x];
		}
	}

	return $result;
}

//divides alle values in a matrix by a number
function divMatrixByVal($matrix, $val)
{
	$dimx = sizeof($matrix[0]);
	$dimy = sizeof($matrix);
	for($x = 0; $x < $dimx; $x++)
	{
		for($y = 0; $y < $dimy; $y++)
		{
			$matrix[$y][$x] = $matrix[$y][$x]/$val;
		}
	}

	return $matrix;
}

//multiplies two matrices
function multiplyMatrix($mat1, $mat2)
{
	$m1 = sizeof($mat1);
	$n1 = sizeof($mat1[0]);
	$m2 = sizeof($mat2);
	$n2 = sizeof($mat2[0]);
	
	if($n1 != $m2)
	{
		//echo "Dimension mismatch!";
		return 0;
	}	
	for($i = 0; $i < $m1; $i++)
	{
		for($j = 0; $j < $n2; $j++)
		{
			//echo "\nc_".$i."_".$j." = ";
			$sum = 0;
			for($k = 0; $k < $n1; $k++)
			{
				$sum = $sum + $mat1[$i][$k]*$mat2[$k][$j];
				//echo $mat1[$i][$k]." * ".$mat2[$k][$j]." + \n";
			}
			
			if(abs($sum) < 0.000000001)
				$result[$i][$j] = 0; //numerical correction
			else
				$result[$i][$j] = $sum;
		}
	}
	
	return $result;
}


//calculates determinant of a 3x3 matrix
function calcDeterminant3x3($vMatrix)		//up to dim 3x3 ????
{
	$iDim1 = sizeof($vMatrix);
	$iDim2 = sizeof($vMatrix[0]);

	$fSum = 0;

	$k = 0;
	$l = 0;

	//echo "Dim1: ".$iDim1." Dim2: ".$iDim2."\n";

	for($i = 0; $i < $iDim1; $i++)
	{
		
		$fTMP = 1;

		$k = $i;

		for($j = 0; $j < $iDim2; $j++)
		{
			

			if($k == $iDim1)
				$k = 0;
			if($l == $iDim2)
				$l = 0;
	


			$fTMP = $fTMP * $vMatrix[$k][$l];

			$k = $k + 1;
			$l = $l + 1;
		}
		echo $fSum." + ";	
		echo $fTMP." = ";

		$fSum = $fSum + $fTMP;
		echo $fSum."\n";
	}

	$k = 0;
	$l = 0;

		echo " - ";

	for($i = 0; $i < $iDim1; $i++)
	{
		$fTMP = 1;

		$k = $i;
		for($j = 0; $j < $iDim2; $j++)
		{
			
			if($k < 0)
				$k = $iDim1 - 1;
			if($l == $iDim2)
				$l = 0;




			$fTMP = $fTMP * $vMatrix[$k][$l];

			$k = $k - 1;
			$l = $l + 1;

		}
		echo $fSum." - ";
		echo $fTMP." = ";
		$fSum = $fSum - $fTMP;
		echo $fSum."\n";
	}


	return $fSum;
}


//calculates determinant of n x n matrix
function calcDeterminantRL($vMatrix)  //recursive laplace formula (nxn mat)
{
	$iDim = sizeof($vMatrix);

	if($iDim == 2)
		return ($vMatrix[0][0] * $vMatrix[0][0] - $vMatrix[0][1] * $vMatrix[1][0]);
	else
	{
		//create subdeterminant

		$fDet = 0;

		for($i = 0; $i < $iDim; $i++)
		{
			$vTMP = array_fill(0,($iDim - 1),array_fill(0,($iDim - 1),0));

			$ck = 0;

			for($k = 1; $k < $iDim; $k++)
			{
				for($l = 0; $l < $iDim; $l++)
				{
					if($l == $i)
					{
						$ck = 1;
						continue;
					}
					if($ck == 1)
						$vTMP[$k-1][($l-1)] = $vMatrix[$k][$l];
					else
						$vTMP[$k-1][$l] = $vMatrix[$k][$l];
				}
			}

			$fDet = $fDet + (-1)**$i * $vMatrix[0][$i] * calcDeterminantRL($vTMP);

			print_r($vTMP);

		}

		return $fDet;
	}
}

//calculates determinant of diagonal matrix
function calcDeterminantDiag($vMatrix)  //diagonal matrices
{
	$iDim = sizeof($vMatrix);
	$fDet = 1;
	
	for($i = 0; $i < $iDim; $i++)
	{
		$fDet = $fDet * $vMatrix[$i][$i];
	}
	
	return $fDet;
}

//inverts a diagonal matrix
function invertDiag($vMatrix)
{
	//print_r($vMatrix);
	
	//step one create 1-diagonal
	$iDim = sizeof($vMatrix);
	
	$vUnity = unityMatrix($iDim);
	
	for($i = 0; $i < $iDim; $i++)
	{
		$fDivisor = $vMatrix[$i][$i];
		for($j = 0; $j < $iDim; $j++)
		{
			$vMatrix[$i][$j] = $vMatrix[$i][$j] / $fDivisor;
			$vUnity[$i][$j] = $vUnity[$i][$j] / $fDivisor;
		}
	}
	
	//multiply i+1/k to match i/k and add/subtract
	
	//print_r($vMatrix);
	
	for($i = 1; $i < $iDim; $i++)
	{
		echo "\ni: ".$i." j: ".$i."\n\n";
			
		for($l = 0; $l < $i; $l++)
		{	
			
			$fDivisor = $vMatrix[$l][$i];
			echo "\ndivisor m[".$l."][".$i."]: ".$fDivisor."\n\n";

			for($k = $i; $k < $iDim; $k++)
			{
					//echo "current m[".($i-1)."][".$k."]:\n \tm[".$i."][".$k."] * div - m[".($i-1)."][".$k."]";
					//echo "\t-> \t ".$vMatrix[($i-1)][$k]." = ".$vMatrix[$i][$k]." * ".$fDivisor." - ".$vMatrix[($i-1)][$k];
					$vMatrix[($l)][$k] = $vMatrix[($i)][$k] * $fDivisor - $vMatrix[($l)][$k];
					$vUnity[($l)][$k] = $vUnity[($i)][$k] * $fDivisor - $vUnity[($l)][$k];
					//echo " = ".$vMatrix[($l)][$k]."\n";
			}
		}
	}
	
	//print_r($vMatrix);
	//print_r($vUnity);
	
	return $vUnity;
}

//transposes a matrix
function transposeMatrix($matrix)
{
	$dimx = sizeof($matrix);
	$dimy = sizeof($matrix[0]);
	
	//$result = array_fill(0,sizeof($matrix),array_fill(0,sizeof($matrix[0],0)));
	
	
	
	for($x = 0; $x < $dimx; $x++)
	{
		for($y = 0; $y < $dimy; $y++)
		{
			$result[$y][$x] = $matrix[$x][$y];
		}
	}
	return $result;
}

//calculates the adjunct matrix of a matrix
function adjungateMatrix($matrix)
{
	$dimx = sizeof($matrix);
	$dimy = sizeof($matrix[0]);
	
	//$result = array_fill(0,sizeof($matrix),array_fill(0,sizeof($matrix[0],0)));
	
	
	
	for($x = 0; $x < $dimx; $x++)
	{
		for($y = 0; $y < $dimy; $y++)
		{
			$result[$y][$x] = $matrix[$x][$y];
		}
	}
	return $result;
}


//multiplies all values of a matrix by a number 
function scaleMatrix($matrix,$factor)
{
	$dimy = sizeof($matrix);
	$dimx = sizeof($matrix[0]);
	for($x = 0; $x < $dimx; $x++)
	{
		for($y = 0; $y < $dimy; $y++)
		{
			$matrix[$y][$x] = $matrix[$y][$x]*$factor;
		}
	}	
	return $matrix;
}

//too complicated for a simple helper function, needs computer algbra system
function calcEigenValues()
{}


//creates a unity matrix of dimension $dim
function unityMatrix($dim)
{
	$result = array_fill(0,$dim,array_fill(0,$dim,0));

	for($x = 0; $x < $dim; $x++)
	{
		$result[$x][$x] = 1;		
	}	


	return $result;
}


//subtracts two matrices
function subtractMatrix($mat1,$mat2)
{
	$dimx = sizeof($mat1[0]);
	$dimy = sizeof($mat1);
	
	$result = array_fill(0,sizeof($mat1),array_fill(0,sizeof($mat1[0],0)));
	
	for($x = 0; $x < $dimx; $x++)
	{
		for($y = 0; $y < $dimy; $y++)
		{
			$result[$y][$x] = $mat1[$y][$x] - $mat2[$y][$x];
		}
	}	
	return $result;
}


//for statistics, especially correlation analysis
//shifts values if they are scewed
function centerMatrix($matrix)
{
	$dimx = sizeof($matrix);
	$dimy = sizeof($matrix[0]);
	
	$one_mat = array_fill(0,$dimy,array_fill(0,$dimy,1));

	$mult_mat = multiplyMatrix($one_mat, $matrix);
	
	$center_mat = scaleMatrix($mult_mat,$dimy);
	
	$result = subtractMatrix($matrix, $center_mat);
	
	return $result;
}


//prints out matrix
function printMatrix($matrix)
{
	$dim_i = sizeof($matrix);
	$dim_j = sizeof($matrix[0]);
	
	$matstring = "";
	
	for($i = 0; $i < $dim_i; $i++)
	{
		for($j = 0; $j < $dim_j; $j++)
		{
			$matstring.= $matrix[$i][$j]."\t";
		}
		$matstring.= "\n";
	}
	
	return $matstring;
}

//QR Givens Matrix decomposition (for eigenvalue calculation)
//did not work quite well
function QR_G_Matrix_TEST($i, $j, $vMatrix)
{
	$iDim1 = sizeof($vMatrix);
	$iDim2 = sizeof($vMatrix[0]);
	
	$sign = 0;
	
	if($vMatrix[$i][$j] > 1)
		$sign = 1;
	elseif($vMatrix[$i][$j] < 1)
		$sign = -1;
	
	//$i = zeile
	//$j = spalte
	
	$p = $sign*sqrt($vMatrix[$j][$j]**2 + $vMatrix[$i][$j]**2);
	$c = $vMatrix[$j][$j] / $p;
	$s = $vMatrix[$i][$j] / $p;
	
	echo "p: ".$p." c: ".$c." s: ".$s."\n";
	
	$vTMP = array_fill(0, $iDim1, 0);
	
	$vResult = array_fill(0, $iDim1, $vTMP);
	
	for($k = 0; $k < $iDim1; $k++)
	{
		for($l = 0; $l < $iDim1; $l++)
		{/*
			if(($j == $i && $l == $i) || ($j == $k && $l == $k))
				$vResult[$k][$l] = $c;
			
			if($j == $i && $l == $k)
				$vResult[$k][$l] = $s;
			
			if($j == $k && $l == $i)
				$vResult[$k][$l] = $s*-1;
			
			if($j == $l && $j != $i && $j != $k)
				$vResult[$k][$l] = 1;
*/
			if($k == $l)
				$vResult[$k][$l] = 1;
			if($k == $l && ($k == $i || $k == $j))
				$vResult[$k][$l] = $c;
			
			if($k == $j && $l == $i)
				$vResult[$k][$l] = $s;
			if($k == $i && $l == $j)
				$vResult[$k][$l] = $s*-1;
		}
	}

	return $vResult;
}

//other approach for QR Givens

function QRGivens($vMatrix)
{
	$iDim1 = sizeof($vMatrix);
	$iDim2 = sizeof($vMatrix[0]);
	
/*
row		col
0	->	a
		b
		c
1	->	d
		e
		f
2	->	h
		i
		j

a b c		
d e f
h i j	

a11 a12 a13
a21 a22 a23
a31 a32 a33	
*/
	$iNumIt = 0;

	for($i = 1; $i < $iDim2; $i++)
		$iNumIt = $iNumIt + $iDim1-$i;
	
	$vTMP = array_fill(0, $iDim1, 0);
	
	$vResult = array_fill(0, $iDim1, $vTMP);

	$vOldM = $vResult;


	$vUnityMat = unityMatrix($iDim1);
	


	for($k = 0; $k < $iDim2; $k++)	
	{
		for($l = $k+1; $l < $iDim1; $l++)
		{

			if($vMatrix[$l][$k] == 0)
				continue;

			$c = calcC($vMatrix,$l,$k);
			$s = calcS($vMatrix,$l,$k);

			$vRotMat = $vUnityMat;

			//echo "c = ".$vMatrix[$k][$k]." / sqrt(".$vMatrix[$k][$k]."**2 + ".$vMatrix[$l][$k]."**2)\n";
			//echo "s = ".$vMatrix[$l][$k]." / sqrt(".$vMatrix[$k][$k]."**2 + ".$vMatrix[$l][$k]."**2)\n";
			//echo "c = a".$k.$k." / sqrt(a".$k.$k."**2 + a".$l.$k."**2)\n";
			//echo "s = a".$l.$k." / sqrt(a".$k.$k."**2 + a".$l.$k."**2)\n";

			$vRotMat[$k][$l] = $s*-1;
			$vRotMat[$l][$k] = $s;
			$vRotMat[$k][$k] = $c;
			$vRotMat[$l][$l] = $c;

			//print_r($vMatrix);
			//print_r($vRotMat);

			$vMatrix = multiplyMatrix($vRotMat, $vMatrix);
		}
	}

	//print_r($vMatrix);

	return $vMatrix;
}


//other QR stuff
function calcC($vMatrix, $i, $j)
{
	return ($vMatrix[$j][$j] / sqrt($vMatrix[$j][$j] * $vMatrix[$j][$j] + $vMatrix[$i][$j] * $vMatrix[$i][$j]));
}

//and again QR stuff
function calcS($vMatrix, $i, $j)
{
	return (-1 * $vMatrix[$i][$j] / sqrt($vMatrix[$j][$j] * $vMatrix[$j][$j] + $vMatrix[$i][$j] * $vMatrix[$i][$j]));	
}


//rotate a matrix by an angle around the x-axis
function RotateX($vMatrix, $fAngle)
{
	$vRX = array(array(1,0,0), array(0,cos($fAngle),-1*sin($fAngle)), array(0,sin($fAngle),cos($fAngle)));
	
	return multiplyMatrix($vRX, $vMatrix);
}
//rotate a matrix by an angle around the y-axis
function RotateY($vMatrix, $fAngle)
{
	$vRY = array(array(cos($fAngle),0,sin($fAngle)), array(0,1,0), array(-1*sin($fAngle),0,cos($fAngle)));
	
	return multiplyMatrix($vRY, $vMatrix);
}
//rotate a matrix by an angle around the z-axis
function RotateZ($vMatrix, $fAngle)
{
	$vRZ = array(array(cos($fAngle),-1*sin($fAngle),0), array(sin($fAngle),cos($fAngle),0), array(0,0,1));
	
	return multiplyMatrix($vRZ, $vMatrix);
}


/**
 * Gaussian elimination for linear equation system solution
 * @param  array $A matrix
 * @param  array $x vector
 * @return array    solution vector
 */
function gauss($A, $x) {
    # Just make a single matrix
    for ($i=0; $i < count($A); $i++) {
        $A[$i][] = $x[$i];
    }
    $n = count($A);

    for ($i=0; $i < $n; $i++) {
        # Search for maximum in this column
        $maxEl = abs($A[$i][$i]);
        $maxRow = $i;
        for ($k=$i+1; $k < $n; $k++) {
            if (abs($A[$k][$i]) > $maxEl) {
                $maxEl = abs($A[$k][$i]);
                $maxRow = $k;
            }
        }


        # Swap maximum row with current row (column by column)
        for ($k=$i; $k < $n+1; $k++) {
            $tmp = $A[$maxRow][$k];
            $A[$maxRow][$k] = $A[$i][$k];
            $A[$i][$k] = $tmp;
        }

        # Make all rows below this one 0 in current column
        for ($k=$i+1; $k < $n; $k++) {
            $c = -$A[$k][$i]/$A[$i][$i];
            for ($j=$i; $j < $n+1; $j++) {
                if ($i==$j) {
                    $A[$k][$j] = 0;
                } else {
                    $A[$k][$j] += $c * $A[$i][$j];
                }
            }
        }
    }

    # Solve equation Ax=b for an upper triangular matrix $A
    $x = array_fill(0, $n, 0);
    for ($i=$n-1; $i > -1; $i--) {
        $x[$i] = $A[$i][$n]/$A[$i][$i];
        for ($k=$i-1; $k > -1; $k--) {
            $A[$k][$n] -= $A[$k][$i] * $x[$i];
        }
    }

    return $x;
}

?>
