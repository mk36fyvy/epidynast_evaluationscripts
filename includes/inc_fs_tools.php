

<?php

//gives back all files of a cetain file extension in a folder
function scandir_ext($path,$extension)
{
    $file_parts = array();
    $result = array();
    $k = 0;

    $scan = scandir("${path}");
    for ($i=0; $i<count($scan); $i++)
    {
		$file_parts['extension'] = "";
        $file_parts = pathinfo($path."/".$scan[$i]);
        if ($scan[$i] != '.' && $scan[$i] != '..' && $scan[$i] != is_dir($scan[$i]))
		{
			if($file_parts['extension'] == $extension)
        	{
            	array_push($result, $scan[$i]);
            	//$result[$k] == $scan[$i];
            	$k++;
			}
        }

    }

    return $result;

}

//deletes a directory
function deleteDir($dirPath)
{
    if (! is_dir($dirPath))
	{
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/')
	{
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file)
	{
        if (is_dir($file))
		{
			deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}


?>
