<?php

/*
prop data:

[iteration][channel][nucleosome]

*/


require_once("inc_data_func.php");
require_once("inc_matrix_func.php");


//creates gnuplot compatible file 
//TODO:
//data structure:

//parameter structure:
function createGNUPlotFile($data,$params, $custom = array())
{


	$gplt = "
set datafile separator \"\t\"\n
set autoscale fix\n
set key outside right center\n";
if($params["terminal"] == "png" || $params["terminal"] == "svg")
{
	$gplt = $gplt."set terminal ".$params["terminal"]." size ".$params["size_x"].",".$params["size_y"]."\n";
	$gplt = $gplt."set output '".$params["output"]."'\n";
}
elseif($params["terminal"] == "dumb")
{
	$gplt = $gplt."set terminal ".$params["terminal"]."\n";
}


$gplt = $gplt."set object 1 rect from screen 0, 0, 0 to screen 1, 1, 0 behind\n
set object 1 rect fc  rgb \"white\"  fillstyle solid 1.0\n
set xrange [".$params["x_min"].":".$params["x_max"]."]\n
set yrange [".$params["y_min"].":".$params["y_max"]."]\n
set xlabel \"".$params["x_label"]."\" \n
set ylabel \"".$params["y_label"]."\"\n
set title \"Concentration of modifications\"\n

set mxtics 5\n";

if(sizeof($data) != 0)
	$gplt = $gplt."plot";

for($i = 0; $i < sizeof($data); $i++)
{
	$gplt = $gplt." '".$data[$i]["data"]."' u ".$data[$i]["x"].":".$data[$i]["y"]." title \"".$data[$i]["title"]."\" ".$data[$i]["linestyle"].",";
}

$gplt = $gplt."\n";


for($i = 0; $i < sizeof($custom); $i++)
{
	$gplt = $gplt.$custom[$i]."\n";
}
$gplt = $gplt."\n";

	return $gplt;

}



//plots data from data array as plots of lines of rectangles, each representing a nucleosome
//each line represents an iteration
//a color map is used to give each nucleosome (with a distinctive et of modifications!) a certain color (r,g,b):
//2d associative array with values $colormap["NUCLEOSOME"]["red"/"green"/"blue"], for eample $colormap["H3[K4.me]"]["red]  or  $colormap["H3[K4.me]H4[K5.ac]"]["blue"]
//image is saved as $filename
//imagetype is svg as standrd or png if the php-gd library is installed
function plotData($cdata, $colormap, $filename, $resolution, $imagetype = "svg")
{
	$times = array();

	//$cdata[0] = $cdata[1];
	
	$totaldata = $cdata[1];

	$data = array();
	$times = array();

    $numit = sizeof($totaldata);
	$numnuc = sizeof(explode(";",$totaldata[1]))-1;

	$itcounter = 0;

	$totaldata[0] = $totaldata[1];


	for($it = 0; $it < $numit; $it++)
	{
		if($it > $itcounter*$resolution)
		{
			array_push($data, $totaldata[$it]);
			$time = explode(" ", $cdata[0][$it])[1];
			array_push($times,$time);
			$itcounter++;
		}

	}

	$nummods = sizeof($colormap);


	//all units in px
	$fMargin = 20;

	$field_size = 10;
	
	$frame_width = 1;

	$frame_spacing = 0;

	$spacing = 10;

	$numfields_x = ceil(sqrt($nummods));

	$numfields_y = ceil($nummods/$numfields_x);

	$numit = sizeof($data);


	//print_r($data);
	//print_r($times);
	//die(0);


	//$fDimX = 2 * $fMargin + ($numnuc-1) * $spacing + 2 * $numnuc * $frame_width * $numfields_x + $numnuc * $field_size * $numfields_x;
	$fDimX = 1350;
	$fDimY = 2 * $fMargin + ($numit-1) * $spacing + 2 * $numit * $frame_width * $numfields_y + $numnuc * $field_size * $numfields_y;

	if($imagetype == "png")
	{
		$im = imagecreatetruecolor($fDimX, $fDimY)
      or die('Cannot Initialize new GD image stream');
	}
	if($imagetype == "svg")
	{

		$im = fopen($filename, "w");

		fwrite($im,'<?xml version="1.0" standalone="no"?>');
		fwrite($im,'<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">');
		fwrite($im,'<svg width="'.$fDimX.'px" height="'.$fDimY.'px" version="1.1" xmlns="http://www.w3.org/2000/svg" viewport-fill="black">');
		fwrite($im, '<rect x="0" y="0" width="'.$fDimX.'" height="'.$fDimY.'" style="fill: white;"/>');
	}
	else
		die('Error! No valid image type');
	
	$pixred = 0;
	$pixblue = 0;
	$pixgreen = 0;

	//print_r($colormap);

	for($y = 0; $y < $numit; $y++)
	{

		$state = explode(";",$data[$y]);



		for($x = 0; $x < ($numnuc+1); $x++)
		{
						
	
//fwrite($im, '<text x="'.$x.'" y="'.$field_size.'" style="font-size: '.$field_size.'px; color: white;" fill="white" dy="'.(-2*$spacing).'">'.($y+1).'</text>');
//fwrite($im, '<rect x="'.$x*($field_size+$spacing).'" y="'.$y*($field_size+$spacing).'" width="'.$field_size.'" height="'.$field_size.'" style="fill: rgb('.$pixred.','.$pixgreen.','.$pixblue.'); stroke-width: 1px; stroke: black;"/>');

			if($x != 0)
			{				
				$nucleosome = explode(":",$state[($x-1)])[0];



				//print_r($nucleosome);

				//echo "\n".$nucleosome."\n";

				if($nucleosome == "{}")
				{
					$pixred = 255;
					$pixblue = 255;
					$pixgreen = 255;
					//die(0);
				}			
				else
				{
					$nucleosome = str_replace(array("{","}"),"",$nucleosome);
	
					$pixred = $colormap[$nucleosome]["red"];
					$pixblue = $colormap[$nucleosome]["blue"];
					$pixgreen = $colormap[$nucleosome]["green"];
				}

				//echo "r: ".$pixred." g: ".$pixgreen." b: ".$pixblue."\n";

				//$xpos = $fMargin + ($x-1)*($field_size+$spacing);

				//simplified for single field nucs
				$xpos = $fMargin + ($x-1)*(2*$frame_width+$field_size+$spacing);
				$ypos = $fMargin + $y*($field_size+$spacing);

			}

			if($imagetype == "svg")
			{
				if($x == 0)
				{
					fwrite($im, '<text x="2" y="'.($fMargin + $y*($field_size+$spacing)).'" style="font-size: '.$field_size.'px; color: black;" fill="black" dy="'.(-2*$spacing).'">'.($y+1).'</text>');
				}
				else	
				{
					fwrite($im, '<rect x="'.$xpos.'" y="'.$ypos.'" width="'.$field_size.'" height="'.$field_size.'" style="fill: rgb('.$pixred.','.$pixgreen.','.$pixblue.'); stroke-width: '.$frame_width.'px; stroke: black;"/>');
				}
			}
			if($imagetype == "png")
			{
				if($x == 0)
				{
					
				}
				else
				{
					$color = ImageColorAllocate ($im, $pixred, $pixgreen, $pixblue);
					imagefilledrectangle ($im, $xpos, $ypos, $xpos+$field_size, $ypos+$field_size, $color);
				}
			}
			
		}
	}


	if($imagetype == "svg")
	{
		fwrite($im,'</svg>');
		fclose($im);
	}
	if($imagetype == "png")
	{
		imagepng($im, $filename);
		imagedestroy($im);
	}
	/*
	for($it = 1; $it < $numit; $it++)
	{
		//print_r($simoutput[$run][$it]);
		$vStateData = getStateData($data[$it]);
		$state = $data[$it];

		$time = explode(" ", $cdata[0][$it])[1];

		array_push($times,$time);


		$vTMP = explode("}",$vStateData[0]);

		array_pop($vTMP);
		
		//print_r($vTMP);
		//die(0);
		
		for($nuc = 0; $nuc < sizeof($vTMP); $nuc++)
		{
			$nucleosome = str_replace(array("{","}"),"",$vTMP[0]);
			//decompose histones
			$vHistones = explode("]",$nucleosome);
			print_r($vHistones);
			die(0);
		}
	}*/
}


//draws a matrix map for a 2d array filled with values in the range 0-1
//field size is the size of the maps rectangles in pixels
//$filename is the image filename
function draw_normalized_matrix_map($data, $field_size, $filename, $imagetype)
{

	//echo $filename;
	$spacing = 1;

	$fDimX = sizeof($data[0]);
	$fDimY = sizeof($data);

	if($imagetype == "png")
	{
		$im = imagecreatetruecolor(($field_size+$spacing)*($fDimX+1), ($field_size+$spacing)*$fDimY)
      or die('Cannot Initialize new GD image stream');
	}
	if($imagetype == "svg")
	{

		$im = fopen($filename, "w");

		fwrite($im,'<?xml version="1.0" standalone="no"?>');
		fwrite($im,'<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">');
		fwrite($im,'<svg width="'.($field_size+$spacing)*($fDimX+1).'px" height="'.($field_size+$spacing)*$fDimY.'px" version="1.1" xmlns="http://www.w3.org/2000/svg" viewport-fill="black">');
		fwrite($im, '<rect x="0" y="0" width="'.($field_size+$spacing)*($fDimX+1).'" height="'.($field_size+$spacing)*$fDimY.'" style="fill: black;"/>');
	}
	else
		die('Error! No valid image type');

/*
	for x in range(width):
        p = x / float(width - 1)
        r = int((1.0-p) * r1 + p * r2 + 0.5)
        g = int((1.0-p) * g1 + p * g2 + 0.5)
        b = int((1.0-p) * b1 + p * b2 + 0.5)
        pix[x,y] = (r,g,b)
*/

	$pixred = 0;
	$pixblue = 0;
	$pixgreen = 0;

	for($y = 0; $y < $fDimY; $y++)
	{
		for($x = 0; $x < $fDimX; $x++)
		{
						
	
			if($x == 0)
				if($y == 0)
					fwrite($im, '<text x="'.$x.'" y="'.$field_size.'" style="font-size: '.$field_size.'px; color: white;" fill="white" dy="'.(-2*$spacing).'">'.($y+1).'</text>');
				else
					fwrite($im, '<text x="'.$x.'" y="'.(($y*$field_size+$y*$spacing)+$field_size).'" style="font-size: '.$field_size.'px; color: white;" fill="white">'.($y+1).'</text>');
			else
			{
				$value = $data[$y][$x-1];
				if($value > 0.5)
				{
					$pixred = round(512-$value*512);
					$pixgreen = 255;
					$pixblue = 0;	
				}			
				else
				{
					$pixred = 255;
					$pixgreen = round(512*$value);
					$pixblue = 0;
				}

				//echo "x: ".$x." y: ".$y." R: ".$pixred." B: ".$pixgreen." G: ".$pixblue."\n";


				if($imagetype == "svg")
				{
					fwrite($im, '<rect x="'.$x*($field_size+$spacing).'" y="'.$y*($field_size+$spacing).'" width="'.$field_size.'" height="'.$field_size.'"  
     style="fill: rgb('.$pixred.','.$pixgreen.','.$pixblue.'); stroke-width: 1px; stroke: black;"/>');
				}
				if($imagetype == "png")
				{
					$color = ImageColorAllocate ($im, $pixred, $pixgreen, $pixblue);
					imagefilledrectangle ($im, $x*$field_size, $y*$field_size, $x*$field_size+$field_size, $y*$field_size+$field_size, $color);
				}	

			 
			}
			
		}
	}


	if($imagetype == "svg")
	{
		fwrite($im,'</svg>');
		fclose($im);
	}
	if($imagetype == "png")
	{
		imagepng($im, $filename);
		imagedestroy($im);
	}
}
 
 
 //plots graph similar to plotdata function
 //multiple runs are loaded as array (cdata array is array of cdata arrays in the plotdata function)
//color is determined after probability to find on of three modifications (me, ac, un) at a nucleosome at a certain time
function plotMeanDataArray($cdata, $colormap, $filename, $resolution, $imagetype = "svg")
{
	$iTimes = array();

	
	$iNumBins = sizeof($cdata);
	$iNumNucs = sizeof($cdata[0]);
	$iNumMods = sizeof($cdata[0][0]);


	//all units in px
	$fMargin = 20;

	$field_size = 10;
	
	$frame_width = 1;

	$frame_spacing = 0;

	$spacing = 10;

	$numfields_x = ceil(sqrt($iNumMods));

	$numfields_y = ceil($iNumMods/$numfields_x);



	//print_r($data);
	//print_r($times);
	//die(0);


	//$fDimX = 2 * $fMargin + ($numnuc-1) * $spacing + 2 * $numnuc * $frame_width * $numfields_x + $numnuc * $field_size * $numfields_x;
	$fDimX = 1350;
	$fDimY = 2 * $fMargin + ($iNumBins-1) * $spacing + 2 * $iNumBins * $frame_width * $numfields_y + $iNumNucs * $field_size * $numfields_y;

	if($imagetype == "png")
	{
		$im = imagecreatetruecolor($fDimX, $fDimY)
      or die('Cannot Initialize new GD image stream');
	}
	if($imagetype == "svg")
	{

		$im = fopen($filename.".".$imagetype, "w");

		fwrite($im,'<?xml version="1.0" standalone="no"?>');
		fwrite($im,'<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">');
		fwrite($im,'<svg width="'.$fDimX.'px" height="'.$fDimY.'px" version="1.1" xmlns="http://www.w3.org/2000/svg" viewport-fill="black">');
		fwrite($im, '<rect x="0" y="0" width="'.$fDimX.'" height="'.$fDimY.'" style="fill: white;"/>');
	}
	else
		die('Error! No valid image type');
	
	$pixred = 0;
	$pixblue = 0;
	$pixgreen = 0;

	//print_r($colormap);

	for($y = 0; $y < $iNumBins; $y++)
	{

		for($x = 0; $x < ($iNumNucs+1); $x++)
		{
						
	
//fwrite($im, '<text x="'.$x.'" y="'.$field_size.'" style="font-size: '.$field_size.'px; color: white;" fill="white" dy="'.(-2*$spacing).'">'.($y+1).'</text>');
//fwrite($im, '<rect x="'.$x*($field_size+$spacing).'" y="'.$y*($field_size+$spacing).'" width="'.$field_size.'" height="'.$field_size.'" style="fill: rgb('.$pixred.','.$pixgreen.','.$pixblue.'); stroke-width: 1px; stroke: black;"/>');

			if($x != 0)
			{				

				$pixred = 255 * $cdata[$y][($x-1)]["{H3[K4.me]}"];
				$pixblue = 255 * $cdata[$y][($x-1)]["{H3[K4.ac]}"];
				$pixgreen = 255 * $cdata[$y][($x-1)]["{}"];
				
				$xpos = $fMargin + ($x-1)*(2*$frame_width+$field_size+$spacing);
				$ypos = $fMargin + $y*($field_size+$spacing);

			}

			if($imagetype == "svg")
			{
				if($x == 0)
				{
					fwrite($im, '<text x="2" y="'.($fMargin + $y*($field_size+$spacing)).'" style="font-size: '.$field_size.'px; color: black;" fill="black" dy="'.(-2*$spacing).'">'.($y+1).'</text>');
				}
				else	
				{
					fwrite($im, '<rect x="'.$xpos.'" y="'.$ypos.'" width="'.$field_size.'" height="'.$field_size.'" style="fill: rgb('.$pixred.','.$pixgreen.','.$pixblue.'); stroke-width: '.$frame_width.'px; stroke: white;"/>');
				}
			}
			if($imagetype == "png")
			{
				if($x == 0)
				{
					
				}
				else
				{
					$color = ImageColorAllocate ($im, $pixred, $pixgreen, $pixblue);
					imagefilledrectangle ($im, $xpos, $ypos, $xpos+$field_size, $ypos+$field_size, $color);
				}
			}
			
		}
	}


	if($imagetype == "svg")
	{
		fwrite($im,'</svg>');
		fclose($im);
	}
	if($imagetype == "png")
	{
		imagepng($im, $filename);
		imagedestroy($im);
	}
	/*
	for($it = 1; $it < $numit; $it++)
	{
		//print_r($simoutput[$run][$it]);
		$vStateData = getStateData($data[$it]);
		$state = $data[$it];

		$time = explode(" ", $cdata[0][$it])[1];

		array_push($times,$time);


		$vTMP = explode("}",$vStateData[0]);

		array_pop($vTMP);
		
		//print_r($vTMP);
		//die(0);
		
		for($nuc = 0; $nuc < sizeof($vTMP); $nuc++)
		{
			$nucleosome = str_replace(array("{","}"),"",$vTMP[0]);
			//decompose histones
			$vHistones = explode("]",$nucleosome);
			print_r($vHistones);
			die(0);
		}
	}*/
}

//plot 3-state triangle plot in $sFileName.svg
//plots folder of tar.gz packaged simulations in triangle
//2d coordinates of the plot are saved seperately (tri_coord_FILENAME)
//plotting can take a while and can be resumed from coord file for lange number of sims

function trianglePlotFull($vFiles, $fTime, $sFileName, $bCont = 0)
{

	$fScale = 300;

	$vDataPoints = array();
	$vColorInfo = array();

	print_r($vFiles);
	
	 
    if($bCont == 1)
    {
        $fhCoord = fopen("tri_coord_".$sFileName,"r");
        
        while(!feof($fhCoord))
		{
			$sLine = fgets($fhCoord);
            $vCoords = explode("\t", $sLine);
			//print_r($vCoords);
			if(array_search($vCoords[0]."tar.gz", $vFiles))
            {
				$iKey = array_search($vCoords[0]."tar.gz", $vFiles);

				echo "Key: ".$iKey."\n";
                echo "Skipping ".$vFiles[$iKey].": already processed.\n";
                unset($vFiles[$iKey]);
            } 
		}

        fclose($fhCoord);

        $fhCoord = fopen("tri_coord_".$sFileName,"a");  //TODO: check for empty lines
    }
    else
        $fhCoord = fopen("tri_coord_".$sFileName,"w");

	$iNumSims = sizeof($vFiles);

	print_r($vFiles);
	
	
	//die(0);
	
	for($iSim = 0; $iSim < $iNumSims; $iSim++)
	{
		$sFileName = explode(".",$vFiles[$iSim])[0];
	
		$sHash = explode("_", $sFileName)[0];

		echo "Processing ".$sFileName."\n";

		echo "Extracting ".$vFiles[$iSim]."\n";
		system("mkdir ".$sFileName);
		system("tar -xzf ".$sFileName.".tar.gz -C ./".$sFileName);


		$vScan = scandir($sFileName."/".$sFileName);



		$sPath = "";

		$vSimFolder = array();

		for ($iScanItem = 0; $iScanItem < count($vScan); $iScanItem++)
		{

			//$file_parts = pathinfo($filename."/".$scan[$i]);
			if ($vScan[$iScanItem] != '.' && $vScan[$iScanItem] != '..')// && $scan[$i] == is_dir($scan[$i]))
			{
				//echo $scan[$i];
				$sPath = $vScan[$iScanItem];
				array_push($vSimFolder, $vScan[$iScanItem]);
			}

		}

		if($sPath == "")
			die(0);	//TODO: error handling


    	$iNumRuns = sizeof($vSimFolder);
	
		$vMeanData = array(0,0,0);	
	
		for($iRun = 0; $iRun < $iNumRuns; $iRun++)
		{

			//triangle rotation (expects points in vector list)



			$sTargetState = "{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}";

	
			//prepare mean data

			//HACK: include needed function if not already done
			include_once("/scratch/nicoh/eval/inc_matrix_func.php");
			include_once("/scratch/nicoh/eval/inc_get_data.php");
			include_once("/scratch/nicoh/eval/inc_data_func.php");

			$cData = getSimpleStateData("./".$sFileName."/".$sFileName."/".$vSimFolder[$iRun]."/outfile.txt");

			$iNumIt = sizeof($cData);

			$iTargetIt = -1;

			//echo $iNumIt."\n";

			for($iIt = 0; $iIt < $iNumIt; $iIt++)
			{
				$vInfo = explode(" ", $cData[0][$iIt]);

				$fCurTime = $vInfo[1];

				//echo $fCurTime.":".$fTime."\n";

				if($fCurTime >= $fTime)
				{
					$iTargetIt = $iIt;
					break;
				} 
			}


			if($iTargetIt == -1)
				$iTargetIt = $iNumIt - 1;
			
			$sData = $cData[0][$iTargetIt];
			$sData = convertOutfileStateToString($sData);
			$vData = getRightWrong($sData, $sTargetState);

	
			$vMeanData[0] = $vMeanData[0] + $vData[0] / $iNumRuns;;	
			$vMeanData[1] = $vMeanData[1] + $vData[1] / $iNumRuns;;	
			$vMeanData[2] = $vMeanData[2] + $vData[2] / $iNumRuns;;	
	

		}

		//TODO: make dynamic together with targetstate
		//$iNumNuc = sizeof(explode("}",));
		$iNumNuc = 60;

		$vVector = array(array($vMeanData[0] / $iNumNuc * $fScale), array($vMeanData[1] / $iNumNuc * $fScale), array($vMeanData[2] / $iNumNuc * 		$fScale));
	
	
		$vVector = RotateZ($vVector,deg2rad(-45));
		$vVector = RotateY($vVector,deg2rad(-54.73));
		$vVector = RotateZ($vVector,deg2rad(-90));

		array_push($vDataPoints, array($vVector[0][0], $vVector[1][0]));
	
		$sColor = "black";

		//TODO: enable filtering?	
		//if(array_search($hash, $vGood))
		//	$color = "green";
		//if(array_search($hash, $vBad))
		//	$color = "red";

		array_push($vColorInfo, $sColor);

		fputs($fhCoord, $sFileName."\t".$vVector[0][0]."\t".$vVector[1][0]."\n");
	
		system("rm -R ".$sFileName);

	}	

	fclose($fhCoord);

	$vAxisX = array(array($fScale),array(0),array(0));
	$vAxisY = array(array(0),array($fScale),array(0));
	$vAxisZ = array(array(0),array(0),array($fScale));

	$vRotated11 = RotateZ($vAxisX,deg2rad(-45));
	$vRotated12 = RotateZ($vAxisY,deg2rad(-45));
	$vRotated13 = RotateZ($vAxisZ,deg2rad(-45));

	$vRotated21 = RotateY($vRotated11,deg2rad(-54.73));
	$vRotated22 = RotateY($vRotated12,deg2rad(-54.73));
	$vRotated23 = RotateY($vRotated13,deg2rad(-54.73));

	$vRotated21 = RotateZ($vRotated21,deg2rad(-90));
	$vRotated22 = RotateZ($vRotated22,deg2rad(-90));
	$vRotated23 = RotateZ($vRotated23,deg2rad(-90));

	$fMaxX = max(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$fMinX = max(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$fMaxY = max(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));
	$fMinY = max(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));


	$fMargin = 40 * $fScale * 0.05;
	$fDimX = $fMaxX - $fMinX + $fMargin;
	$fDimY = $fMaxY - $fMinY + $fMargin;

	$fhIm = fopen($sFileName.".svg","w");

	fwrite($fhIm,'<?xml version="1.0" standalone="no"?>');
	fwrite($fhIm,'<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">');
	fwrite($fhIm,'<svg width="'.$fDimX.'px" height="'.$fDimY.'px" version="1.1" xmlns="http://www.w3.org/2000/svg" viewport-fill="black">');
	fwrite($fhIm, '<rect x="0" y="0" width="'.$fDimX.'" height="'.$fDimY.'" style="fill: white;"/>');
		
	//draw axes
		
	$Corner1X = $vRotated21[0][0];
	$Corner1Y = $vRotated21[1][0];
	$Corner2X = $vRotated22[0][0];
	$Corner2Y = $vRotated22[1][0];
	$Corner3X = $vRotated23[0][0];
	$Corner3Y = $vRotated23[1][0];
	
	fwrite($fhIm, '<text x="'.($Corner1X - 10 + $fMargin / 2).'" y="'.($Corner1Y + $fMargin / 2).'" fill="red" text-anchor="end" style="font-family:sans-serif; font: 15px">T</text>');
	fwrite($fhIm, '<text x="'.($Corner2X + 10 + $fMargin / 2).'" y="'.($Corner2Y + $fMargin / 2).'" fill="red" text-anchor="start" style="font: 15px sans-serif">F</text>');
	fwrite($fhIm, '<text x="'.($Corner3X + $fMargin / 2).'" y="'.($Corner3Y - 10 + $fMargin / 2).'" fill="red" text-anchor="middle" style="font: 15px sans-serif">N</text>');

	fwrite($fhIm, '<line x1="'.($Corner1X + $fMargin / 2).'" y1="'.($Corner1Y + $fMargin / 2).'" x2="'.($Corner2X + $fMargin / 2).'" y2="'.($Corner2Y + $fMargin / 2).'" style="stroke:black; stroke-width:2px;" />');
	fwrite($fhIm, '<line x1="'.($Corner2X + $fMargin / 2).'" y1="'.($Corner2Y + $fMargin / 2).'" x2="'.($Corner3X + $fMargin / 2).'" y2="'.($Corner3Y + $fMargin / 2).'" style="stroke:black; stroke-width:2px;" />');
	fwrite($fhIm, '<line x1="'.($Corner3X + $fMargin / 2).'" y1="'.($Corner3Y + $fMargin / 2).'" x2="'.($Corner1X + $fMargin / 2).'" y2="'.($Corner1Y + $fMargin / 2).'" style="stroke:black; stroke-width:2px;" />');


	for($i = 0; $i < sizeof($vDataPoints); $i++)
	{
		$fCX = $vDataPoints[$i][0] + $fMargin / 2;
		$fCY = $vDataPoints[$i][1] + $fMargin / 2;
	
		$sColor = $vColorInfo[$i];

		$sColor = "black";

		fwrite($fhIm, '<circle cx="'.$fCX.'"  cy="'.$fCY.'"  r="1" stroke="'.$sColor.'" fill="'.$sColor.'" fill-opacity="0.2" stroke-opacity="0.2" stroke-width="0"/>');	
	
	}

	fwrite($fhIm,'</svg>');
	
	fclose($fhIm);
}



//plots 3-state triangle plot from coordiates file alone

function trianglePlot($sCoordfile, $sFileName)
{
	$fScale = 300.0;

	$iTicMax = 60;
	$vDataPoints = array();

	$fLegendDistance = 20;

	$picfilename = $sFileName;

	if(gettype($sCoordfile) == "string")
	{
		$fhCoord = fopen($sCoordfile,"r");
		
		while (($buffer = fgets($fhCoord, 4096)) !== false) 
		{
			$vTMP = explode("\t",$buffer);
	
			$vTMP[2] = explode("\n",$vTMP[2])[0];

			array_push($vDataPoints, array($vTMP[1], $vTMP[2]));
		}

		fclose($fhCoord);
	}
	elseif(gettype($sCoordfile) == "array")
	{
		$sMode = "vector";
		$vDataPoints = $sCoordfile;
	}
	else
	{
		echo "Input Error!";
		die(0);
	}
	//$vDataPoints = array_unique($vDataPoints, SORT_REGULAR);

	//$vDataPoints = array_map("unserialize", array_unique(array_map("serialize", $vDataPoints)));

//print_r($vDataPoints);

//die(0);

	$vAxisX = array(array($fScale),array(0),array(0));
	$vAxisY = array(array(0),array($fScale),array(0));
	$vAxisZ = array(array(0),array(0),array($fScale));

	$vRotated11 = RotateZ($vAxisX,deg2rad(-45));
	$vRotated12 = RotateZ($vAxisY,deg2rad(-45));
	$vRotated13 = RotateZ($vAxisZ,deg2rad(-45));

	$vRotated21 = RotateY($vRotated11,deg2rad(-54.73));
	$vRotated22 = RotateY($vRotated12,deg2rad(-54.73));
	$vRotated23 = RotateY($vRotated13,deg2rad(-54.73));

	$vRotated21 = RotateZ($vRotated21,deg2rad(-90));
	$vRotated22 = RotateZ($vRotated22,deg2rad(-90));
	$vRotated23 = RotateZ($vRotated23,deg2rad(-90));

	$max_x = max(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$min_x = min(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$max_y = max(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));
	$min_y = min(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));

	$margin = 0;

	$margin = 200;

	//$shift_y = -61;
	$shift_y = 0;
	//$margin = 40.0 * $fScale * 0.05;
	$dim_x = $max_x - $min_x;// + $marginX;
	$dim_y = $max_y - $min_y;// + $marginY;

	$im = fopen($picfilename.".svg","w");
	

	fwrite($im,"<?xml version=\"1.0\" standalone=\"no\"?>\n");
	fwrite($im,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
	fwrite($im,"<svg width=\"".($dim_x + $margin)."px\" height=\"".($dim_y + $margin)."px\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewport-fill=\"black\">\n\n");
	fwrite($im,"<style>\n.text {font-family: Helvetica, Arial, Tahoma, Geneva, sans-serif; font: Arial, sans-serif;}\n</style>\n");



	fwrite($im, "<rect x=\"0\" y=\"0\" width=\"".($dim_x + $margin)."\" height=\"".($dim_y + $margin)."\" style=\"fill: white;\"/>\n\n");
	//draw axes
		
	$Corner1X = $vRotated21[0][0];
	$Corner1Y = $vRotated21[1][0];
	$Corner2X = $vRotated22[0][0];
	$Corner2Y = $vRotated22[1][0];
	$Corner3X = $vRotated23[0][0];
	$Corner3Y = $vRotated23[1][0];



	//draw legend

	$fNormalization = sqrt(($Corner1X - $Corner2X)**2 + ($Corner1Y - $Corner2Y)**2);

	$vDisplacement1 = array(0,0);
	$vDirection1 = array(0,0);

	$vDirection1[0] = (($Corner1X - $Corner2X) / $fNormalization);
	$vDirection1[1] = (($Corner1Y - $Corner2Y) / $fNormalization);

	$vDisplacement1[0] = (($Corner1X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement1[1] = (($Corner1Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement2 = array(0,0);
	$vDirection2 = array(0,0);

	$vDirection2[0] = (($Corner3X - $Corner2X) / $fNormalization);
	$vDirection2[1] = (($Corner3Y - $Corner2Y) / $fNormalization);

	$vDisplacement2[0] = (($Corner3X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement2[1] = (($Corner3Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement3 = array(0,0);
	$vDirection3 = array(0,0);

	$vDirection3[0] = (($Corner3X - $Corner1X) / $fNormalization);
	$vDirection3[1] = (($Corner3Y - $Corner1Y) / $fNormalization);

	$vDisplacement3[0] = (($Corner3X - $Corner1X) / $fNormalization) * $fLegendDistance;
	$vDisplacement3[1] = (($Corner3Y - $Corner1Y) / $fNormalization) * $fLegendDistance;	


	$fHeight = ($Corner1X - $Corner2X);

	$vMiddle1 = array(0,0);

	$vMiddle1[0] = ($Corner1X - $Corner2X) / 2;
	$vMiddle1[1] = ($Corner1Y - $Corner2Y) / 2;

	$vMiddle2 = array(0,0);

	$vMiddle2[0] = ($Corner2X - $Corner3X) / 2;
	$vMiddle2[1] = ($Corner2Y - $Corner3Y) / 2;

	$vMiddle3 = array(0,0);

	$vMiddle3[0] = ($Corner3X - $Corner1X) / 2;
	$vMiddle3[1] = ($Corner3Y - $Corner1Y) / 2;


//picture title text
//fwrite($im, "<text class=\"text\" x=\"0\" y=\"".($Corner1Y - 100)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".$picfilename."</text>");

fwrite($im, "<!-- Grid on/off, change visibility to either visibility:visible or visibility:hidden -->\n");
fwrite($im, "<g style=\"visibility:visible\">\n\n");
//fwrite($im, "<g style=\"visibility:hidden\">\n\n");


//grid (equal amounts)
$tics = 20;

//orthogonal grid (tics)

$ticlength = 10;
$ticunit = $iTicMax / $tics;


	for($i = 0; $i < $tics + 1; $i++)
	{
		

		fwrite($im, "<line x1=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[0])."\" y1=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[1])."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics))."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[0])."\" y=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");


		fwrite($im, "<line x1=\"".($Corner3X - $i * $vDirection2[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection2[1] * ($fNormalization / $tics))."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 4) * $vDirection3[0])."\" y2=\"".($Corner1Y - ($ticlength + 4) * $vDirection3[1])."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 8) * $vDirection3[0])."\" y=\"".($Corner1Y - ($ticlength + 8) * $vDirection3[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($i * $ticunit)."</text>");


		fwrite($im, "<line x1=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength)."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" />\n");

		fwrite($im, "<line x1=\"".($Corner1X - $vMiddle1[0] - $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" stroke-dasharray=\"4 4\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength + 4)."\" y=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");

	

}



//axis legend

fwrite($im, "<text class=\"text\" x=\"".($Corner3X)."\" y=\"".($Corner3Y+30)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">N</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner1X-40)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">R</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner2X+20)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">W</text>");
fwrite($im, "</g>\n\n");


fwrite($im,"\n\n");
	

//border lines

	fwrite($im, "<line x1=\"".($Corner1X)."\" y1=\"".($Corner1Y)."\" x2=\"".($Corner2X)."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2 + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner2X)."\" y1=\"".($Corner2Y)."\" x2=\"".($Corner3X)."\" y2=\"".($Corner3Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner3X)."\" y1=\"".($Corner3Y)."\" x2=\"".($Corner1X)."\" y2=\"".($Corner1Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");


fwrite($im,"\n\n");

	for($i = 0; $i < sizeof($vDataPoints); $i++)
	{
		$cx = $vDataPoints[$i][0];
		$cy = $vDataPoints[$i][1];
	
		//echo $vDataPoints[$i][0]." - ".$vDataPoints[$i][1]."\n";
	
		$color = "black";

		fwrite($im, "<circle cx=\"".$cx."\"  cy=\"".$cy."\"  r=\"1\" stroke=\"".$color."\" fill=\"".$color."\" fill-opacity=\"0.2\" stroke-opacity=\"0.2\" stroke-width=\"0\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\"/>\n");	
	
	}


	fwrite($im,"</svg>");
		
	fclose($im);	



}


//plots monochrome 3-state triangle plot and takes a second file with selected simulations (hashes)
//to plot them colored to explore their positions

function trianglePlotColorSelGen($sCoordfile, $sFileName)
{
	$fScale = 300.0;

	$iTicMax = 60;
	$vDataPoints = array();
	$vColorPoints = array();
	$fLegendDistance = 20;
	$vRules = array();
	$sMode = "";

	$picfilename = $sFileName;

	if(gettype($sCoordfile) == "string")
	{
		$sMode = "file";
		$fhCoord = fopen($sCoordfile,"r");


		while (($buffer = fgets($fhCoord, 4096)) !== false) 
		{
			$vTMP = explode("\t",$buffer);
			//x,y,#true,#false,#neutral,name
			array_push($vDataPoints, array($vTMP[1], $vTMP[2], $vTMP[3], $vTMP[4], $vTMP[5], $vTMP[0]));
		}
	}
	elseif(gettype($sCoordfile) == "array")
	{
		$sMode = "vector";
		$vDataPoints = $sCoordfile[0];
		$vRules = $sCoordfile[1];
	}
	else
	{
		echo "Input Error!";
		die(0);
	}

	$vColorPoints = array_fill(0,sizeof($vDataPoints),"black");

	//C:red;T:20,30;N:5,10;F:20,30



	if($sMode == "file")
	{

		$fhColorSel = fopen($sCoordfile."_selections","r");

		$vSelectedSims = array();

		echo "Processing simulation selection...\n";

		$fhSelSim = fopen($sCoordfile."_selected_sims","w");

		while (($buffer = fgets($fhColorSel, 4096)) !== false) 
		{
			$vTMP = explode(";",$buffer);

			$sColor = "black";
			$iTMax = -1;
			$iTMin = -1;
			$iFMax = -1;
			$iFMin = -1;
			$iNMax = -1;
			$iNMin = -1;
			

			for($i = 0; $i < sizeof($vTMP); $i++)
			{
				$vItem = explode(":",$vTMP[$i]);
				
				//print_r($vItem);
				if($vItem[0] == "C")
				{
					$sColor = $vItem[1];
				}
				elseif($vItem[0] == "T")
				{
					$vTMP2 = explode(",",$vItem[1]);
					$iTMax = max(floatval($vTMP2[0]),floatval($vTMP2[1]));
					$iTMin = min(floatval($vTMP2[0]),floatval($vTMP2[1]));
				}
				elseif($vItem[0] == "F")
				{
					$vTMP2 = explode(",",$vItem[1]);
					$iFMax = max(floatval($vTMP2[0]),floatval($vTMP2[1]));
					$iFMin = min(floatval($vTMP2[0]),floatval($vTMP2[1]));
				}
				elseif($vItem[0] == "N")
				{
					$vTMP2 = explode(",",$vItem[1]);
					$iNMax = max(floatval($vTMP2[0]),floatval($vTMP2[1]));
					$iNMin = min(floatval($vTMP2[0]),floatval($vTMP2[1]));
				}

			}
	
			//echo $sColor."\t".$iTMax."\t".$iTMin."\t".$iFMax."\t".$iFMin."\t".$iNMax."\t".$iNMin."\n";


			for($k = 0; $k < sizeof($vDataPoints); $k++)
			{
				//echo $k."\n";
				$iChT = 0;
				$iChN = 0;
				$iChF = 0;

				if(($vDataPoints[$k][2] > $iTMin && $vDataPoints[$k][2] < $iTMax) || ($iTMin == -1 && $iTMax == -1))
					$iChT = 1;
				if(($vDataPoints[$k][4] > $iNMin && $vDataPoints[$k][4] < $iNMax) || ($iNMin == -1 && $iNMax == -1))
					$iChN = 1;
				if(($vDataPoints[$k][3] > $iFMin && $vDataPoints[$k][3] < $iFMax) || ($iFMin == -1 && $iFMax == -1))
					$iChF = 1;

				if($iChT == 1 && $iChN == 1 && $iChF == 1)
				{
					//$vColorPoints[$k] = $sColor;

					$iCheck = 0;

					for($l = 0; $l < sizeof($vSelectedSims); $l++)
					{
						if($vDataPoints[$k][5] == $vSelectedSims[$l][0])
						{						
							$iCheck = 1;
							break;
						}
					}

					if($iCheck != 1)
					{
						array_push($vSelectedSims, array($vDataPoints[$k][5], $sColor));
						$vColorPoints[$k] = $sColor;
					}
				}
			}
		}
	}
	elseif($sMode == "vector")
	{
		$fhColorSel = fopen($sCoordfile."_selections","r");

		$vSelectedSims = array();

		echo "Processing simulation selection...\n";

		for($iRule = 0; $iRule < sizeof(); $iRule++) 
		{
			$vTMP = explode(";",$vRules[$iRule]);

			$sColor = "black";
			$iTMax = -1;
			$iTMin = -1;
			$iFMax = -1;
			$iFMin = -1;
			$iNMax = -1;
			$iNMin = -1;
			

			for($i = 0; $i < sizeof($vTMP); $i++)
			{
				$vItem = explode(":",$vTMP[$i]);
				
				//print_r($vItem);
				if($vItem[0] == "C")
				{
					$sColor = $vItem[1];
				}
				elseif($vItem[0] == "T")
				{
					$vTMP2 = explode(",",$vItem[1]);
					$iTMax = max(floatval($vTMP2[0]),floatval($vTMP2[1]));
					$iTMin = min(floatval($vTMP2[0]),floatval($vTMP2[1]));
				}
				elseif($vItem[0] == "F")
				{
					$vTMP2 = explode(",",$vItem[1]);
					$iFMax = max(floatval($vTMP2[0]),floatval($vTMP2[1]));
					$iFMin = min(floatval($vTMP2[0]),floatval($vTMP2[1]));
				}
				elseif($vItem[0] == "N")
				{
					$vTMP2 = explode(",",$vItem[1]);
					$iNMax = max(floatval($vTMP2[0]),floatval($vTMP2[1]));
					$iNMin = min(floatval($vTMP2[0]),floatval($vTMP2[1]));
				}

			}
	
			//echo $sColor."\t".$iTMax."\t".$iTMin."\t".$iFMax."\t".$iFMin."\t".$iNMax."\t".$iNMin."\n";


			for($k = 0; $k < sizeof($vDataPoints); $k++)
			{
				//echo $k."\n";
				$iChT = 0;
				$iChN = 0;
				$iChF = 0;

				if(($vDataPoints[$k][2] > $iTMin && $vDataPoints[$k][2] < $iTMax) || ($iTMin == -1 && $iTMax == -1))
					$iChT = 1;
				if(($vDataPoints[$k][4] > $iNMin && $vDataPoints[$k][4] < $iNMax) || ($iNMin == -1 && $iNMax == -1))
					$iChN = 1;
				if(($vDataPoints[$k][3] > $iFMin && $vDataPoints[$k][3] < $iFMax) || ($iFMin == -1 && $iFMax == -1))
					$iChF = 1;

				if($iChT == 1 && $iChN == 1 && $iChF == 1)
				{
					//$vColorPoints[$k] = $sColor;

					$iCheck = 0;

					for($l = 0; $l < sizeof($vSelectedSims); $l++)
					{
						if($vDataPoints[$k][5] == $vSelectedSims[$l][0])
						{						
							$iCheck = 1;
							break;
						}
					}

					if($iCheck != 1)
					{
						array_push($vSelectedSims, array($vDataPoints[$k][5], $sColor));
						$vColorPoints[$k] = $sColor;
					}
				}
			}
		}
	}
	else
	{
		echo "No simulation selection, continuing...\n";
	}


	array_multisort (array_column($vSelectedSims, 0), SORT_ASC, $vSelectedSims);
	//print_r($vDataPoints);
	print_r($vSelectedSims);
	
	for($l = 0; $l < sizeof($vSelectedSims); $l++)
		fputs($fhSelSim, $vSelectedSims[$l][0]."\t".$vSelectedSims[$l][1]."\n");

	$vAxisX = array(array($fScale),array(0),array(0));
	$vAxisY = array(array(0),array($fScale),array(0));
	$vAxisZ = array(array(0),array(0),array($fScale));

	$vRotated11 = RotateZ($vAxisX,deg2rad(-45));
	$vRotated12 = RotateZ($vAxisY,deg2rad(-45));
	$vRotated13 = RotateZ($vAxisZ,deg2rad(-45));

	$vRotated21 = RotateY($vRotated11,deg2rad(-54.73));
	$vRotated22 = RotateY($vRotated12,deg2rad(-54.73));
	$vRotated23 = RotateY($vRotated13,deg2rad(-54.73));

	$vRotated21 = RotateZ($vRotated21,deg2rad(-90));
	$vRotated22 = RotateZ($vRotated22,deg2rad(-90));
	$vRotated23 = RotateZ($vRotated23,deg2rad(-90));

	$max_x = max(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$min_x = min(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$max_y = max(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));
	$min_y = min(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));

	$margin = 0;

	$margin = 200;

	//$shift_y = -61;
	$shift_y = 0;
	//$margin = 40.0 * $fScale * 0.05;
	$dim_x = $max_x - $min_x;// + $marginX;
	$dim_y = $max_y - $min_y;// + $marginY;

	$im = fopen($picfilename.".svg","w");
	

	fwrite($im,"<?xml version=\"1.0\" standalone=\"no\"?>\n");
	fwrite($im,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
	fwrite($im,"<svg width=\"".($dim_x + $margin)."px\" height=\"".($dim_y + $margin)."px\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewport-fill=\"black\">\n\n");
	fwrite($im,"<style>\n.text {font-family: Helvetica, Arial, Tahoma, Geneva, sans-serif; font: Arial, sans-serif;}\n</style>\n");



	fwrite($im, "<rect x=\"0\" y=\"0\" width=\"".($dim_x + $margin)."\" height=\"".($dim_y + $margin)."\" style=\"fill: white;\"/>\n\n");
	//draw axes
		
	$Corner1X = $vRotated21[0][0];
	$Corner1Y = $vRotated21[1][0];
	$Corner2X = $vRotated22[0][0];
	$Corner2Y = $vRotated22[1][0];
	$Corner3X = $vRotated23[0][0];
	$Corner3Y = $vRotated23[1][0];



	//draw legend

	$fNormalization = sqrt(($Corner1X - $Corner2X)**2 + ($Corner1Y - $Corner2Y)**2);

	$vDisplacement1 = array(0,0);
	$vDirection1 = array(0,0);

	$vDirection1[0] = (($Corner1X - $Corner2X) / $fNormalization);
	$vDirection1[1] = (($Corner1Y - $Corner2Y) / $fNormalization);

	$vDisplacement1[0] = (($Corner1X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement1[1] = (($Corner1Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement2 = array(0,0);
	$vDirection2 = array(0,0);

	$vDirection2[0] = (($Corner3X - $Corner2X) / $fNormalization);
	$vDirection2[1] = (($Corner3Y - $Corner2Y) / $fNormalization);

	$vDisplacement2[0] = (($Corner3X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement2[1] = (($Corner3Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement3 = array(0,0);
	$vDirection3 = array(0,0);

	$vDirection3[0] = (($Corner3X - $Corner1X) / $fNormalization);
	$vDirection3[1] = (($Corner3Y - $Corner1Y) / $fNormalization);

	$vDisplacement3[0] = (($Corner3X - $Corner1X) / $fNormalization) * $fLegendDistance;
	$vDisplacement3[1] = (($Corner3Y - $Corner1Y) / $fNormalization) * $fLegendDistance;	



	$fHeight = ($Corner1X - $Corner2X);

	$vMiddle1 = array(0,0);

	$vMiddle1[0] = ($Corner1X - $Corner2X) / 2;
	$vMiddle1[1] = ($Corner1Y - $Corner2Y) / 2;

	$vMiddle2 = array(0,0);

	$vMiddle2[0] = ($Corner2X - $Corner3X) / 2;
	$vMiddle2[1] = ($Corner2Y - $Corner3Y) / 2;

	$vMiddle3 = array(0,0);

	$vMiddle3[0] = ($Corner3X - $Corner1X) / 2;
	$vMiddle3[1] = ($Corner3Y - $Corner1Y) / 2;


	fwrite($im, "<!-- Grid on/off, change visibility to either visibility:visible or visibility:hidden -->\n");
	fwrite($im, "<g style=\"visibility:visible\">\n\n");
//fwrite($im, "<g style=\"visibility:hidden\">\n\n");


//grid (equal amounts)
	$tics = 20;

//orthogonal grid (tics)

	$ticlength = 10;
	$ticunit = $iTicMax / $tics;


	for($i = 0; $i < $tics + 1; $i++)
	{
		

		fwrite($im, "<line x1=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[0])."\" y1=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[1])."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics))."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[0])."\" y=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");


		fwrite($im, "<line x1=\"".($Corner3X - $i * $vDirection2[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection2[1] * ($fNormalization / $tics))."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 4) * $vDirection3[0])."\" y2=\"".($Corner1Y - ($ticlength + 4) * $vDirection3[1])."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 8) * $vDirection3[0])."\" y=\"".($Corner1Y - ($ticlength + 8) * $vDirection3[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($i * $ticunit)."</text>");


/*		fwrite($im, "<line x1=\"".($Corner1X - $vMiddle1[0] - $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength)."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" stroke-dasharray=\"4 4\"/>\n");
*/

		fwrite($im, "<line x1=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength)."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" />\n");

		fwrite($im, "<line x1=\"".($Corner1X - $vMiddle1[0] - $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" stroke-dasharray=\"4 4\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength + 4)."\" y=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");

	

	}

fwrite($im, "<text class=\"text\" x=\"".($Corner3X)."\" y=\"".($Corner3Y+30)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">N</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner1X-40)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">R</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner2X+20)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">W</text>");
fwrite($im, "</g>\n\n");

	fwrite($im, "</g>\n\n");


	fwrite($im,"\n\n");
	
	fwrite($im, "<line x1=\"".($Corner1X)."\" y1=\"".($Corner1Y)."\" x2=\"".($Corner2X)."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2 + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner2X)."\" y1=\"".($Corner2Y)."\" x2=\"".($Corner3X)."\" y2=\"".($Corner3Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner3X)."\" y1=\"".($Corner3Y)."\" x2=\"".($Corner1X)."\" y2=\"".($Corner1Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");


	fwrite($im,"\n\n");

	for($i = 0; $i < sizeof($vDataPoints); $i++)
	{
		$cx = $vDataPoints[$i][0];
		$cy = $vDataPoints[$i][1];
	
		//echo $vDataPoints[$i][0]." - ".$vDataPoints[$i][1]."\n";
	
		$color = $vColorPoints[$i];

		fwrite($im, "<circle cx=\"".$cx."\"  cy=\"".$cy."\"  r=\"1\" stroke=\"".$color."\" fill=\"".$color."\" fill-opacity=\"0.2\" stroke-opacity=\"0.2\" stroke-width=\"0\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\"/>\n");	
	
	}


	fwrite($im,"</svg>");
		
	fclose($im);	

	fclose($fhCoord);

}

//plots 3-state triangle with the color coded standard deviation of the end position of the simulation 
//the standard deviation is taken from a summary file in the same directory
function trianglePlotSTD($sCoordfile, $sFileName)
{
	$fScale = 300.0;

	$iTicMax = 60;
	$vDataPoints = array();
	$vColorPoints = array();
	$vOpacityPoints = array();
	$vData = array();
	$fLegendDistance = 20;

	$fhCoord = fopen($sCoordfile,"r");

	$picfilename = $sFileName;

	while (($buffer = fgets($fhCoord, 4096)) !== false) 
	{
		$vTMP = explode("\t",$buffer);
	
		array_push($vDataPoints, array($vTMP[1], $vTMP[2], $vTMP[0]));
	}


	$fhSum = fopen("summary","r");
	
	if(!$fhSum)
	{
		echo "ERROR! No summary file!";
		die(0);
	}

	$picfilename = $sFileName;

	while (($buffer = fgets($fhSum, 4096)) !== false) 
	{
		$vTMP = explode("\t",$buffer);
	
		array_push($vData, $vTMP);
	}

	$fMaxSTD = 0;
	
	for($i = 0; $i < sizeof($vData); $i++)
	{
		$fMaxSTD = max($fMaxSTD, $vData[$i][2]);
	}

	//transition colors

	$r1 = 0;
	$g1 = 255;
	$b1 = 0;
	$r2 = 0;
	$g2 = 0;
	$b2 = 255;


	$color = "black";
	$colorsteps = 255;

	$step = $fMaxSTD / $colorsteps;

//print_r($vData);
	echo "Getting STD\n";

	for($i = 0; $i < sizeof($vDataPoints); $i++)
	{
		$STD = $vData[(searchForIdV3($vDataPoints[$i][2], $vData))][2];

		//echo $vDataPoints[$i][2]." => ".$STD."\n";

		if($STD === null)
		{
			echo "Data error, no entry in summary!\n";
			die(0);
		}

		$iNumStep = floor($STD / $step);

		//m[RED]   = (ColorRight[RED]   - ColorLeft[RED])   / PixelsWidthAttemptingToFillIn
		//m[GREEN] = (ColorRight[GREEN] - ColorLeft[GREEN]) / PixelsWidthAttemptingToFillIn
		//m[BLUE]  = (ColorRight[BLUE]  - ColorLeft[BLUE])  / PixelsWidthAttemptingToFillIn

		//b[RED]   = ColorLeft[RED]
		//b[GREEN] = ColorLeft[GREEN]
		//b[BLUE]  = ColorLeft[BLUE]

		//NewCol[pixelXFromLeft][RED]   = m[RED]   * pixelXFromLeft + ColorLeft[RED]    
		//NewCol[pixelXFromLeft][GREEN] = m[GREEN] * pixelXFromLeft + ColorLeft[GREEN]
		//NewCol[pixelXFromLeft][BLUE]  = m[BLUE]  * pixelXFromLeft + ColorLeft[BLUE]
		//////////////////////////////////////////////////////////////////////////////////////////
		//$p = $iNumStep / $colorsteps - 1;

	    //$r = floor((1.0-$p) * $r1 + $p * $r2 + 0.5);
    	//$g = floor((1.0-$p) * $g1 + $p * $g2 + 0.5);
    	//$b = floor((1.0-$p) * $b1 + $p * $b2 + 0.5);

		//$color = "rgb (".$r." ".$g." ".$b.")";

		

		$value = $STD / $fMaxSTD;

		if($value > 0.5)
		{
			$pixred = round(512-$value*512);
			$pixgreen = 255;
			$pixblue = 0;	
		}			
		else
		{
			$pixred = 255;
			$pixgreen = round(512*$value);
			$pixblue = 0;
		}

		//$pixred = round(255*$value);
		//$pixgreen = round(255 - 255*$value);
		//$pixblue = 0;	
		


		$color = sprintf("#%02x%02x%02x", $pixred, $pixgreen, $pixblue); // lets try hex

		//$color = sprintf("#%02x%02x%02x", $r, $g, $b); // lets try hex

		//rgb is wierd
		//$color = "rgb(".$pixred.", ".$pixgreen.", ".$pixblue.")";

		echo $i." of ".sizeof($vDataPoints)."\n";

		array_push($vColorPoints, $color);
	}


	$vAxisX = array(array($fScale),array(0),array(0));
	$vAxisY = array(array(0),array($fScale),array(0));
	$vAxisZ = array(array(0),array(0),array($fScale));

	$vRotated11 = RotateZ($vAxisX,deg2rad(-45));
	$vRotated12 = RotateZ($vAxisY,deg2rad(-45));
	$vRotated13 = RotateZ($vAxisZ,deg2rad(-45));

	$vRotated21 = RotateY($vRotated11,deg2rad(-54.73));
	$vRotated22 = RotateY($vRotated12,deg2rad(-54.73));
	$vRotated23 = RotateY($vRotated13,deg2rad(-54.73));

	$vRotated21 = RotateZ($vRotated21,deg2rad(-90));
	$vRotated22 = RotateZ($vRotated22,deg2rad(-90));
	$vRotated23 = RotateZ($vRotated23,deg2rad(-90));

	$max_x = max(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$min_x = min(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$max_y = max(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));
	$min_y = min(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));

	$margin = 0;

	$margin = 200;

	//$shift_y = -61;
	$shift_y = 0;
	//$margin = 40.0 * $fScale * 0.05;
	$dim_x = $max_x - $min_x;// + $marginX;
	$dim_y = $max_y - $min_y;// + $marginY;

	$im = fopen($picfilename.".svg","w");
	

	fwrite($im,"<?xml version=\"1.0\" standalone=\"no\"?>\n");
	fwrite($im,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
	fwrite($im,"<svg width=\"".($dim_x + $margin)."px\" height=\"".($dim_y + $margin)."px\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewport-fill=\"black\">\n\n");
	fwrite($im,"<style>\n.text {font-family: Helvetica, Arial, Tahoma, Geneva, sans-serif; font: Arial, sans-serif;}\n</style>\n");



	fwrite($im, "<rect x=\"0\" y=\"0\" width=\"".($dim_x + $margin)."\" height=\"".($dim_y + $margin)."\" style=\"fill: white;\"/>\n\n");
	//draw axes
		
	$Corner1X = $vRotated21[0][0];
	$Corner1Y = $vRotated21[1][0];
	$Corner2X = $vRotated22[0][0];
	$Corner2Y = $vRotated22[1][0];
	$Corner3X = $vRotated23[0][0];
	$Corner3Y = $vRotated23[1][0];



	//draw legend

	$fNormalization = sqrt(($Corner1X - $Corner2X)**2 + ($Corner1Y - $Corner2Y)**2);

	$vDisplacement1 = array(0,0);
	$vDirection1 = array(0,0);

	$vDirection1[0] = (($Corner1X - $Corner2X) / $fNormalization);
	$vDirection1[1] = (($Corner1Y - $Corner2Y) / $fNormalization);

	$vDisplacement1[0] = (($Corner1X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement1[1] = (($Corner1Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement2 = array(0,0);
	$vDirection2 = array(0,0);

	$vDirection2[0] = (($Corner3X - $Corner2X) / $fNormalization);
	$vDirection2[1] = (($Corner3Y - $Corner2Y) / $fNormalization);

	$vDisplacement2[0] = (($Corner3X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement2[1] = (($Corner3Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement3 = array(0,0);
	$vDirection3 = array(0,0);

	$vDirection3[0] = (($Corner3X - $Corner1X) / $fNormalization);
	$vDirection3[1] = (($Corner3Y - $Corner1Y) / $fNormalization);

	$vDisplacement3[0] = (($Corner3X - $Corner1X) / $fNormalization) * $fLegendDistance;
	$vDisplacement3[1] = (($Corner3Y - $Corner1Y) / $fNormalization) * $fLegendDistance;	

//type 1 parallel
/*
	fwrite($im, '<line x1="'.($Corner1X + $vDisplacement2[0]).'" y1="'.($Corner1Y - $vDisplacement2[1]).'" x2="'.($Corner2X - $vDisplacement2[0]).'" y2="'.($Corner2Y - $vDisplacement2[1]).'" style="stroke:black; stroke-width:2px;" transform="translate('.(($dim_x + $margin) / 2 - $vMiddle3[0]).' '.(($dim_y + $margin) / 2 + $shift_y - $vMiddle3[1]).')"/>');
	fwrite($im, '<line x1="'.($Corner2X - $vDisplacement1[0]).'" y1="'.($Corner2Y + $vDisplacement1[1]).'" x2="'.($Corner3X + $vDisplacement3[0]).'" y2="'.($Corner3Y + $vDisplacement3[1]).'" style="stroke:black; stroke-width:2px;" transform="translate('.(($dim_x + $margin) / 2 - $vMiddle3[0]).' '.(($dim_y + $margin) / 2 + $shift_y - $vMiddle3[1]).')"/>');
	fwrite($im, '<line x1="'.($Corner3X + $vDisplacement2[0]).'" y1="'.($Corner3Y + $vDisplacement2[1]).'" x2="'.($Corner1X + $vDisplacement1[0]).'" y2="'.($Corner1Y + $vDisplacement1[1]).'" style="stroke:black; stroke-width:2px;" transform="translate('.(($dim_x + $margin) / 2 - $vMiddle3[0]).' '.(($dim_y + $margin) / 2 + $shift_y - $vMiddle3[1]).')"/>');
*/
//type 2 pertenticular

	$fHeight = ($Corner1X - $Corner2X);

	$vMiddle1 = array(0,0);

	$vMiddle1[0] = ($Corner1X - $Corner2X) / 2;
	$vMiddle1[1] = ($Corner1Y - $Corner2Y) / 2;

	$vMiddle2 = array(0,0);

	$vMiddle2[0] = ($Corner2X - $Corner3X) / 2;
	$vMiddle2[1] = ($Corner2Y - $Corner3Y) / 2;

	$vMiddle3 = array(0,0);

	$vMiddle3[0] = ($Corner3X - $Corner1X) / 2;
	$vMiddle3[1] = ($Corner3Y - $Corner1Y) / 2;


	fwrite($im, "<text class=\"text\" x=\"0\" y=\"".($Corner1Y - 100)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".$picfilename."</text>");

	fwrite($im, "<!-- Grid on/off, change visibility to either visibility:visible or visibility:hidden -->\n");
	fwrite($im, "<g style=\"visibility:visible\">\n\n");
//fwrite($im, "<g style=\"visibility:hidden\">\n\n");


//grid (equal amounts)
	$tics = 20;


	for($i = 0; $i < $tics / 2; $i++)
	{

	}

//orthogonal grid (tics)

$ticlength = 10;
$ticunit = $iTicMax / $tics;


	for($i = 0; $i < $tics + 1; $i++)
	{
		

		fwrite($im, "<line x1=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[0])."\" y1=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[1])."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics))."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[0])."\" y=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");


		fwrite($im, "<line x1=\"".($Corner3X - $i * $vDirection2[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection2[1] * ($fNormalization / $tics))."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 4) * $vDirection3[0])."\" y2=\"".($Corner1Y - ($ticlength + 4) * $vDirection3[1])."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 8) * $vDirection3[0])."\" y=\"".($Corner1Y - ($ticlength + 8) * $vDirection3[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($i * $ticunit)."</text>");


/*		fwrite($im, "<line x1=\"".($Corner1X - $vMiddle1[0] - $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength)."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.4\"/>\n");
*/

		fwrite($im, "<line x1=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength)."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" />\n");

		fwrite($im, "<line x1=\"".($Corner1X - $vMiddle1[0] - $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" stroke-dasharray=\"4 4\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength + 4)."\" y=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");

	

}

/*
fwrite($im, "<text class=\"text\" x=\"".($Corner2X - 25)."\" y=\"".($Corner2Y - $vMiddle2[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">unmodified Nucleosomes</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner1X - 65)."\" y=\"".($Corner2Y - $vMiddle2[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">correct Nucleosomes</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner1X - $vMiddle1[0] - 55)."\" y=\"".($Corner1Y - 45)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">incorrect Nucleosomes</text>");
*/

fwrite($im, "<text class=\"text\" x=\"".($Corner3X)."\" y=\"".($Corner3Y+30)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">N</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner1X-40)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">R</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner2X+20)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">W</text>");
fwrite($im, "</g>\n\n");




//fwrite($im, '<circle cx="0"  cy="0"  r="10" stroke="green" fill="green" fill-opacity="0.7" stroke-opacity="0.7" stroke-width="0" transform="translate('.(($dim_x + $margin) / 2).' '.(($dim_y + $margin) / 2).')"/>');

fwrite($im,"\n\n");
	
	fwrite($im, "<line x1=\"".($Corner1X)."\" y1=\"".($Corner1Y)."\" x2=\"".($Corner2X)."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2 + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner2X)."\" y1=\"".($Corner2Y)."\" x2=\"".($Corner3X)."\" y2=\"".($Corner3Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner3X)."\" y1=\"".($Corner3Y)."\" x2=\"".($Corner1X)."\" y2=\"".($Corner1Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");



fwrite($im,"\n\n");

	for($i = 0; $i < sizeof($vDataPoints); $i++)
	{
		$cx = $vDataPoints[$i][0];
		$cy = $vDataPoints[$i][1];
	
		//echo $vDataPoints[$i][0]." - ".$vDataPoints[$i][1]."\n";
	
		$color = $vColorPoints[$i];
		$opa = $vOpacityPoints[$i];

		fwrite($im, "<circle cx=\"".$cx."\"  cy=\"".$cy."\"  r=\"1\" stroke=\"".$color."\" fill=\"".$color."\" fill-opacity=\"0.2\" stroke-opacity=\"".$opa."\" stroke-width=\"0\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\"/>\n");	
	
	}


	fwrite($im,"</svg>");
		
	fclose($im);	

	fclose($fhCoord);

}

//stroke-dasharray:2,4;stroke-dashoffset:0
//format hash\txcoord\tycoord\tcolor\topacity


//plots 3-state triangle with color information for each simulation included in $sCoordFile
//image is plotted as $sFileName.svg
function trianglePlotColorFile($sCoordfile, $sFileName)
{
	$fScale = 300.0;

	$iTicMax = 60;
	$vDataPoints = array();
	$vColorPoints = array();
	$vOpacityPoints = array();

	$fLegendDistance = 20;


	//add vector support

	if(gettype($sCoordfile) == "string")
	{
		$fhCoord = fopen($sCoordfile,"r");

		$picfilename = $sFileName;

		while (($buffer = fgets($fhCoord, 4096)) !== false) 
		{
			$vTMP = explode("\t",$buffer);
	
			array_push($vDataPoints, array($vTMP[1], $vTMP[2]));

			array_push($vColorPoints, $vTMP[3]);

			array_push($vOpacityPoints, $vTMP[4]);
		
		}
	}
	elseif(gettype($sCoordfile) == "array")
	{
		$vDataPoints = $sCoordfile[0];
		$vColorPoints = $sCoordfile[1];
		$vOpacityPoints = $sCoordfile[2];
	}
	else
	{
		echo "Input Error!";
		die(0);
	}
	//$vDataPoints = array_unique($vDataPoints, SORT_REGULAR);

	//$vDataPoints = array_map("unserialize", array_unique(array_map("serialize", $vDataPoints)));

//print_r($vDataPoints);

//die(0);

	$vAxisX = array(array($fScale),array(0),array(0));
	$vAxisY = array(array(0),array($fScale),array(0));
	$vAxisZ = array(array(0),array(0),array($fScale));

	$vRotated11 = RotateZ($vAxisX,deg2rad(-45));
	$vRotated12 = RotateZ($vAxisY,deg2rad(-45));
	$vRotated13 = RotateZ($vAxisZ,deg2rad(-45));

	$vRotated21 = RotateY($vRotated11,deg2rad(-54.73));
	$vRotated22 = RotateY($vRotated12,deg2rad(-54.73));
	$vRotated23 = RotateY($vRotated13,deg2rad(-54.73));

	$vRotated21 = RotateZ($vRotated21,deg2rad(-90));
	$vRotated22 = RotateZ($vRotated22,deg2rad(-90));
	$vRotated23 = RotateZ($vRotated23,deg2rad(-90));

	$max_x = max(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$min_x = min(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$max_y = max(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));
	$min_y = min(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));

	$margin = 0;

	$margin = 200;

	//$shift_y = -61;
	$shift_y = 0;
	//$margin = 40.0 * $fScale * 0.05;
	$dim_x = $max_x - $min_x;// + $marginX;
	$dim_y = $max_y - $min_y;// + $marginY;

	$im = fopen($picfilename.".svg","w");
	

	fwrite($im,"<?xml version=\"1.0\" standalone=\"no\"?>\n");
	fwrite($im,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
	fwrite($im,"<svg width=\"".($dim_x + $margin)."px\" height=\"".($dim_y + $margin)."px\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewport-fill=\"black\">\n\n");
	fwrite($im,"<style>\n.text {font-family: Helvetica, Arial, Tahoma, Geneva, sans-serif; font: Arial, sans-serif;}\n</style>\n");



	fwrite($im, "<rect x=\"0\" y=\"0\" width=\"".($dim_x + $margin)."\" height=\"".($dim_y + $margin)."\" style=\"fill: white;\"/>\n\n");
	//draw axes
		
	$Corner1X = $vRotated21[0][0];
	$Corner1Y = $vRotated21[1][0];
	$Corner2X = $vRotated22[0][0];
	$Corner2Y = $vRotated22[1][0];
	$Corner3X = $vRotated23[0][0];
	$Corner3Y = $vRotated23[1][0];



	//draw legend

	$fNormalization = sqrt(($Corner1X - $Corner2X)**2 + ($Corner1Y - $Corner2Y)**2);

	$vDisplacement1 = array(0,0);
	$vDirection1 = array(0,0);

	$vDirection1[0] = (($Corner1X - $Corner2X) / $fNormalization);
	$vDirection1[1] = (($Corner1Y - $Corner2Y) / $fNormalization);

	$vDisplacement1[0] = (($Corner1X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement1[1] = (($Corner1Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement2 = array(0,0);
	$vDirection2 = array(0,0);

	$vDirection2[0] = (($Corner3X - $Corner2X) / $fNormalization);
	$vDirection2[1] = (($Corner3Y - $Corner2Y) / $fNormalization);

	$vDisplacement2[0] = (($Corner3X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement2[1] = (($Corner3Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement3 = array(0,0);
	$vDirection3 = array(0,0);

	$vDirection3[0] = (($Corner3X - $Corner1X) / $fNormalization);
	$vDirection3[1] = (($Corner3Y - $Corner1Y) / $fNormalization);

	$vDisplacement3[0] = (($Corner3X - $Corner1X) / $fNormalization) * $fLegendDistance;
	$vDisplacement3[1] = (($Corner3Y - $Corner1Y) / $fNormalization) * $fLegendDistance;	



	$fHeight = ($Corner1X - $Corner2X);

	$vMiddle1 = array(0,0);

	$vMiddle1[0] = ($Corner1X - $Corner2X) / 2;
	$vMiddle1[1] = ($Corner1Y - $Corner2Y) / 2;

	$vMiddle2 = array(0,0);

	$vMiddle2[0] = ($Corner2X - $Corner3X) / 2;
	$vMiddle2[1] = ($Corner2Y - $Corner3Y) / 2;

	$vMiddle3 = array(0,0);

	$vMiddle3[0] = ($Corner3X - $Corner1X) / 2;
	$vMiddle3[1] = ($Corner3Y - $Corner1Y) / 2;


fwrite($im, "<text class=\"text\" x=\"0\" y=\"".($Corner1Y - 100)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".$picfilename."</text>");

fwrite($im, "<!-- Grid on/off, change visibility to either visibility:visible or visibility:hidden -->\n");
fwrite($im, "<g style=\"visibility:visible\">\n\n");
//fwrite($im, "<g style=\"visibility:hidden\">\n\n");


//grid (equal amounts)
$tics = 20;


//orthogonal grid (tics)

$ticlength = 10;
$ticunit = $iTicMax / $tics;


	for($i = 0; $i < $tics + 1; $i++)
	{
		

		fwrite($im, "<line x1=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[0])."\" y1=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[1])."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics))."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[0])."\" y=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");


		fwrite($im, "<line x1=\"".($Corner3X - $i * $vDirection2[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection2[1] * ($fNormalization / $tics))."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 4) * $vDirection3[0])."\" y2=\"".($Corner1Y - ($ticlength + 4) * $vDirection3[1])."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 8) * $vDirection3[0])."\" y=\"".($Corner1Y - ($ticlength + 8) * $vDirection3[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($i * $ticunit)."</text>");


/*		fwrite($im, "<line x1=\"".($Corner1X - $vMiddle1[0] - $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength)."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px; stroke-dasharray:2,4; stroke-dashoffset:0\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" />\n");
*/

		fwrite($im, "<line x1=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength)."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" />\n");

		fwrite($im, "<line x1=\"".($Corner1X - $vMiddle1[0] - $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" stroke-dasharray=\"4 4\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength + 4)."\" y=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");

	

}

fwrite($im, "<text class=\"text\" x=\"".($Corner3X)."\" y=\"".($Corner3Y+30)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">N</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner1X-40)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">R</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner2X+20)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">W</text>");
fwrite($im, "</g>\n\n");




//fwrite($im, '<circle cx="0"  cy="0"  r="10" stroke="green" fill="green" fill-opacity="0.7" stroke-opacity="0.7" stroke-width="0" transform="translate('.(($dim_x + $margin) / 2).' '.(($dim_y + $margin) / 2).')"/>');

fwrite($im,"\n\n");
	
	fwrite($im, "<line x1=\"".($Corner1X)."\" y1=\"".($Corner1Y)."\" x2=\"".($Corner2X)."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2 + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner2X)."\" y1=\"".($Corner2Y)."\" x2=\"".($Corner3X)."\" y2=\"".($Corner3Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner3X)."\" y1=\"".($Corner3Y)."\" x2=\"".($Corner1X)."\" y2=\"".($Corner1Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");



fwrite($im,"\n\n");

	for($i = 0; $i < sizeof($vDataPoints); $i++)
	{
		$cx = $vDataPoints[$i][0];
		$cy = $vDataPoints[$i][1];
	
		//echo $vDataPoints[$i][0]." - ".$vDataPoints[$i][1]."\n";
	
		$color = $vColorPoints[$i];
		$opacity = $vOpacityPoints[$i];

		//test
		//if($opacity == 0 || $color != "magenta")
		//	continue;

		fwrite($im, "<circle cx=\"".$cx."\"  cy=\"".$cy."\"  r=\"1\" stroke=\"".$color."\" fill=\"".$color."\" fill-opacity=\"".floatval($opacity)."\" stroke-opacity=\"".$opacity."\" stroke-width=\"0\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\"/>\n");	
	
	}


	fwrite($im,"</svg>");
		
	fclose($im);	

	fclose($fhCoord);

}

//same function as trianglePlotColorSelGen but with selectable simulation selection file
//selection file format: hash\tcolor
function trianglePlotColorSel($sCoordfile, $sSelFile, $sFileName)
{
	$fScale = 300.0;

	$iTicMax = 60;
	$vDataPoints = array();
	$vColorPoints = array();
	$vCoordPoints = array();

	$fLegendDistance = 20;
	$vRules = array();
	$sMode = "";

	$picfilename = $sFileName;

	if(gettype($sCoordfile) == "string")
	{
		$sMode = "file";
		$fhCoord = fopen($sCoordfile,"r");


		while (($buffer = fgets($fhCoord, 4096)) !== false) 
		{
			$vTMP = explode("\t",$buffer);
			//x,y,#true,#false,#neutral,name
			array_push($vCoordPoints, array($vTMP[1], $vTMP[2], $vTMP[0]));
		}
	}
	elseif(gettype($sCoordfile) == "array")
	{
		$sMode = "vector";
		$vDataPoints = $sCoordfile;
	}
	else
	{
		echo "Input Error!";
		die(0);
	}

	
	if($sMode = "string")
	{
		$fhColorSel = fopen($sSelFile,"r");

		$vSelectedSims = array();

		echo "Processing simulation selection...\n";

		while (($buffer = fgets($fhColorSel, 4096)) !== false) 
		{
			$vTMP = explode("\t",$buffer);

			array_push($vSelectedSims, array($vTMP[0], $vTMP[1]));

		}
		
		//$vColorPoints = array_fill(0,sizeof($vSelectedSims),"black");

		$iInd = 0;
		
		for($k = 0; $k < sizeof($vCoordPoints); $k++)
		{
			//echo ($k/sizeof($vCoordPoints) * 100)."%\n";
			for($l = 0; $l < sizeof($vSelectedSims); $l++)
			{
				if($vCoordPoints[$k][2] == $vSelectedSims[$l][0])
				{						
					array_push($vDataPoints, array($vCoordPoints[$k][0], $vCoordPoints[$k][1]));
					//$vColorPoints[$iInd] = $vSelectedSims[$l][1];
					array_push($vColorPoints, $vSelectedSims[$l][1]);
					$iInd++;
				}
			}
		}
	}
	elseif($sMode = "array")
	{
		$vDataPoints = $sCoordfile;
		
		$vColorPoints = array_fill(0,sizeof($vDataPoints),"black");

		for($k = 0; $k < sizeof($vDataPoints); $k++)
		{
			for($l = 0; $l < sizeof($vSelectedSims); $l++)
			{
				if($vDataPoints[$k][5] == $vSelectedSims[$l][0])
				{						
					array_push($vSelectedSims, array($vDataPoints[$k][5], $sColor));
					$vColorPoints[$k] = $sColor;
				}
			}
		}
	}
	else
	{
		echo "No simulation selection, continuing...\n";
	}


	//array_multisort (array_column($vSelectedSims, 0), SORT_ASC, $vSelectedSims);
	//array_multisort (array_column($vSelectedSims, 0), SORT_ASC, $vSelectedSims);
	//print_r($vDataPoints);
	//print_r($vSelectedSims);
	
	

	$vAxisX = array(array($fScale),array(0),array(0));
	$vAxisY = array(array(0),array($fScale),array(0));
	$vAxisZ = array(array(0),array(0),array($fScale));

	$vRotated11 = RotateZ($vAxisX,deg2rad(-45));
	$vRotated12 = RotateZ($vAxisY,deg2rad(-45));
	$vRotated13 = RotateZ($vAxisZ,deg2rad(-45));

	$vRotated21 = RotateY($vRotated11,deg2rad(-54.73));
	$vRotated22 = RotateY($vRotated12,deg2rad(-54.73));
	$vRotated23 = RotateY($vRotated13,deg2rad(-54.73));

	$vRotated21 = RotateZ($vRotated21,deg2rad(-90));
	$vRotated22 = RotateZ($vRotated22,deg2rad(-90));
	$vRotated23 = RotateZ($vRotated23,deg2rad(-90));

	$max_x = max(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$min_x = min(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$max_y = max(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));
	$min_y = min(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));

	$margin = 0;

	$margin = 200;

	//$shift_y = -61;
	$shift_y = 0;
	//$margin = 40.0 * $fScale * 0.05;
	$dim_x = $max_x - $min_x;// + $marginX;
	$dim_y = $max_y - $min_y;// + $marginY;

	$im = fopen($picfilename.".svg","w");
	

	fwrite($im,"<?xml version=\"1.0\" standalone=\"no\"?>\n");
	fwrite($im,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
	fwrite($im,"<svg width=\"".($dim_x + $margin)."px\" height=\"".($dim_y + $margin)."px\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewport-fill=\"black\">\n\n");
	fwrite($im,"<style>\n.text {font-family: Helvetica, Arial, Tahoma, Geneva, sans-serif; font: Arial, sans-serif;}\n</style>\n");



	fwrite($im, "<rect x=\"0\" y=\"0\" width=\"".($dim_x + $margin)."\" height=\"".($dim_y + $margin)."\" style=\"fill: white;\"/>\n\n");
	//draw axes
		
	$Corner1X = $vRotated21[0][0];
	$Corner1Y = $vRotated21[1][0];
	$Corner2X = $vRotated22[0][0];
	$Corner2Y = $vRotated22[1][0];
	$Corner3X = $vRotated23[0][0];
	$Corner3Y = $vRotated23[1][0];



	//draw legend

	$fNormalization = sqrt(($Corner1X - $Corner2X)**2 + ($Corner1Y - $Corner2Y)**2);

	$vDisplacement1 = array(0,0);
	$vDirection1 = array(0,0);

	$vDirection1[0] = (($Corner1X - $Corner2X) / $fNormalization);
	$vDirection1[1] = (($Corner1Y - $Corner2Y) / $fNormalization);

	$vDisplacement1[0] = (($Corner1X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement1[1] = (($Corner1Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement2 = array(0,0);
	$vDirection2 = array(0,0);

	$vDirection2[0] = (($Corner3X - $Corner2X) / $fNormalization);
	$vDirection2[1] = (($Corner3Y - $Corner2Y) / $fNormalization);

	$vDisplacement2[0] = (($Corner3X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement2[1] = (($Corner3Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement3 = array(0,0);
	$vDirection3 = array(0,0);

	$vDirection3[0] = (($Corner3X - $Corner1X) / $fNormalization);
	$vDirection3[1] = (($Corner3Y - $Corner1Y) / $fNormalization);

	$vDisplacement3[0] = (($Corner3X - $Corner1X) / $fNormalization) * $fLegendDistance;
	$vDisplacement3[1] = (($Corner3Y - $Corner1Y) / $fNormalization) * $fLegendDistance;	



	$fHeight = ($Corner1X - $Corner2X);

	$vMiddle1 = array(0,0);

	$vMiddle1[0] = ($Corner1X - $Corner2X) / 2;
	$vMiddle1[1] = ($Corner1Y - $Corner2Y) / 2;

	$vMiddle2 = array(0,0);

	$vMiddle2[0] = ($Corner2X - $Corner3X) / 2;
	$vMiddle2[1] = ($Corner2Y - $Corner3Y) / 2;

	$vMiddle3 = array(0,0);

	$vMiddle3[0] = ($Corner3X - $Corner1X) / 2;
	$vMiddle3[1] = ($Corner3Y - $Corner1Y) / 2;


	fwrite($im, "<!-- Grid on/off, change visibility to either visibility:visible or visibility:hidden -->\n");
	fwrite($im, "<g style=\"visibility:visible\">\n\n");
//fwrite($im, "<g style=\"visibility:hidden\">\n\n");


//grid (equal amounts)
	$tics = 20;

//orthogonal grid (tics)

	$ticlength = 10;
	$ticunit = $iTicMax / $tics;


	for($i = 0; $i < $tics + 1; $i++)
	{
		

		fwrite($im, "<line x1=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[0])."\" y1=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[1])."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics))."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[0])."\" y=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");


		fwrite($im, "<line x1=\"".($Corner3X - $i * $vDirection2[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection2[1] * ($fNormalization / $tics))."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 4) * $vDirection3[0])."\" y2=\"".($Corner1Y - ($ticlength + 4) * $vDirection3[1])."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 8) * $vDirection3[0])."\" y=\"".($Corner1Y - ($ticlength + 8) * $vDirection3[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($i * $ticunit)."</text>");


/*		fwrite($im, "<line x1=\"".($Corner1X - $vMiddle1[0] - $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength)."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.4\"/>\n");
*/

		fwrite($im, "<line x1=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength)."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" />\n");

		fwrite($im, "<line x1=\"".($Corner1X - $vMiddle1[0] - $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" stroke-dasharray=\"4 4\"/>\n");


		fwrite($im, "<text class=\"text\" x=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength + 4)."\" y=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");

	

	}

fwrite($im, "<text class=\"text\" x=\"".($Corner3X)."\" y=\"".($Corner3Y+30)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">N</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner1X-40)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">R</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner2X+20)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">W</text>");
fwrite($im, "</g>\n\n");


	fwrite($im,"\n\n");
	
	fwrite($im, "<line x1=\"".($Corner1X)."\" y1=\"".($Corner1Y)."\" x2=\"".($Corner2X)."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2 + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner2X)."\" y1=\"".($Corner2Y)."\" x2=\"".($Corner3X)."\" y2=\"".($Corner3Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner3X)."\" y1=\"".($Corner3Y)."\" x2=\"".($Corner1X)."\" y2=\"".($Corner1Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");


	fwrite($im,"\n\n");

	for($i = 0; $i < sizeof($vDataPoints); $i++)
	{
		$cx = $vDataPoints[$i][0];
		$cy = $vDataPoints[$i][1];
	
		//echo $vDataPoints[$i][0]." - ".$vDataPoints[$i][1]."\n";
	
		$color = $vColorPoints[$i];

		fwrite($im, "<circle cx=\"".$cx."\"  cy=\"".$cy."\"  r=\"1\" stroke=\"".$color."\" fill=\"".$color."\" fill-opacity=\"0.2\" stroke-opacity=\"0.2\" stroke-width=\"0\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\"/>\n");	
	
	}


	fwrite($im,"</svg>");
		
	fclose($im);	

	fclose($fhCoord);

}
//stroke-dasharray:2,4;stroke-dashoffset:0

//plots single simulation trajectory in a triangle plot
//the position for each point of the trajectory is calculated at a specific time 
//the timesteps are of fixed width from start to end of the simulation calculated by the variables 
//$tics = 20;
//$ticlength = 10;
//$iTicMax = 60;
//$ticunit = $iTicMax / $tics;


function trianglePlotTrace($vDataPoints, $sFileName)
{
	$fScale = 300.0;

	$iTicMax = 60;



	$fLegendDistance = 20;



	
	$vAxisX = array(array($fScale),array(0),array(0));
	$vAxisY = array(array(0),array($fScale),array(0));
	$vAxisZ = array(array(0),array(0),array($fScale));

	$vRotated11 = RotateZ($vAxisX,deg2rad(-45));
	$vRotated12 = RotateZ($vAxisY,deg2rad(-45));
	$vRotated13 = RotateZ($vAxisZ,deg2rad(-45));

	$vRotated21 = RotateY($vRotated11,deg2rad(-54.73));
	$vRotated22 = RotateY($vRotated12,deg2rad(-54.73));
	$vRotated23 = RotateY($vRotated13,deg2rad(-54.73));

	$vRotated21 = RotateZ($vRotated21,deg2rad(-90));
	$vRotated22 = RotateZ($vRotated22,deg2rad(-90));
	$vRotated23 = RotateZ($vRotated23,deg2rad(-90));

	$max_x = max(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$min_x = min(array($vRotated21[0][0], $vRotated22[0][0], $vRotated23[0][0]));
	$max_y = max(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));
	$min_y = min(array($vRotated21[1][0], $vRotated22[1][0], $vRotated23[1][0]));

	$margin = 0;

	$margin = 200;

	//$shift_y = -61;
	$shift_y = 0;
	//$margin = 40.0 * $fScale * 0.05;
	$dim_x = $max_x - $min_x;// + $marginX;
	$dim_y = $max_y - $min_y;// + $marginY;

	$im = fopen($sFileName.".svg","w");
	

	fwrite($im,"<?xml version=\"1.0\" standalone=\"no\"?>\n");
	fwrite($im,"<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
	fwrite($im,"<svg width=\"".($dim_x + $margin)."px\" height=\"".($dim_y + $margin)."px\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewport-fill=\"black\">\n\n");
	fwrite($im,"<style>\n.text {font-family: Helvetica, Arial, Tahoma, Geneva, sans-serif; font: Arial, sans-serif;}\n</style>\n");



	fwrite($im, "<rect x=\"0\" y=\"0\" width=\"".($dim_x + $margin)."\" height=\"".($dim_y + $margin)."\" style=\"fill: white;\"/>\n\n");
	//draw axes
		
	$Corner1X = $vRotated21[0][0];
	$Corner1Y = $vRotated21[1][0];
	$Corner2X = $vRotated22[0][0];
	$Corner2Y = $vRotated22[1][0];
	$Corner3X = $vRotated23[0][0];
	$Corner3Y = $vRotated23[1][0];



	//draw legend

	$fNormalization = sqrt(($Corner1X - $Corner2X)**2 + ($Corner1Y - $Corner2Y)**2);

	$vDisplacement1 = array(0,0);
	$vDirection1 = array(0,0);

	$vDirection1[0] = (($Corner1X - $Corner2X) / $fNormalization);
	$vDirection1[1] = (($Corner1Y - $Corner2Y) / $fNormalization);

	$vDisplacement1[0] = (($Corner1X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement1[1] = (($Corner1Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement2 = array(0,0);
	$vDirection2 = array(0,0);

	$vDirection2[0] = (($Corner3X - $Corner2X) / $fNormalization);
	$vDirection2[1] = (($Corner3Y - $Corner2Y) / $fNormalization);

	$vDisplacement2[0] = (($Corner3X - $Corner2X) / $fNormalization) * $fLegendDistance;
	$vDisplacement2[1] = (($Corner3Y - $Corner2Y) / $fNormalization) * $fLegendDistance;	

	$vDisplacement3 = array(0,0);
	$vDirection3 = array(0,0);

	$vDirection3[0] = (($Corner3X - $Corner1X) / $fNormalization);
	$vDirection3[1] = (($Corner3Y - $Corner1Y) / $fNormalization);

	$vDisplacement3[0] = (($Corner3X - $Corner1X) / $fNormalization) * $fLegendDistance;
	$vDisplacement3[1] = (($Corner3Y - $Corner1Y) / $fNormalization) * $fLegendDistance;	



	$fHeight = ($Corner1X - $Corner2X);

	$vMiddle1 = array(0,0);

	$vMiddle1[0] = ($Corner1X - $Corner2X) / 2;
	$vMiddle1[1] = ($Corner1Y - $Corner2Y) / 2;

	$vMiddle2 = array(0,0);

	$vMiddle2[0] = ($Corner2X - $Corner3X) / 2;
	$vMiddle2[1] = ($Corner2Y - $Corner3Y) / 2;

	$vMiddle3 = array(0,0);

	$vMiddle3[0] = ($Corner3X - $Corner1X) / 2;
	$vMiddle3[1] = ($Corner3Y - $Corner1Y) / 2;


fwrite($im, "<text class=\"text\" x=\"0\" y=\"".($Corner1Y - 100)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".$sFileName."</text>");

fwrite($im, "<!-- Grid on/off, change visibility to either visibility:visible or visibility:hidden -->\n");
fwrite($im, "<g style=\"visibility:visible\">\n\n");
//fwrite($im, "<g style=\"visibility:hidden\">\n\n");


//grid (equal amounts)
$tics = 20;


//orthogonal grid (tics)

$ticlength = 10;
$ticunit = $iTicMax / $tics;


	for($i = 0; $i < $tics + 1; $i++)
	{
		

		fwrite($im, "<line x1=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[0])."\" y1=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 8) * $vDirection2[1])."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics))."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.1\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X  + $i * $vDirection3[0] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[0])."\" y=\"".($Corner1Y + $i * $vDirection3[1] * ($fNormalization / $tics) + ($ticlength + 24) * $vDirection2[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");


		fwrite($im, "<line x1=\"".($Corner3X - $i * $vDirection2[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection2[1] * ($fNormalization / $tics))."\" x2=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 4) * $vDirection3[0])."\" y2=\"".($Corner1Y - ($ticlength + 4) * $vDirection3[1])."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.1\"/>\n");

		fwrite($im, "<text class=\"text\" x=\"".($Corner1X - $i * $vDirection1[0] * ($fNormalization / $tics) - ($ticlength + 8) * $vDirection3[0])."\" y=\"".($Corner1Y - ($ticlength + 8) * $vDirection3[1])."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($i * $ticunit)."</text>");


/*		fwrite($im, "<line x1=\"".($Corner1X - $vMiddle1[0] - $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength)."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.1\"/>\n");
*/

		fwrite($im, "<line x1=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength)."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" />\n");

		fwrite($im, "<line x1=\"".($Corner1X - $vMiddle1[0] - $i * $vDirection3[0] * ($fNormalization / $tics))."\" y1=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" x2=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics))."\" y2=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" stroke-opacity=\"0.2\" stroke-dasharray=\"4 4\"/>\n");


		fwrite($im, "<text class=\"text\" x=\"".($Corner2X + $vMiddle1[0] + $i * $vDirection3[0] * ($fNormalization / $tics) + $ticlength + 4)."\" y=\"".($Corner3Y - $i * $vDirection3[1] * ($fNormalization / $tics))."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\">".($iTicMax - $i * $ticunit)."</text>");

	

}

fwrite($im, "<text class=\"text\" x=\"".($Corner3X)."\" y=\"".($Corner3Y+30)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">N</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner1X-40)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">R</text>");

fwrite($im, "<text class=\"text\" x=\"".($Corner2X+20)."\" y=\"".($Corner1Y-20)."\" transform=\"translate(".(($dim_x + $margin) / 2 )." ".(($dim_y + $margin) / 2 + $shift_y ).")\" font-size=\"200%\">W</text>");
fwrite($im, "</g>\n\n");




//fwrite($im, '<circle cx="0"  cy="0"  r="10" stroke="green" fill="green" fill-opacity="0.7" stroke-opacity="0.7" stroke-width="0" transform="translate('.(($dim_x + $margin) / 2).' '.(($dim_y + $margin) / 2).')"/>');

fwrite($im,"\n\n");
	
	fwrite($im, "<line x1=\"".($Corner1X)."\" y1=\"".($Corner1Y)."\" x2=\"".($Corner2X)."\" y2=\"".($Corner2Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2 + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner2X)."\" y1=\"".($Corner2Y)."\" x2=\"".($Corner3X)."\" y2=\"".($Corner3Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");
	fwrite($im, "<line x1=\"".($Corner3X)."\" y1=\"".($Corner3Y)."\" x2=\"".($Corner1X)."\" y2=\"".($Corner1Y)."\" style=\"stroke:blue; stroke-width:2px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\" stroke-opacity=\"0.4\"/>\n");



	fwrite($im,"\n\n");

//////////////////////////
//actual data processing//
//////////////////////////



	foreach($vDataPoints as $sSim => $vSimData)
	{
		for($i = 0; $i < sizeof($vSimData); $i++)
		{

			$cx = $vSimData[$i][0];
			$cy = $vSimData[$i][1];
	
			//echo $vDataPoints[$i][0]." - ".$vDataPoints[$i][1]."\n";
	
			$color = $vSimData[$i][2];
			$opacity = $vSimData[$i][3];

			if($i == sizeof($vSimData)-1)
				$color = "#ff0000";
		//test
		//if($opacity == 0 || $color != "magenta")
		//	continue;

			fwrite($im, "<circle cx=\"".$cx."\"  cy=\"".$cy."\"  r=\"1\" stroke=\"".$color."\" fill=\"".$color."\" fill-opacity=\"".floatval($opacity)."\" stroke-opacity=\"".$opacity."\" stroke-width=\"0\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\"/>\n");	

			if($i > 2 && $i != sizeof($vSimData)-2)
				fwrite($im, "<line x1=\"".$cx."\" y1=\"".$cy."\" x2=\"".$vSimData[($i-1)][0]."\" y2=\"".$vSimData[($i-1)][1]."\" stroke-opacity=\"0.2\" style=\"stroke:".$color."; stroke-width:0.5px;\" transform=\"translate(".(($dim_x + $margin) / 2)." ".(($dim_y + $margin) / 2  + $shift_y).")\"/>\n");

		}
	
	}


	fwrite($im,"</svg>");
		
	fclose($im);	



}


function getColorGradient($fStartValue,$fMaxValue,$fValue)
{

}



//helper function to return color hex code for a specific value ($fValue) from a binned range calculated from $fstartValue and $fMaxValue
function getColorBinned($fStartValue,$fMaxValue,$fValue)	//static bin number of 5
{
	$fBinSize = ($fMaxValue - $fStartValue) / 5;

	//echo $fValue." vs.\n\t\t".$fStartValue."\n\t\t".$fMaxValue."\n\n";

	if($fValue >= $fStartValue && $fValue < ($fStartValue + $fBinSize))
		return "#d7191c";
	elseif($fValue >= ($fStartValue + $fBinSize) && $fValue < ($fStartValue + 2*$fBinSize))
		return "#fdae61";
	elseif($fValue >= ($fStartValue + 2*$fBinSize) && $fValue < ($fStartValue + 3*$fBinSize))
		return "#008837";
	elseif($fValue >= ($fStartValue + 3*$fBinSize) && $fValue < ($fStartValue + 4*$fBinSize))
		return "#2c7bb6";
	elseif($fValue >= ($fStartValue + 4*$fBinSize) && $fValue <= ($fStartValue + 5*$fBinSize))
		return "#7b3294";
	else
	{
		echo "Error: Value out of range!";
	}
}


//helper function to return color hex code for a specific value ($fValue) from a binned range specified in $vBinVec
function getColorWeightBinned($vBinVec,$fValue)
{

	//echo $fValue." vs.\n\t\t".$vBinVec[0]."\n\t\t".$vBinVec[1]."\n\t\t".$vBinVec[2]."\n\t\t".$vBinVec[3]."\n\t\t".$vBinVec[4]."\n\t\t".$vBinVec[5]."\n\n";

	if($fValue >= $vBinVec[0] && $fValue < ($vBinVec[0] + $vBinVec[1]))
		return "#d7191c";
	elseif($fValue >= ($vBinVec[0] + $vBinVec[1]) && $fValue < ($vBinVec[0] + $vBinVec[2]))
		return "#fdae61";
	elseif($fValue >= ($vBinVec[0] + $vBinVec[2]) && $fValue < ($vBinVec[0] + $vBinVec[3]))
		return "#008837";
	elseif($fValue >= ($vBinVec[0] + $vBinVec[3]) && $fValue < ($vBinVec[0] + $vBinVec[4]))
		return "#2c7bb6";
	elseif($fValue >= ($vBinVec[0] + $vBinVec[4]) && $fValue <= ($vBinVec[0] + $vBinVec[5]))
		return "#7b3294";
	else
	{
		echo "Error: Value out of range!";
	}
}


//plots nucleosome string graphically 
//state is provided as input statefile
//colors are given by $vColorCode array on the form 
//state is plotted as $sFileName.svg

function drawState($sState, $vColorCode, $sFileName)
{
	$fhStatePlotFile = fopen($sFileName,"w");
	$vTMP = explode("}",$sState);
	array_pop($vTMP);

	$iNumNuc = sizeof($vTMP);
	$vStates = array();
	$vColors = array();
	for($i = 0; $i < $iNumNuc; $i++)
	{
		$sNucState = substr($vTMP[$i],1);
		$vStates[] = $sNucState;
		if(isset($vColorCode[$sNucState]))
			$vColors[] = $vColorCode[$sNucState];
		elseif($sNucState = "")
			$vColors[] = "#FFFFFF";
		else
			$vColors[] = "#FFFFFF";
	}

	//print_r($vStates);

	$iStateWidth = 20;
	$iStateHeight = 40;

	$iMarginX = 4;
	$iMarginY = 4;
	$iDimX = 2*$iMarginX + $iNumNuc*$iStateWidth;
	$iDimY = 2*$iMarginY + $iStateHeight;


	
	$sSVG = "<?xml version=\"1.0\" standalone=\"no\"?>\n
<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n
<svg width=\"".$iDimX."px\" height=\"".$iDimY."px\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" viewport-fill=\"black\">\n\n
<style>\n.text {font-family: Helvetica, Arial, Tahoma, Geneva, sans-serif; font: Arial, sans-serif;}\n</style>\n
<rect x=\"0\" y=\"0\" width=\"".$iDimX."\" height=\"".$iDimY."\" style=\"fill: white;\"/>\n\n
";

	for($i = 0; $i < $iNumNuc; $i++)
	{
		$sSVG = $sSVG."
<rect x=\"".($iMarginX + $i*$iStateWidth)."\" y=\"".$iMarginY."\" width=\"".$iStateWidth."\" height=\"".$iStateHeight."\" stroke=\"black\" fill=\"".$vColors[$i]."\" fill-opacity=\"0.8\" stroke-opacity=\"0.8\"/>
";
	}
	$sSVG = $sSVG."</svg>";

	fwrite($fhStatePlotFile, $sSVG);
	fclose($fhStatePlotFile);
}
?>
