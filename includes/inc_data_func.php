<?php

//quadratic matix multiplication (in form of 2d arrays)
function quadMatrixMulti($mat1,$mat2)
{
	$result = [[]];
	$dim = sizeof($mat1);
	
	
	for($i = 0; $i < $dim; $i++)
	{
		for($k = 0; $k < $dim; $k++)
		{
			for($j = 0; $j < $dim; $j++)
			{
				$result[$i][$k] += $mat1[$i][$j]*$mat2[$j][$k];
			}
		}
	}
	
	
	return $result;
}


//converts HISTONE[SITE:MOD] to array
function decomposeHistone($site)
{
	if($site[sizeof($site)] == "]")	//remove last ] if existing
	{
		
	}

	$tmp1 = explode("[",$site);

	$tmp2 = explode(".",$tmp1[1]);

	//return [0]->histone, [1]->site, [2]->state
	return array($tmp1[0],$tmp2[0],$tmp2[1]);
}

//opens datafile and extracts array of iteration header information (>IT TIME NUCLEOSOME CHANNEL PROBABILITY)
function GetIterationInfo($datafile)
{

	$data = array();
	$itdata = array();
	
	$handle = fopen($datafile, "r");

	while (!feof($handle))
	{
		$buffer = fgets($handle,4096);

		if($buffer[0] == ">")
		{
			//>IT TIME NUCLEOSOME CHANNEL PROB
			$itdata = explode(" ",$buffer);
			array_push($data,$itdata);
		}
		
	}
	
	return $data;
}


//takes iteration info vector and returns vector of iteration numbers if time duration of stepwidth has passed 
function TimeToIterrationConversion($iterationinfo, $stepwidth)
{
	$selected_steps = array();
	$k = 0;

	for($i = 0; $i < sizeof($iterationinfo); $i++)
	{
		if($iterationinfo[$i][1] >= $k * $stepwidth)
		{
			array_push($selected_steps,$i);
			$k++;
		}
	}

	return $selected_steps;
}

//select iteration data from data-array with an array of iteration numbers
function GetSelectedData($selectedit, $data)
{
	$selecteddata = array();

	$selit = sizeof($selectedit);

	for($i = 0; $i < $selit; $i++)
	{
		array_push($selecteddata,$data[$selectedit[$i]]);
	}

	return $selecteddata;
}

//convert outfile format (...{...}:BOUND_ENZYME;{HISTONE[SITE:MOD]}:BOUND_ENZYME;{...}:BOUND_ENZYME;...) to state string (...{...}{HISTONE[SITE:MOD]}{...}...)
function convertOutfileStateToString($state)
{

	$vTMP = explode(";",$state);

	$converted = "";

	for($i = 0; $i < sizeof($vTMP); $i++)
	{
		$converted = $converted.explode(":",$vTMP[$i])[0];
	}

	return $converted;

}

//get size of number
function getNumberDigits($number)
{
	$result = -1;
	$modulo = -1;
	$i = 0;
	while($modulo != $number)
	{
		$modulo = $number % (1 * 10 ** $i);
		$i++;
		$result = $i - 1;
	}
	
	return $result;
}

//get leading zeros for number formatting
function leadingZeros($number, $maxnumber)
{
	$i = 0;
	while(1)
	{
		if(($maxnumber % (10**$i)) != 0)
		{
			break;	
		}	
		$i++;
	}
	return str_pad($number, $i, '0', STR_PAD_LEFT);
}

//create rule file from 2 dimensional arry
function createRuleFile($paramset,$filename)
{

$filecontent = '
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<enzymeSet>
  <enzymeDef>
    <name enabled="1" value="Noise"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac_rem"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.ac]})"/>
      <rule value="({H3[K4.un]})"/>
      <rate value="'.$paramset[0][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[0][1].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Noise"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me_rem"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.me]})"/>
      <rule value="({H3[K4.un]})"/>
      <rate value="'.$paramset[1][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[1][1].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Extender"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.un]})"/>
      <rule value="{H3[K4.ac]}({H3[K4.ac]})"/>
      <rate value="'.$paramset[2][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[2][1].'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="ac_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.un]}){H3[K4.ac]}"/>
      <rule value="({H3[K4.ac]}){H3[K4.ac]}"/>
      <rate value="'.$paramset[2][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[2][1].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Extender"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.un]})"/>
      <rule value="{H3[K4.me]}({H3[K4.me]})"/>
      <rate value="'.$paramset[3][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[3][1].'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="me_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.un]}){H3[K4.me]}"/>
      <rule value="({H3[K4.me]}){H3[K4.me]}"/>
      <rate value="'.$paramset[3][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[3][1].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Truncater"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.ac]}){H3[K4.me]}"/>
      <rule value="{H3[K4.ac]}({H3[K4.un]}){H3[K4.me]}"/>
      <rate value="'.$paramset[4][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[4][1].'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="ac_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.ac]}){H3[K4.ac]}"/>
      <rule value="{H3[K4.me]}({H3[K4.un]}){H3[K4.ac]}"/>
      <rate value="'.$paramset[4][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[4][1].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Truncater"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.me]}){H3[K4.ac]}"/>
      <rule value="{H3[K4.me]}({H3[K4.un]}){H3[K4.ac]}"/>
      <rate value="'.$paramset[5][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[5][1].'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="me_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.me]}){H3[K4.me]}"/>
      <rule value="{H3[K4.ac]}({H3[K4.un]}){H3[K4.me]}"/>
      <rate value="'.$paramset[5][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[5][1].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Filler"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ace]}({H3[K4.un]}){H3[K4.ac]}"/>
      <rule value="{H3[K4.ac]}({H3[K4.ac]}){H3[K4.ac]}"/>
      <rate value="'.$paramset[6][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[6][1].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Filler"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.un]}){H3[K4.me]}"/>
      <rule value="{H3[K4.me]}({H3[K4.me]}){H3[K4.me]}"/>
      <rate value="'.$paramset[7][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[7][1].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Corrector"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.ac]}){H3[K4.me]}"/>
      <rule value="{H3[K4.me]}({H3[K4.un]}){H3[K4.me]}"/>
      <rate value="'.$paramset[8][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[8][1].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Corrector"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.me]}){H3[K4.ac]}"/>
      <rule value="{H3[K4.ac]}({H3[K4.un]}){H3[K4.ac]}"/>
      <rate value="'.$paramset[9][0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[9][1].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
</enzymeSet>';

	$handle = fopen($filename,"w");
	fputs($handle,$filecontent);
	fclose($handle);
}


//create rule file from one dimensional arry

function createRuleFile28($paramset,$filename)
{

$filecontent = '
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<enzymeSet>
  <enzymeDef>
    <name enabled="1" value="Noise"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac_rem"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.ac]})"/>
      <rule value="({H3[K4.un]})"/>
      <rate value="'.$paramset[0].'"/>
      <dissociationRate type="absolute" value="'.$paramset[1].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Noise"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me_rem"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.me]})"/>
      <rule value="({H3[K4.un]})"/>
      <rate value="'.$paramset[2].'"/>
      <dissociationRate type="absolute" value="'.$paramset[3].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Extender"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.un]})"/>
      <rule value="{H3[K4.ac]}({H3[K4.ac]})"/>
      <rate value="'.$paramset[4].'"/>
      <dissociationRate type="absolute" value="'.$paramset[5].'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="ac_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.un]}){H3[K4.ac]}"/>
      <rule value="({H3[K4.ac]}){H3[K4.ac]}"/>
      <rate value="'.$paramset[6].'"/>
      <dissociationRate type="absolute" value="'.$paramset[7].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Extender"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.un]})"/>
      <rule value="{H3[K4.me]}({H3[K4.me]})"/>
      <rate value="'.$paramset[8].'"/>
      <dissociationRate type="absolute" value="'.$paramset[9].'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="me_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.un]}){H3[K4.me]}"/>
      <rule value="({H3[K4.me]}){H3[K4.me]}"/>
      <rate value="'.$paramset[10].'"/>
      <dissociationRate type="absolute" value="'.$paramset[11].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Truncater"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.ac]}){H3[K4.me]}"/>
      <rule value="{H3[K4.ac]}({H3[K4.un]}){H3[K4.me]}"/>
      <rate value="'.$paramset[12].'"/>
      <dissociationRate type="absolute" value="'.$paramset[13].'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="ac_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.ac]}){H3[K4.ac]}"/>
      <rule value="{H3[K4.me]}({H3[K4.un]}){H3[K4.ac]}"/>
      <rate value="'.$paramset[14].'"/>
      <dissociationRate type="absolute" value="'.$paramset[15].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Truncater"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.me]}){H3[K4.ac]}"/>
      <rule value="{H3[K4.me]}({H3[K4.un]}){H3[K4.ac]}"/>
      <rate value="'.$paramset[16].'"/>
      <dissociationRate type="absolute" value="'.$paramset[17].'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="me_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.me]}){H3[K4.me]}"/>
      <rule value="{H3[K4.ac]}({H3[K4.un]}){H3[K4.me]}"/>
      <rate value="'.$paramset[18].'"/>
      <dissociationRate type="absolute" value="'.$paramset[19].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Filler"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.un]}){H3[K4.ac]}"/>
      <rule value="{H3[K4.ac]}({H3[K4.ac]}){H3[K4.ac]}"/>
      <rate value="'.$paramset[20].'"/>
      <dissociationRate type="absolute" value="'.$paramset[21].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Filler"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.un]}){H3[K4.me]}"/>
      <rule value="{H3[K4.me]}({H3[K4.me]}){H3[K4.me]}"/>
      <rate value="'.$paramset[22].'"/>
      <dissociationRate type="absolute" value="'.$paramset[23].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Corrector"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.ac]}){H3[K4.me]}"/>
      <rule value="{H3[K4.me]}({H3[K4.un]}){H3[K4.me]}"/>
      <rate value="'.$paramset[24].'"/>
      <dissociationRate type="absolute" value="'.$paramset[25].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Corrector"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.me]}){H3[K4.ac]}"/>
      <rule value="{H3[K4.ac]}({H3[K4.un]}){H3[K4.ac]}"/>
      <rate value="'.$paramset[26].'"/>
      <dissociationRate type="absolute" value="'.$paramset[27].'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
</enzymeSet>';

$handle = fopen($filename,"w");
fputs($handle,$filecontent);
fclose($handle);
}


/*
function searchForId($id, $array) 
{
	foreach($array as $key => $val) 
	{
		if ($val[0] == $id) 
		{
			//echo "FOUND ".$val[0]."!";
			return $key;
		}
	}
	return null;
}

function searchForIdV2($id, $array) 
{
	foreach($array as $key => $val)
	{
		if (in_array($id, $val)) 
		{
                return $val[0];
		}
	}
}
*/

function searchForIdV3($id, $array) 
{
	return array_search($id, array_column($array, 0));
}

//remove special chars from string
function stripSpecialChars($sString)
{
	$sString = str_replace(array("\n", "\t", "\r"), '', $sString);
	$sString = preg_replace('~[[:cntrl:]]~', '', $sString); 
	$sString = trim($sString);

	return $sString;
}
?>
