<?php

////deprecated
//extract a php phar archive
function extractArchive($file,$path)
{
	if(!file_exists($path."/".$file))
	{
		echo "Unzipping ".$path."/".$file."\n";

		$p = new PharData($path."/".$file);
		$p2 = $p->decompress();
 	}
	else
	{
		echo "Error! File ".$path."/".$file." not existing!\n";
		return 0;
	}
	$phar = new PharData($path."/".$file);
	$phar->extractTo($path);
}

//extract tar archive (just couldnt remeber the switches :) )
function extractArchive2($file)
{
	echo "Extracting".$file."\n";
	system("mkdir ".$file);
	system("tar -xzvf ".$file.".tar.gz -C ./".$file);
}

//deprecated
//used to extract simulation from tar archive
function getData($path, $filenames, $num_runs)
{

	$numit = 0;
	$nucstring = "";
	$itdatavec = array(0);
	$simvec = array(0);
	$runvec = array(0);
	$temp1 = array(0);
	$numnuc = 0;
	$numchannels = 0;
	$prop_data = array(0);
	$prop_matrix = array(0);

	for($i=0; $i < $num_runs; $i++)
	{

		if(!file_exists($filenames."_".$i.".tar"))
		{
			echo "Unzipping ".$filenames."_".$i.".tar.gz \n";

			//$p = new Phar($path.'gauss_'.$par1.'_'.$par2.'_'.$k.'.tar', 0, $path.'gauss_'.$par1.'_'.$par2.'_'.$k.'.tar.gz');
			$p = new PharData($path.$filenames.'_'.$i.'.tar.gz');
			$p2 = $p->decompress();


			//$p2->extractTo('/gauss_'.$par1.'_'.$par2.'_'.$k);
	 	}
		$phar = new PharData($filenames.'_'.$i.'.tar');
		$phar->extractTo($path);

		//if($k == 0)
		//{
			$outfilehandle = fopen($path."/simout/outfile.txt", "r");

			$itdata = "";

			$j = 0;

			while(!feof($outfilehandle))
			{
				$line = fgets($outfilehandle);
				if($line[0] == ">")
				{
					if($j != 0)
					{
						array_push($itdatavec, $nucstring);
						array_push($simvec, $itdatavec);
					}
					else
						array_shift($simvec);

					$itdata = $line;
					$itdatavec = explode(" ", $itdata);


					$nucstring = "";
					$j++;
				}
				else
				{
					$nucstring = $nucstring.$line;
				}

			}

			array_push($itdatavec, $nucstring);
			array_push($simvec, $itdatavec);

		//}

		array_push($runvec, $simvec);




		//$numit = ltrim($itdatavec[0], '>');
		$numit = $j;


		//get prop data info

		if (($handle = fopen("simout/prop_matrices/0.csv", "r")) !== FALSE)
		{
			//echo "Opening simout/prop_matrices/0.csv for propensity data\n";
			$k = 0;

    		while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
			{

				array_pop($data);
				if(!$data)
						break;
				$numnuc = sizeof($data);
				$k++;
			}
			$numchannels = $k;
		}


		$temp1 = array_pad ( $temp1 , $numnuc , 0 );

		$prop_matrix = array_pad ( $prop_matrix , $numchannels , $temp1 );

		$prop_data = array_pad ( $prop_data , $numit , $prop_matrix );



		//get prop matrix data



		for($it=0; $it < $numit; $it++)
		{

			if (($handle = fopen("simout/prop_matrices/".$it.".csv", "r")) !== FALSE)
			{
				//echo "Opening simout/prop_matrices/".$it.".csv...\n";
				$k = 0;

	    		while (($data = fgetcsv($handle, 1000, ";")) !== FALSE)
				{
					//$cols = explode(";", $row);

					array_pop($data);



					//if(strstr($data[0], "|") || $data[0] == NULL || $data == NULL)
					if(!$data)
						break;

					//echo "Channel ".$k." of run ".$i." of iteration ".$it."\n";

					//array_push($prop_data[$it][$k], $data);

					$prop_data[$it][$k] = $data;

					//$data2 = explode(";", $data[0]);

					//for($o=0; $o<sizeof($data2)-1;$o++)	//o = nuc
					//{

						//echo "Adding ".$data2[$o]." at nucleosome ".$o."\n";
					//	$prop_data[$it][$k][$o] = $prop_data[$it][$k][$o] + $data2[$o];

					//}

					//$prop_matrix[$k] = $data;

					//$prop_data[$it][$r][$s] = $prop_data[$it][$r][$s] + $prop_matrix[$r][$s];

					$k++;

				}

				fclose($handle);
			}


			unlink('simout/prop_matrices/'.$it.'.csv');
		}

		//clean up
		deleteDir("simout");

	}
	/*
echo "Parameter: \n
\tNum runs: ".$num_runs."\n
\tNum channels: ".$numchannels."\n
\tPath: ".$path."\n
";*/

	return array($runvec, $prop_data);

}


/*
opens standard outfile and reads simulation data in array:
results first dimension:
	iteration overview data as string
	if string is exploded by whitespace, it creates an array with index
		0 -> it
		1 -> time
		2 -> channel
		3 -> position
		4 -> state
	(array = explode(delimiter, string)
second dimension:
	state string for each iteration as array

	two other version further down, because of issues in assigning the corect iteration to the overview data (shifted by one iteration)
*/


function getSimpleData($filename)
{
	$k = 0;
	$data = array();
	$state = "";
	$buffer = "";
	$handle = fopen($filename, "r");

    if(!$handle)
    {
        echo "ERROR! Cannot open file ".$filename;
        exit(0);
    }

	while (!feof($handle))
	{
		$buffer = fgets($handle,4096);

		if($buffer[0] == ">")
		{
			array_push($data,$state);
			$k++;
			$state = "";
		}
		else
		{
            if($k != 0)
			    $state = $state.$buffer;
		}
	}

	return $data;
}

//other version of reading datafile into multidimensional vector
function getSimpleStateData($filename)
{
	$k = 0;
	$data = array();
	$stateinfo = array();
	$state = "";
	$buffer = "";
	$handle = fopen($filename, "r");

    if(!$handle)
    {
        echo "ERROR! Cannot open file ".$filename;
        exit(0);
    }

	while (!feof($handle))
	{
		$buffer = fgets($handle,4096);

		if($buffer[0] == ">")
		{
			array_push($stateinfo,$buffer);

			array_push($data,$state);

            if($k == 0)
            {
                array_pop($stateinfo);
                array_pop($data);
            }

			$k++;
			$state = "";
		}
		else
		{
            if($k != 0)
			    $state = $state.$buffer;
		}
	}

	return array($stateinfo,$data);
}

//other version of reading datafile into multidimensional vector
function getSimpleStateData2($filename)
{
	$k = 0;
	$data = array();
	$stateinfo = array();
	$state = "";
	$buffer = "";
	$handle = fopen($filename, "r");

    if(!$handle)
    {
        echo "ERROR! Cannot open file ".$filename;
        exit(0);
    }

	while (!feof($handle))
	{
		$buffer = fgets($handle,4096);

		if($buffer[0] == ">")
		{
			array_push($stateinfo,$buffer);

			$buffer = fgets($handle,4096);
			$state = "";
			$state = $state.$buffer;
			array_push($data,$state);
		}
	}

	return array($stateinfo,$data);
}
/*
function getSimpleStateData2($filename)
{
	$k = 0;
	$data = array();
	$stateinfo = array();
	$state = "";
	$buffer = "";
	$handle = fopen($filename, "r");

    if(!$handle)
    {
        echo "ERROR! Cannot open file ".$filename;
        exit(0);
    }

	while (!feof($handle))
	{
		$buffer = fgets($handle,4096);

		if($buffer[0] == ">")
		{
			array_push($stateinfo,$buffer);

			array_push($data,$state);

            if($k == 0)
            {
            	//array_pop($stateinfo);
                array_pop($data);

				$buffer = fgets($handle,4096);
				$state = "";
				$state = $state.$buffer;
				array_push($data,$state);
            }

			$k++;
			$state = "";
		}
		else
		{
            if($k != 0)
			    $state = $state.$buffer;
		}
	}

	return array($stateinfo,$data);
}
*/


//loads folder with propensity matrices and stores the propensity information in a multidimensional array

function getPropensityData($dir)
{

	$k = 0;

	//$result = array();

	//$tmp_arr = array_fill(0,$numnuc,0);


	//$numit = 40000;
	//$numchannels = 20;

	//$tmp_arr2 = array_fill(0,$numchannels,array());

	//$result = array_fill(0,$numit,$tmp_arr2);

	$files = array();

	if (is_dir($dir))
	{
		if ($dh = opendir($dir))
		{
			while (($file = readdir($dh)) !== false)
			{
				//echo "filename: $file : filetype: " . filetype($dir.$file) . "\n";
				if ($file != "." && $file != "..")
					array_push($files, $file);



			}
			closedir($dh);

			sort($files, SORT_NUMERIC);

			$numit = sizeof($files);

			$numit = 4000;

			for($it = 0; $it < $numit; $it++)
			{
				if (($handle = fopen($dir."/".$it.".csv", "r")) !== FALSE)
				{

					$k = 0;
					while (($data = fgetcsv($handle, 4000, ";")) !== FALSE)
					{
						//array_push($result[$it][$k],$data);

						for($nuc = 0; $nuc < sizeof($data); $nuc++)
							$result[$it][$k][$nuc] = $data[$nuc];

						$k++;
					}
				}

				fclose($handle);

				if($it % ($numit/10) == 0 && $it != 0)
					echo round(100*$it/$numit)."%\n";

			}
		}
	}



	print_r($result);
	return $result;
}


//same function as above but with a single selectable reaction channel
function getPropensityDataPerChannel($dir, $channel)
{

	$k = 0;

	//$result = array();



	//$tmp_arr = array_fill(0,$numnuc,0);


	//$numit = 40000;
	//$numchannels = 20;

	//$tmp_arr2 = array_fill(0,$numchannels,array());

	//$result = array_fill(0,$numit,$tmp_arr2);

	$files = array();

	if (is_dir($dir))
	{
		if ($dh = opendir($dir))
		{
			while (($file = readdir($dh)) !== false)
			{
				//echo "filename: $file : filetype: " . filetype($dir.$file) . "\n";
				if ($file != "." && $file != "..")
					array_push($files, $file);



			}
			closedir($dh);

			sort($files, SORT_NUMERIC);

			$numit = sizeof($files);

			$result = new SplFixedArray($numit);

			//$numit = 38000;

			for($it = 0; $it < $numit; $it++)
			{



				$filestring = file_get_contents($dir."/".$it.".csv");

				$lines = explode(PHP_EOL,$filestring);

				$nucinfo = explode(";", $lines[$channel]);

				array_pop($nucinfo);

				$result[$it] = new SplFixedArray(sizeof($nucinfo));

				$result[$it] = $nucinfo;


					if($it % ($numit/10) == 0 && $it != 0)
						echo round(100*$it/$numit)."%\n";

			}
		}
	}



//	print_r($result);

	return $result;

}


//deprecated
//old function to extract reactions during a simulations from propensity files
function getReactions($dir)
{

	$k = 0;

	//$result = array();

	$result = array();

	//$tmp_arr = array_fill(0,$numnuc,0);


	//$numit = 40000;
	//$numchannels = 20;

	//$tmp_arr2 = array_fill(0,$numchannels,array());

	//$result = array_fill(0,$numit,$tmp_arr2);

	$files = array();

	if (is_dir($dir))
	{
		if ($dh = opendir($dir))
		{
			while (($file = readdir($dh)) !== false)
			{
				//echo "filename: $file : filetype: " . filetype($dir.$file) . "\n";
				if ($file != "." && $file != "..")
					array_push($files, $file);
			}
			closedir($dh);

			sort($files, SORT_NUMERIC);


			$numit = sizeof($files);

			/*$infostring = file_get_contents($dir."/0.csv");

			$infolines = explode(PHP_EOL,$infostring);

			$nucinfo = explode(";", $infolines[0]);

			$numchannels = sizeof($infolines-4);
			$numnuc = sizeof($nucinfo);

			//$tmp_arr2 = array_fill(0,$numchannels,array());

			//$result = array_fill(0,$numit,$tmp_arr2);
			*/
			for($it = 0; $it < $numit; $it++)
			{
				$filestring = file_get_contents($dir."/".$it.".csv");

				$lines = explode(PHP_EOL,$filestring);
				//$lines = explode("\r\n",$filestring);

				$NEWLINE_RE = '/(\r\n)|\r|\n/'; // take care of all possible newline-encodings in input file
				$filestring = preg_replace($NEWLINE_RE,'-', $filestring);

				$lines = explode("-",$filestring);


				for($line = 0; $line < sizeof($lines); $line++)
				{
					if(strpos($lines[$line], "|") != false)
					{
						$result[$it] = explode("|", $lines[$line]);
					}
				}

				//print_r($lines[0]);
				//exit(0);

				if($it % ($numit/10) == 0 && $it != 0)
					echo round(100*$it/$numit)."%\n";

			}
		}
	}



//	print_r($result);

	return $result;

}

//takes single state string an fills 2d array with an array of nucleosomes ind the first dimension and the array of bound enzymes as second dimension
function getStateData($data)
{
	//$numit = sizeof($data);

	//$vStates = array();
	//$vBoundEnzymes = array();

	$sState = "";
	$sBoundEnz = "";
	$vNucInfo = explode(";",$data);

//print_r($vNucInfo);

	for($nucs = 0; $nucs < sizeof($vNucInfo)-1; $nucs++)
	{
		$vTMP = explode(":",$vNucInfo[$nucs]);
		$sState = $sState.$vTMP[0];
		$sBoundEnz = $sBoundEnz.$vTMP[1]."|";



		//print_r($vTMP);

		//exit(0);
	}

		//exit(0);

	//array_push($vStates,$sState);
	//array_push($vBoundEnzymes,$sBoundEnz);


//	return array($vStates, $vBoundEnzymes);
	return array($sState, $sBoundEnz);
}


//writes data as labelled file
//TODO:  find data to pot
function writeDSV($filename, $data, $x_label = array(), $y_label = array(), $delimiter = "\t")
{
	$datasize = sizeof($data);

//	echo "Datalines: ".$datasize."\n";

	$handle = fopen($filename,"w");
	for($line = -1; $line < $datasize; $line++)
	{
		if(sizeof($y_label) != 0 && $line == -1)
		{
			for($i = 0; $i < sizeof($y_label); $i++)
			{
				fputs($handle, $y_label[$i]);
				if($entry != sizeof($y_label)-1)
					fputs($handle, "\t");
			}
			fputs($handle, "\n");
			continue;
		}
		else
		{
			if($line == -1)
				$line = 0;
		}
		$numdata = sizeof($data[$line]);

		if(sizeof($x_label) != 0)
		{
			fputs($handle, $x_label[$line]."\t");
		}

		for($entry = 0; $entry < $numdata; $entry++)
		{
			fputs($handle, $data[$line][$entry]);
			if($entry != $numdata-1)
				fputs($handle, "\t");
		}
		fputs($handle, "\n");
	}
}


//converts old stochdyn outpu files to histsim output file format (without bound enzymes)
//the stateconvert vector translates the stochdyn state number to a nucleosome configuration (for example {H3[K4.me]})
function StochDynConverter($file,$stateconvert)
{

	$handle = fopen($file,"r");

	$handle2 = fopen($file."_converted","w");

	fputs($handle2, "##\n\n");


	$row = 0;
	$nucdata = -1;
	$timedata = -1;
	$iterdata = -1;
	while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE)
	{

		if($row == 0)
		{

			//get sim information

			for($i = 0; $i < sizeof($data); $i++)
			{

				//print_r($data);

				if($data[$i] == "time")
				{
					$nucdata = $i + 3;
					$timedata = $i;
					$iterdata = $i + 1;
					break;
				}
			}


			if($nucdata == -1)
			{
				die("ERROR!\n");
			}
			$row++;
			continue;
		}


		fputs($handle2, ">".$data[$iterdata]." ".$data[$timedata]." 0 0 0\n");

		for($j = 0; $j < strlen($data[$nucdata]); $j++)
		{
			$state = $data[$nucdata][$j];

			fputs($handle2, $stateconvert[$state].":-1;");
		}
		fputs($handle2,"\n");

		$row++;
	}
	fclose($handle);
	fclose($handle2);
}


//same function as above but for files without header
function StochDynConverter_headless($file,$stateconvert,$numenzymes)
{

	$handle = fopen($file,"r");
	$handle2 = fopen($file."_converted","w");

	fputs($handle2, "##\n\n");


	$row = 0;
	$nucdata = $numenzymes + 4;
	$timedata = $numenzymes + 1;
	$iterdata = $numenzymes + 2;



	while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE)
	{


		print_r($data);

		fputs($handle2, ">".$data[$iterdata]." ".$data[$timedata]." 0 0 0\n");

		for($j = 0; $j < strlen($data[$nucdata]); $j++)
		{
			$state = $data[$nucdata][$j];

			fputs($handle2, $stateconvert[$state].":-1;");
		}
		fputs($handle2,"\n");

		$row++;
	}
	fclose($handle);
	fclose($handle2);

}

//parses rulefile in associative php array
function parseEnzymes($sRuleFile)
{

	$result = array();

	$vRuleArray = array();

	$vRuleArray["target"] = "";
	$vRuleArray["rule"] = "";
	$vRuleArray["assocrate"] = "";
	$vRuleArray["dissocrate"] = "";
	$vRuleArray["name"] = "";

	$iInRule = -1;
	$iInEnzyme = -1;

	$fsHandle = fopen($sRuleFile,"r");

	//error_reporting(0);

	while (($buffer = fgets($fsHandle)) !== FALSE)
	{

		//echo "Buffer: ".$buffer."\n";

		if(strpos($buffer, "<enzymeDef>"))
		{
			//echo "Enzyme start.\n";
			$iInEnzyme = 1;
			$iInRule = 0;
			$vTMP = array();
		}

		if(strpos($buffer, "</enzymeDef>"))
		{
			//echo "Enzyme end.\n";
			//print_r($vTMP);
			$iInEnzyme = 0;
			array_push($result, $vTMP);
		}

		if($iInEnzyme == 1)
		{
			if($iInRule == 0 && strpos($buffer, "<name"))
			{
				$iStart = stripos($buffer, "value=\"") + 7;
				$iEnd = strpos($buffer, "\"",$iStart);
				$vTMP["name"] = substr($buffer, $iStart, $iEnd - $iStart);
				//echo "Name: ".$vTMP["name"]."\n";
			}
			if($iInRule == 0 && strpos($buffer, "<concentration"))
			{
				$iStart = stripos($buffer, "value=\"") + 7;
				$iEnd = strpos($buffer, "\"",$iStart);
				$vTMP["concentration"] = substr($buffer, $iStart, $iEnd - $iStart);
				//echo "Concentration: ".$vTMP["concentration"]."\n";
			}



			if(strpos($buffer, "<bindingRatesRuleSet>"))
			{
				$iInRule = 1;
				$vTMP2 = array();
				$vTMP2 = $vRuleArray;
				//echo "Rule start\n";
			}

			if($iInRule == 1 && strpos($buffer, "<name"))
			{
				$iStart = stripos($buffer, "value=\"") + 7;
				$iEnd = strpos($buffer, "\"",$iStart);
				$vTMP2["name"] = substr($buffer, $iStart, $iEnd - $iStart);
				//echo "\tName: ".$vTMP2["name"]."\n";
			}
			if($iInRule == 1 && strpos($buffer, "<target"))
			{
				$iStart = stripos($buffer, "value=\"") + 7;
				$iEnd = strpos($buffer, "\"",$iStart);
				$vTMP2["target"] = substr($buffer, $iStart, $iEnd - $iStart);
				//echo "\tTarget: ".$vTMP2["target"]."\n";
			}
			if($iInRule == 1 && strpos($buffer, "<rule"))
			{
				$iStart = stripos($buffer, "value=\"") + 7;
				$iEnd = strpos($buffer, "\"",$iStart);
				$vTMP2["rule"] = substr($buffer, $iStart, $iEnd - $iStart);
				//echo "\tRule: ".$vTMP2["rule"]."\n";
			}
			if($iInRule == 1 && strpos($buffer, "<rate"))
			{
				$iStart = stripos($buffer, "value=\"") + 7;
				$iEnd = strpos($buffer, "\"",$iStart);
				$vTMP2["assocrate"] = substr($buffer, $iStart, $iEnd - $iStart);
				//echo "\tRate: ".$vTMP2["assocrate"]."\n";
			}
			if($iInRule == 1 && strpos($buffer, "<dissociationRate"))
			{
				$iStart = stripos($buffer, "value=\"") + 7;
				$iEnd = strpos($buffer, "\"",$iStart);
				$vTMP2["dissocrate"] = substr($buffer, $iStart, $iEnd - $iStart);
				//echo "\tDissociation rate: ".$vTMP2["dissocrate"]."\n";
			}

			if(strpos($buffer, "</bindingRatesRuleSet>"))
			{

				array_push($vTMP, $vTMP2);
				$iInRule = 0;
				//echo "\tRule end\n";
			}
		}

	}

	fclose($fsHandle);

	return $result;
}


//compares two states in standrad format and returns vector of numbers of correct, incorrect and neutrals positions
function getRightWrong($sData, $sTargetState)
{
	$vStateArray1 = explode("}",$sData);
	$vStateArray2 = explode("}",$sTargetState);
	array_pop($vStateArray1);
	array_pop($vStateArray2);

	//print_r($vStateArray1);
	//print_r($vStateArray2);
	$iNumTrue = 0;
	$iNumFalse = 0;
	$iNumNeutral = 0;

	//print_r($vStateArray1);
	//print_r($vStateArray2);

	$iNumNuc = 0;

	if(sizeof($vStateArray1) != sizeof($vStateArray2))
		return 0;
	else
		$iNumNuc = sizeof($vStateArray1);
	//	die(0);

	for($i = 0; $i < $iNumNuc; $i++)
	{

		$sNuc1 = "";
		$sNuc2 = "";

		$vStateArray1[$i] = $vStateArray1[$i]."}";
		$vStateArray2[$i] = $vStateArray2[$i]."}";
		//$vStateArray1[$i] = explode(":",$vStateArray1[$i])[0];
		//$vStateArray2[$i] = explode(":",$vStateArray2[$i])[0];

		if($vStateArray1[$i] == "{}")
			$sNuc1 = "un";
		else
		{
			$TMP = explode(".",$vStateArray1[$i])[1];
			$sNuc1 = explode("]",$TMP)[0];
		}

		//print_r($vStateArray2[$i]);

		if($vStateArray2[$i] == "{}")
			$sNuc2 = "un";
		else
		{
			$TMP = explode(".",$vStateArray2[$i])[1];
			$sNuc2 = explode("]",$TMP)[0];
		}

		//echo "State1: ".$sNuc1."\tState2: ".$sNuc2."\n";

		if($vStateArray1[$i] == $vStateArray2[$i])
			$iNumTrue++;
		if($vStateArray1[$i] != $vStateArray2[$i] && ($sNuc1 != "un" && $sNuc2 != "un"))
			$iNumFalse++;
		if($vStateArray1[$i] != $vStateArray2[$i] && ($sNuc1 == "un" || $sNuc2 == "un"))
			$iNumNeutral++;
	}

	return array($iNumTrue, $iNumFalse, $iNumNeutral);
}


//reads any data file (CSV, tab delimited..) and returns 2d vector $vData[LINE][DATAFIELD]
function readDataFile($sFilename, $sDelimiter = "\t")
{

	$fhFile = fopen($sFilename,"r");

	while (($buffer = fgets($fhFile)) !== false)
	{
		$vTMP = explode($sDelimiter,$buffer);

		$iLastInd = sizeof($vTMP)-1;

		$vTMP[$iLastInd] = str_replace(array("\n", "\t", "\r"), '', $vTMP[$iLastInd]);

		$vTMP[$iLastInd] = preg_replace('~[[:cntrl:]]~', '', $vTMP[$iLastInd]);

		$vTMP[$iLastInd] = trim($vTMP[$iLastInd]);

		$vData[] = $vTMP;
	}

	return $vData;

}

?>
