<?php


//get amount (percentage) of nucleosome modifiaction in string (complete nucleosome needed as $mod)
function getModDensitiy($state, $mod)
{
	$mod = substr($mod,0,sizeof($mod)-1);
	$count = 0;
	$state_arr = explode("]",$state);

	for($i = 0; $i < sizeof($state_arr);$i ++)
	{
		if($state_arr[$i] == $mod) {
			$count++;
		}
	}

	return $count / sizeof($state_arr);

}

//read xml rulefile with simplexml library (needs to be included)
function readRules($rulefile)
{
	if (file_exists($rulefile))
	{
		$xml = simplexml_load_file($rulefile);
		//print_r($xml);
		return $xml;
	}
	else
	{
		exit('Load file error!');
	}
}


//wie haeufig kommt eine modifikation (zb H3[K9.me]) absolut im state vor
function getNumberOfOccurence($state, $mod)
{
	$result = substr_count($state,$mod);

	return $result;
}

//wie haeufig kommt eine modifikation (zb H3[K9.me]) prozentual im state vor
function getModDensity($state, $mod)
{
	$numnuc = sizeof(explode("}",$state))-1;

	$result = substr_count($state,$mod)/$numnuc;

	return $result;
}

//TODO: description
function calc_prop_dist($prop_data)
{
	$numit = sizeof($prop_data);
	$numchannels = sizeof($prop_data[0]);
	$numnuc = sizeof($prop_data[0][0]);

	$tmp_arr = array_fill(0,$numnuc,0);


	$result = array_fill(0,$numchannels,$tmp_arr);

	$stdev = $result;

	for($it = 0; $it < $numit; $it++)
	{
		for($chan = 0; $chan < $numchannels; $chan++)
		{
			for($nuc = 0; $nuc < $numnuc; $nuc++)
			{
				$result[$chan][$nuc] = $result[$chan][$nuc] + $prop_data[$it][$chan][$nuc]/$numit;
			}
		}
	}

	$normfactor = 1/(1-$numit);


	for($chan = 0; $chan < $numchannels; $chan++)
	{
		for($nuc = 0; $nuc < $numnuc; $nuc++)
		{

			for($it = 0; $it < $numit; $it++)
			{
				$stdev[$chan][$nuc] = $stdev[$chan][$nuc] + $normfactor * ($result[$chan][$nuc] - $prop_data[$it][$chan][$nuc]) * ($result[$chan][$nuc] - $prop_data[$it][$chan][$nuc]);

			}
		}
	}


	return array($result, $stdev);
}


//TODO: description
function calc_prop_dist_from_channels($prop_data)
{
	$numit = sizeof($prop_data);

	$numnuc = sizeof($prop_data[0]);

	$result = array_fill(0,$numnuc,0);


	$stdev = $result;

	for($it = 0; $it < $numit; $it++)
	{
		for($nuc = 0; $nuc < $numnuc; $nuc++)
		{
			$result[$nuc] = $result[$nuc] + $prop_data[$it][$nuc]/$numit;
		}
	}

	$normfactor = 1/(1-$numit);


	for($nuc = 0; $nuc < $numnuc; $nuc++)
	{

		for($it = 0; $it < $numit; $it++)
		{
			$stdev[$nuc] = $stdev[$nuc] + $normfactor * ($result[$nuc] - $prop_data[$it][$nuc]) * ($result[$nuc] - $prop_data[$it][$nuc]);

		}
	}


	return array($result, $stdev);
}


//TODO: description
function calcReactionDist($reactions)
{
	$numit = sizeof($reactions);

	//$result = array();
	$numchannels = 0;
	$numnucs = 0;

	for($it = 0; $it < $numit; $it++)
	{
		if($numnucs < $reactions[$it][0]) {
			$numnucs = $reactions[$it][0];
		}
		if($numchannels < $reactions[$it][1]) {
			$numchannels = $reactions[$it][1];
		}
	}

	$numnucs++;
	$numchannels++;


	$tmp_arr2 = array_fill(0,$numnucs,0);

	$result = array_fill(0,$numchannels,$tmp_arr2);

	//print_r($result);

	//exit(0);

	for($it = 0; $it < $numit; $it++)
	{
		$position = $reactions[$it][0];
		$channel = $reactions[$it][1];

		//echo "Pos: ".$position." Chan: ".$channel."\n";

		$result[$channel][$position]++;
	}

	return $result;
}

//takes output data and calculates turnover rates and time a nucleosome was bound by a certain enzyme and in a certain state
function calcOccupationStatistics($data, $states, $numchannels)
{
	//create state matrix
	//$states = array of modifications to monitor
	//$data = simpledata structure

	$finalstatedata = $data[1][(sizeof($data[1])-1)];

	$finalstatevec = explode(";",$finalstatedata);



	$numnuc = sizeof($finalstatevec)-1;

	//print_r($finalstatevec);

	$turnoverrates = array_fill(0, $numnuc, 0);

	$numstates = sizeof($states);

	$tmp_arr = array();

	for($l = 0; $l < $numstates; $l++)
	{
		$tmp_arr[$states[$l]] = 0;
	}

	$tmp_arr2 = array_fill(0, $numchannels, 0);

	$statetimes = array_fill(0, $numnuc, $tmp_arr);

	$enzymebindings = array_fill(0, $numnuc, $tmp_arr2);


	$finalsiminfo = $data[0][(sizeof($data[1])-1)];

	$runtime = explode(" ",$finalsiminfo)[1];

	echo "Runtime: ".$runtime."\n";

	$numit = sizeof($data[0]);

	$laststate = "";

	$lastboundenzyme = -1;

	$lasttime = 0;
	$curtime = 0;

	//$initialstate = "{H3[K4.me]}{}{H3[K4.me]}{}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}";

	$initialstatedata = $data[1][1];

	$initialstatevec = explode(";",$finalstatedata);

	$initstatevec = array();

	for($o = 0; $o < $numnuc; $o++)
	{
		$tmp = explode(":",$initialstatevec[$o])[0];

		array_push($initstatevec, $tmp);
	}

	//$initstatevec = explode("}", $initialstate);

	//array_pop($initstatevec);

	//print_r($initstatevec);

	//die(0);

	//run trough data
	for($j = 0; $j < $numnuc; $j++)
	{
		//echo "Nucleosome ".$j."\n";
		$lasttime = 0;
		$curtime = 0;
		$lastboundenzyme = -1;


		for($i = 1; $i < $numit; $i ++)
		{
			if($i == 1)
			{
				$laststate = $initstatevec[$j];//."}";
			}

			$simdatastring = $data[0][$i];
			$statedatastring = $data[1][$i];

			$statedata = explode(";",$statedatastring);
			$simdata = explode(" ",$simdatastring);


			$curtime = $simdata[1];
			//$curenz = $simdata[2]; ???
			$curpos = $simdata[3];

			$curstatedata = explode(":",$statedata[$j]);

//			print_r($statedata[$j]);


			$curstate = explode(":",$statedata[$j])[0];

			$curenzbound = explode(":",$statedata[$j])[1];

			//if($laststate == "{}")
			//	$laststate ==
			//if($curstate == "{}")
			//	$laststate ==

			//echo "It: ".$i." Curstate: ".$curstate." Laststate ".$laststate." Curtime ".$curtime." Lasttime ".$lasttime."\n";


			if($laststate != $curstate)
			{

				//echo "State changed, last bound enz ".$lastboundenzyme." cur bound enz".$curenzbound."\n";

				//increment turnover
				$turnoverrates[$j]++;

				//increment bound enzyme
				$enzymebindings[$j][$lastboundenzyme]++;

				$duration = $curtime - $lasttime;

				$statetimes[$j][$laststate] = $statetimes[$j][$laststate] + $duration;

				$lasttime = $curtime;
			}

			//conclude everything at last iteration

			if($i == $numit-1)
			{
				//echo "last state, last bound enz ".$lastboundenzyme." cur bound enz".$curenzbound."\n";

				$duration = $curtime - $lasttime;

				$statetimes[$j][$curstate] = $statetimes[$j][$curstate] + $duration;

			}


			//$oldstatedata = explode(";",$oldstatedatastring);
			//$oldsimdata = explode(";",$oldstatedatastring);
			//exception for first iteration:

			$laststate = $curstate;
			$lastboundenzyme = $curenzbound;


			$oldstatedatastring = $statedatastring;
		}
	}

	return array($enzymebindings, $statetimes, $turnoverrates);
}


//returns the order of magnitude of a number
function getMagnitude($value, $maxmag = 10000)
{
	$digits = floor(log($maxmag, 10) + 1);

	for($i = 1; $i < $digits+1; $i++)
	{
		$mod =$i*10;
		if($value % $mod == $value)
			return ($i-1)*10;
	}
}

//calcs mean value of array
function calcMeanValue($values)
{
	$numvals = sizeof($values);

	if($numvals == 0)
	{
		echo "Error! No values to calculate\n";
		return -1;
	}

	$sum = 0;

	for($i = 0; $i < $numvals; $i++)
	{
		$sum = $sum + $values[$i];
	}
	return $sum / $numvals;
}

//calcs median of array
function calcMedian($arr)
{
    $count = count($arr);
    $middleval = floor(($count-1)/2);
    if($count % 2)
	{
        $median = $arr[$middleval];
    }
	else
	{
        $low = $arr[$middleval];
        $high = $arr[$middleval+1];
        $median = (($low+$high)/2);
    }
    return $median;
}

//returns quartiles of values in sorted array
function calcQuartiles($arr)
{
    $count = count($arr);
    $middleval = floor(($count-1)/2);
	$low = 0;
	$high = 0;
    if($count % 2)
	{
        $median = $arr[$middleval];
    }
	else
	{
        $low = $arr[$middleval];
        $high = $arr[$middleval+1];
        $median = (($low+$high)/2);
    }

	$fFirstQuartile = 0;
	$fThirdQuartile = 0;

	$fMin = min($arr);
	$fMax = max($arr);

	//split data in halves
	$vLower = array_slice($arr,0,$middleval);
	$vUpper = array_slice($arr,$middleval+1);



	if(!($count % 2))
	{
		$fFirstQuartile = calcMedian($vLower);
		$fThirdQuartile = calcMedian($vUpper);
	}
	else
	{
/*		//boxplot method
		$fFirstQuartile = calcMedian($vLower);
		$fThirdQuartile = calcMedian($vUpper);
*/
		//Tukey hinge method
		$vLower[] = $median;
		array_unshift($vUpper,$median);

		$fFirstQuartile = calcMedian($vLower);
		$fThirdQuartile = calcMedian($vUpper);
	}


	return array($fFirstQuartile, $median, $fThirdQuartile);
}


//calculates expectation value from arrays of values and their probabilities
function calcExpectationValue($values, $prob)
{
	$numvals = sizeof($values);

	$result = 0;
	for($i = 0; $i < $numvals; $i++)
	{
		$result = $result + $values[$i]*$prob[$i];
	}
	return $result;
}

//calculates standard deviation of array values
function calcSampleSTD($data)
{
	$n = sizeof($data);

	$mean = calcMeanValue($data);

	$sum = 0;

	for($i = 0; $i < $n; $i++)
	{
		$sum = $sum + ($data[$i]-$mean)*($data[$i]-$mean);
	}

	return sqrt($sum/($n-1));
}

//calculates variance of array values
function calcEqualDistVariance($data)
{
	$n = sizeof($data);

	$mean = calcMeanValue($data);

	$sum = 0;

	for($i = 0; $i < $n; $i++)
	{
		$sum = $sum + ($data[$i]-$mean)*($data[$i]-$mean);
	}

	return $sum/($n-1);
}


//calculates covariance between a set of values
function calcSamplceCovariance($value_matrix)
{

//first dimension: number of parameters
//second dimension: number of samples

	$numparams = sizeof($value_matrix);
	$numvals = sizeof($value_matrix[0]);

	$result = array_fill(0,$numvals,array_fill(0,$numvals,0));

	echo "Num Params: ".$numparams." Num Samples: ".$numvals."\n";

	for($i = 0; $i < $numparams; $i++)
	{
		for($j = 0; $j < $numparams; $j++)
		{
			$sum = 0;
			$mean_i = calcMeanValue($value_matrix[$i]);
			$mean_j = calcMeanValue($value_matrix[$j]);

			echo "mean_i: ".$mean_i." mean_j: ".$mean_j."\n";


			for($k = 0; $k < $numvals; $k++)
			{
				 $sum = ($value_matrix[$i][$k]-$mean_i)*($value_matrix[$j][$k]-$mean_j);
			}

			$result[$i][$j]	= $sum / ($numvals-1);
		}
	}

	//print_r($result);

	return $result;
}

//attempt to dedect asymptotes in data
function findHorizAsympt($data, $interval, $grainsize, $intervalldata = array())
{


	$samplesize = sizeof($intervalldata);
	if($samplesize == 0)
	{
		$totalinterval = $samplesize;
		$num_range = range(1, $samplesize);
		$intervalldata = array_combine($num_range, $num_range);
	}
	else
		$totalinterval = max($intervalldata);


	$minimum = min($data);
	$maximum = max($data);

	$intervalcounter = 1;
	$k = 0;
	$last_k = 0;

	$meandata = array();
	$stddata = array();


	//print_r($data);
	//print_r($intervalldata);

	//die();


	for($it = 1; $it < $samplesize-1; $it++)
	{


		if($intervalldata[$it] >= $intervalcounter*$grainsize)
		{

			$interdata = array_slice($data,$it,($k-$last_k));
			$std = calcSampleSTD($interdata);

			$tempdata = array();
			$intervalldone = $intervalldata[($k)] - $intervalldata[($last_k)];

			$mean = calcMeanValue($interdata);

			//echo "k: ".$k." last k: ".$last_k."\n";

			array_push($meandata, $mean);
			array_push($stddata, $std);

			$intervalcounter++;
			$last_k = $k;

			unset($tempdata);
		}
		$k++;

	}

	//$threshold = 2/3*($maximum - $minimum);
	//$threshold = 2/3*($maximum - $minimum)/max($stddata);
	//*getMagnitude($maximum - $minimum);
	//$threshold = max($stddata)/($maximum - $minimum);
	$threshold = $meandata[sizeof($meandata)-1]-3*max($stddata);

	for($i = 0; $i < sizeof($meandata); $i++)
	{
		if($meandata[sizeof($meandata)-$i-1] < $threshold)
			return $i;
	}

	$threshold = $meandata[sizeof($meandata)-1]+3*max($stddata);

	for($i = 0; $i < sizeof($meandata); $i++)
	{
		if($meandata[sizeof($meandata)-$i-1] > $threshold)
			return $i;
	}

	return -1;

	//print_r($meandata);
	//print_r($stddata);
	//echo "Min: ".$minimum."\tMax: ".$maximum."\n";
	//echo "Threshold: ".$threshold."\tMax STD: ".max($stddata)."\n";

}

function findHorizAsymptY($data, $interval, $grainsize, $intervalldata = array())
{


	$samplesize = sizeof($intervalldata);
	if($samplesize == 0)
	{
		$totalinterval = $samplesize;
		$num_range = range(1, $samplesize);
		$intervalldata = array_combine($num_range, $num_range);
	}
	else
		$totalinterval = max($intervalldata);


	$minimum = min($data);
	$maximum = max($data);

	$intervalcounter = 1;
	$k = 0;
	$last_k = 0;

	$meandata = array();
	$stddata = array();


	//print_r($data);
	//print_r($intervalldata);

	//die();


	for($it = 1; $it < $samplesize-1; $it++)
	{


		if($intervalldata[$it] >= $intervalcounter*$grainsize)
		{

			$interdata = array_slice($data,$it,($k-$last_k));
			$std = calcSampleSTD($interdata);

			$tempdata = array();
			$intervalldone = $intervalldata[($k)] - $intervalldata[($last_k)];

			$mean = calcMeanValue($interdata);

			//echo "k: ".$k." last k: ".$last_k."\n";

			array_push($meandata, $mean);
			array_push($stddata, $std);

			$intervalcounter++;
			$last_k = $k;

			unset($tempdata);
		}
		$k++;

	}

	//$threshold = 2/3*($maximum - $minimum);
	//$threshold = 2/3*($maximum - $minimum)/max($stddata);
	//*getMagnitude($maximum - $minimum);
	//$threshold = max($stddata)/($maximum - $minimum);
	$threshold = $meandata[sizeof($meandata)-1]-3*max($stddata);

	$borderinterval = -1;

	for($i = 0; $i < sizeof($meandata); $i++)
	{
		if($meandata[sizeof($meandata)-$i-1] < $threshold)
			$borderinterval = $i;
	}

	$threshold = $meandata[sizeof($meandata)-1]+3*max($stddata);

	for($i = 0; $i < sizeof($meandata); $i++)
	{
		if($meandata[sizeof($meandata)-$i-1] > $threshold)
			$borderinterval = $i;
	}

	return calcMeanValue(array_slice($meandata,$borderinterval,(sizeof($meandata)-$borderinterval-1)));

	//print_r($meandata);
	//print_r($stddata);
	//echo "Min: ".$minimum."\tMax: ".$maximum."\n";
	//echo "Threshold: ".$threshold."\tMax STD: ".max($stddata)."\n";

}
/*
function GPLTFit($datafile,$function,$params)
{

	$gplt_params["terminal"] = "dumb";
	$gplt_params["size_x"] = "1920";
	$gplt_params["size_y"] = "1080";
	//$gplt_params["output"] = "fittest.png";
	$gplt_params["x_min"] = "0";
	$gplt_params["x_max"] = "1";
	$gplt_params["y_min"] = "0";
	$gplt_params["y_max"] = "110";
	$gplt_params["x_label"] = "";
	$gplt_params["y_label"] = "";


	$custom[0] = "set datafile missing '?'";
	$custom[1] = $function;
	$custom[2] = "FIT_LIMIT = 1e-6";
foreach(key value bla bla)
	$custom[3] = "G = 100";
	$custom[4] = "k = 1";
	$custom[5] = "c = 1";

	$custom[6] = "fit f(x) '".$datafile."' using 1:2 via ";
foreach(key value..)
	"G, k, c";



	$gplt_data = array();


	$gplt = createGNUPlotFile($gplt_data,$gplt_params,$custom);

	$handle = fopen("fit.gplt","w");
	fputs($handle, $gplt);
	fclose($handle);

	system('/usr/bin/gnuplot fit.gplt');

	//parse parameters

	$handle = fopen("fit.log","r");

	while(($buffer = fgets($handle, 4096)) !== false)
	{
        echo $buffer;
    }

	fclose($handle);

}
*/

//TODO: insert description
function calcSMA($data, $order)
{
	$SMA = array();
	$numvalues = sizeof($data);
	for($k = $order - 1; $k < $numvalues; $k++)
	{
		$sum = 0;
		for($i = 0; $i < $order; $i++)
		{
			$sum = $sum + $data[($k - $i)];
		}
		array_push($SMA, $sum/$order);
	}
	return $SMA;
}

//calculate differential quotient values from data points (x-values: DataX, y-vlaues: dataY) in a certain x-range (minh, maxh)
function calcDiffQ($dataX, $dataY, $maxh, $minh, $threshold = 0)
{
	$numvalues = sizeof($dataX);
	$start = 0;
	$end = 0;
	$check = 0;

	$diffq = array();

	for($t = 0; $t < sizeof($dataX); $t++)
	{
		if($dataX[$t] >= $maxh && $check == 0)
		{
			$start = $t;
			$check = 1;
			//echo "X: ".$dataX[$t]." maxh: ".$maxh."\n";
		}

		if($dataX[$t] >= $dataX[(sizeof($dataX)-1)] - $maxh)
		{
			$end = $t;
			break;
		}
	}

	//echo "Start: ".$dataX[$start]." End: ".$dataX[$end]."\n";
	//echo "Start: ".$start." End: ".$end."\n";

	for($i = 0; $i < $start; $i++)
	{
		array_push($diffq,0);
	}

	for($i = $start; $i < $end; $i++)
	{
		$Y = $dataY[$i];
		$X = $dataX[$i];

		$curstart = 0;
		$curend = 0;

		for($t = $i; $dataX[$i] - $dataX[$t] > $maxh; $t--)
		{
			$curstart = $t;
		}

		for($t = $i; $dataX[$t] - $dataX[$i] > $maxh; $t++)
		{
			$curend = $t;
		}

		$diff = ($dataY[$curend] - $dataY[$curstart])/(2*$maxh);

		array_push($diffq,$diff);
	}

	for($i = $end; $i < $numvalues; $i++)
	{
		array_push($diffq,0);
	}

	return $diffq;
}


//calculates relative growth between consecutive points of data
function calcGrowth($data)
{
	$growth = array();
	$numvalues = sizeof($data);
	for($k = 1; $k < $numvalues; $k++)
	{
		if($data[($k-1)] == 0)
			array_push($growth, 0);
		else
			array_push($growth, ($data[$k] - $data[($k-1)])/$data[($k-1)]);
	}
	return $growth;
}


//calculate a custom distance between two states in ouput format ({NUCLEOSOME}:ENZYME;{}....)
//distance matrix can be supplied
//standard distance is steps needed between transistions me <-> un <-> ac
function getDistanceLevRaw($state1, $state2, $vDistmat = array())
{
	//$vDistmat = array();

	//distance matrix used
	if(sizeof($vDistmat == 0))
	{
		$vDistmat["un"]["un"] = 0;
		$vDistmat["un"]["me"] = 1;
		$vDistmat["un"]["ac"] = 1;
		$vDistmat["me"]["un"] = 1;
		$vDistmat["me"]["me"] = 0;
		$vDistmat["me"]["ac"] = 2;
		$vDistmat["ac"]["un"] = 1;
		$vDistmat["ac"]["me"] = 2;
		$vDistmat["ac"]["ac"] = 0;
	}
	//raw state data

	$vStateArray1 = explode(";",$state1);
	$vStateArray2 = explode(";",$state2);

	array_pop($vStateArray1);
	array_pop($vStateArray2);

	$iDistance = 0;

	//print_r($vDistmat);


	//print_r($vStateArray1);
	//print_r($vStateArray2);



	if(sizeof($vStateArray1) != sizeof($vStateArray2))
		return -1;

	//	die(0);

	for($i = 0; $i < sizeof($vStateArray1); $i++)
	{

		//$vStateArray1[$i] = $vStateArray1[$i]."}";
		//$vStateArray2[$i] = $vStateArray2[$i]."}";

		//preg_match("/\{a*\.(?P<hist>a*)\}/" , $vStateArray1[$i], $vNuc1T);
		//preg_match("/\{a*\.(?P<hist>a*)\}/" , $vStateArray2[$i], $vNuc2T);

		//preg_match("/\.(?P<hist>a*)\]/" , $vStateArray1[$i], $vNuc1T);
		//preg_match("/\.(?P<hist>a*)\]/" , $vStateArray2[$i], $vNuc2T);

		$vStateArray1[$i] = explode(":",$vStateArray1[$i])[0];
		$vStateArray2[$i] = explode(":",$vStateArray2[$i])[0];

		if($vStateArray1[$i] == "{}")
			$sNuc1 = "un";
		else
		{
			$TMP = explode(".",$vStateArray1[$i])[1];
			$sNuc1 = explode("]",$TMP)[0];
		}

		//print_r($vStateArray2[$i]);

		if($vStateArray2[$i] == "{}")
			$sNuc2 = "un";
		else
		{
			$TMP = explode(".",$vStateArray2[$i])[1];
			$sNuc2 = explode("]",$TMP)[0];
		}

		//echo "State1: ".$sNuc1."\tState2: ".$sNuc2."\n";


		$iDistance = $iDistance + $vDistmat[$sNuc1][$sNuc2];
	}

	return $iDistance;
}


//calculate a custom distance between two states in statefile format ({NUCLEOSOME1}{NUCLEOSOME1}....)
function getDistanceLev($state1, $state2)//, $vDistmat = array())
{
	//$vDistmat = array();

	//if(sizeof($vDistmat == 0))
	//{
		$vDistmat["un"]["un"] = 0;
		$vDistmat["un"]["me"] = 1;
		$vDistmat["un"]["ac"] = 1;
		$vDistmat["me"]["un"] = 1;
		$vDistmat["me"]["me"] = 0;
		$vDistmat["me"]["ac"] = 2;
		$vDistmat["ac"]["un"] = 1;
		$vDistmat["ac"]["me"] = 2;
		$vDistmat["ac"]["ac"] = 0;
	//}
	//raw state data

	$vStateArray1 = explode("}",$state1);
	$vStateArray2 = explode("}",$state2);

	array_pop($vStateArray1);
	array_pop($vStateArray2);

	$iDistance = 0;

	//print_r($vDistmat);


	//print_r($vStateArray1);
	//print_r($vStateArray2);



	if(sizeof($vStateArray1) != sizeof($vStateArray2))
		return -1;

	//	die(0);

	for($i = 0; $i < sizeof($vStateArray1); $i++)
	{

		$vStateArray1[$i] = $vStateArray1[$i]."}";
		$vStateArray2[$i] = $vStateArray2[$i]."}";


		if($vStateArray1[$i] == "{}")
			$sNuc1 = "un";
		else
		{
			$TMP = explode(".",$vStateArray1[$i])[1];
			$sNuc1 = explode("]",$TMP)[0];
		}

		//print_r($vStateArray2[$i]);

		if($vStateArray2[$i] == "{}")
			$sNuc2 = "un";
		else
		{
			$TMP = explode(".",$vStateArray2[$i])[1];
			$sNuc2 = explode("]",$TMP)[0];
		}

		//echo "State1: ".$sNuc1."\tState2: ".$sNuc2."\n";


		$iDistance = $iDistance + $vDistmat[$sNuc1][$sNuc2];
	}

	return $iDistance;
}


//TODO
function getDistanceTri($state1, $state2)
{
	//$vDistmat = array();


	//raw state data

	$vStateArray1 = explode("}",$state1);
	$vStateArray2 = explode("}",$state2);

	array_pop($vStateArray1);
	array_pop($vStateArray2);

	$iDistance = 0;

	//print_r($vDistmat);


	//print_r($vStateArray1);
	//print_r($vStateArray2);



	if(sizeof($vStateArray1) != sizeof($vStateArray2))
		return -1;

	//	die(0);

	$vDistArr = array(0,0,0);

	for($i = 0; $i < sizeof($vStateArray1); $i++)
	{

		$vStateArray1[$i] = $vStateArray1[$i]."}";
		$vStateArray2[$i] = $vStateArray2[$i]."}";


		if($vStateArray1[$i] == "{}")
			$sNuc1 = "un";
		else
		{
			$TMP = explode(".",$vStateArray1[$i])[1];
			$sNuc1 = explode("]",$TMP)[0];
		}

		//print_r($vStateArray2[$i]);

		if($vStateArray2[$i] == "{}")
			$sNuc2 = "un";
		else
		{
			$TMP = explode(".",$vStateArray2[$i])[1];
			$sNuc2 = explode("]",$TMP)[0];
		}

		if($sNuc1 == "un" && $sNuc2 != "un")
			$vDistArr[1]++;
		elseif($sNuc1 != "un" && $sNuc2 == "un")
			$vDistArr[1]++;
		elseif($sNuc1 == $sNuc2)
			$vDistArr[0]++;
		elseif($sNuc1 != $sNuc2)
			$vDistArr[2]++;
	}



	return $vDistArr;
}

//TODO: description
function getDistancePos($state1, $state2, $vDistmatTrue = array(), $vDistmatFalse = array())
{

	if(sizeof($vDistmatTrue == 0))
	{
		$vDistmatTrue["un"]["un"] = 1;
		$vDistmatTrue["un"]["me"] = 0;
		$vDistmatTrue["un"]["ac"] = 0;
		$vDistmatTrue["me"]["un"] = 0;
		$vDistmatTrue["me"]["me"] = 1;
		$vDistmatTrue["me"]["ac"] = 0;
		$vDistmatTrue["ac"]["un"] = 0;
		$vDistmatTrue["ac"]["me"] = 0;
		$vDistmatTrue["ac"]["ac"] = 1;
	}

	if(sizeof($vDistmatFalse == 0))
	{
		$vDistmatFalse["un"]["un"] = 0;
		$vDistmatFalse["un"]["me"] = 1;
		$vDistmatFalse["un"]["ac"] = 1;
		$vDistmatFalse["me"]["un"] = 1;
		$vDistmatFalse["me"]["me"] = 0;
		$vDistmatFalse["me"]["ac"] = 1;
		$vDistmatFalse["ac"]["un"] = 1;
		$vDistmatFalse["ac"]["me"] = 1;
		$vDistmatFalse["ac"]["ac"] = 0;
	}
	//raw state data

	//$vStateArray1 = explode(";",$state1);
	//$vStateArray2 = explode(";",$state2);

	$vStateArray1 = explode("}",$state1);
	$vStateArray2 = explode("}",$state2);
	array_pop($vStateArray1);
	array_pop($vStateArray2);

	print_r($vStateArray1);
	print_r($vStateArray2);
	$iDistanceTrue = 0;
	$iDistanceFalse = 0;


	//print_r($vStateArray1);
	//print_r($vStateArray2);

	$iNumNuc = 0;

	if(sizeof($vStateArray1) != sizeof($vStateArray2))
		return -1;
	else
		$iNumNuc = sizeof($vStateArray1);
	//	die(0);

	for($i = 0; $i < sizeof($vStateArray1); $i++)
	{

		$vStateArray1[$i] = $vStateArray1[$i]."}";
		$vStateArray2[$i] = $vStateArray2[$i]."}";
		//$vStateArray1[$i] = explode(":",$vStateArray1[$i])[0];
		//$vStateArray2[$i] = explode(":",$vStateArray2[$i])[0];

		if($vStateArray1[$i] == "{}")
			$sNuc1 = "un";
		else
		{
			$TMP = explode(".",$vStateArray1[$i])[1];
			$sNuc1 = explode("]",$TMP)[0];
		}

		//print_r($vStateArray2[$i]);

		if($vStateArray2[$i] == "{}")
			$sNuc2 = "un";
		else
		{
			$TMP = explode(".",$vStateArray2[$i])[1];
			$sNuc2 = explode("]",$TMP)[0];
		}

		//echo "State1: ".$sNuc1."\tState2: ".$sNuc2."\n";


		$iDistanceTrue = $iDistanceTrue + $vDistmatTrue[$sNuc1][$sNuc2];
		$iDistanceFalse = $iDistanceFalse + $vDistmatFalse[$sNuc1][$sNuc2];
	}

	$iDistanceTrue = $iDistanceTrue / $iNumNuc;
	$iDistanceFalse = $iDistanceFalse / $iNumNuc;



	return $iDistanceTrue - $iDistanceFalse;
}


//calculates euclidiean distance between two vectors (sqrt(a^2 + b^2 + ....)
function getDistanceEuclidiean($vVec1, $vVec2)
{
	if(sizeof($vVec1) != sizeof($vVec2))
	{
		echo "Error: Vectors of different dimension!";
		die(0);
	}

	$fSum = 0;

	for($i = 0; $i < sizeof($vVec1); $i++)
	{
		$fSum = $fSum + (($vVec1[$i] - $vVec2[$i]) * ($vVec1[$i] - $vVec2[$i]));
	}

	$fDistance = sqrt($fSum);

	return $fDistance;
}


//calculate distance accrding to how many steps in the tri state system (me <-> un <-> ac) are required to get from one state to another
function getDistanceStep($vVec1, $vVec2)	//manhattan metric
{

	if(sizeof($vVec1) != sizeof($vVec2))
	{
		echo "Error: Vectors of different dimension!";
		die(0);
	}

	$fDistance = 0;


	for($i = 0; $i < sizeof($vVec1); $i++)
	{
		$fDistance = $fDistance + abs($vVec1[$i] - $vVec2[$i]);
	}

	//$fDistance = $vVec1[1]*2 + $vVec1[2];
	$fDistance = abs(($vVec1[1] - $vVec2[1]) *2 + ($vVec1[2] - $vVec2[2]));
	return $fDistance;
}


//calculates 2d positions on the triangular plane as representation of the tri state system
//arguments are passed dynamically but are:
//$iT, $iF, $iN, $iNumNuc, $fScaleFactor
//(was for testing)
function transformToTriangle()
{

	if(func_num_args() == 4)
		$vVector = array(array(func_get_arg(0) * func_get_arg(3)), array(func_get_arg(1) * func_get_arg(3)), array(func_get_arg(2) * func_get_arg(3)));
	if(func_num_args() == 5)
		$vVector = array(array(func_get_arg(0) / func_get_arg(3) * func_get_arg(4)), array(func_get_arg(1) / func_get_arg(3) * func_get_arg(4)), array(func_get_arg(2) / func_get_arg(3) * func_get_arg(4)));

	$vVector = RotateZ($vVector,deg2rad(-45));
	$vVector = RotateY($vVector,deg2rad(-54.73));
	$vVector = RotateZ($vVector,deg2rad(-90));

	return array($vVector[0][0], $vVector[1][0]);
}



//takes outfiles of multiple runs and calculates mean occurrenceof all modifications of all nucleosomes in a certain time (iBinTime) for the whole simulation time
//all simulation folders have to be in $sdatafolder and have to be named simout_X with X being a run number beginning with 0 (and leading zeroes)
function get_mean_state($sDataFolder, $iNumRuns, $fBinTime, $vStates)
{

	//collect data

	$vRunTimes = array();

	$vTimes = array();
	$vTimesPerRun = array();

	for($iRun = 0; $iRun < $iNumRuns; $iRun++)
	{

		$iNumNuc = 0;
		$vTimes = array();
		//non hard coded version?

		if($iNumRuns < 10)
			$file = $sDataFolder."/simout_".$iRun."/outfile.txt";
		if($iNumRuns < 100)
		{
			if($iRun < 10)
				$file = $sDataFolder."/simout_0".$iRun."/outfile.txt";
			if($iRun >= 10 && $iRun < 100)
				$file = $sDataFolder."/simout_".$iRun."/outfile.txt";
		}
		if($iNumRuns >= 100)
		{
			if($iRun < 10)
				$file = $sDataFolder."/simout_00".$iRun."/outfile.txt";
			if($iRun >= 10 && $iRun < 100)
				$file = $sDataFolder."/simout_0".$iRun."/outfile.txt";
			if($iRun >= 100)
				$file = $sDataFolder."/simout_".$iRun."/outfile.txt";
		}

        echo "File: ".$file."\n";

		$cdata = getSimpleStateData($file);


        //print_r($cdata);



		$data = $cdata[0][(sizeof($cdata[0])-1)];
		$fTime = explode(" ", $data)[1];


		for($i = 0; $i < sizeof($cdata[0]); $i++)
		{
			$fTime = explode(" ", $cdata[0][$i])[1];
			array_push($vTimes, $fTime);
		}

		$data = $cdata[1];
		$iNumNuc = sizeof(explode(";", $data[1])) - 1;

		array_push($vTimesPerRun, $fTime);
		array_push($vRunTimes, $vTimes);

        unset($cdata);
        unset($data);
        unset($fTime);
        unset($vTimes);

        //print_r($cdata[1]);
        //die(0);
	}



	$fRunTime = min($vTimesPerRun);



	$iNumBins = $fRunTime / $fBinTime;

    //print_r($iNumBins);

    //print_r($vRunTimes);

	$vTimekeeper = array();

	for($fInt = 0; $fInt <= $fRunTime; $fInt = $fInt + $fBinTime)
    {

        $TMP = array();

        for($iCRun = 0; $iCRun < $iNumRuns; $iCRun++)
        {

            for($iCTime = 0; $iCTime < sizeof($vRunTimes[$iCRun]); $iCTime++)
            {
                //echo "Run: ".$iCRun." Current intervalltime ".$fInt." Time: ".$vRunTimes[$iCRun][$iCTime]."\n";

                if($vRunTimes[$iCRun][$iCTime] >= $fInt)
                {
                    //echo "BREAK!\n";
                    //echo "Run: ".$iCRun." Current intervalltime ".$fInt." Time: ".$vRunTimes[$iCRun][$iCTime]."\n";

                    array_push($TMP, $iCTime);
                    break;
                }

            }
        }

        array_push($vTimekeeper,$TMP);

    }

    //print_r($vTimekeeper);

    //die(0);

	$vNucStateArray = array();


	$vStateArray = array();

	//prepare state array


	for($i = 0; $i < sizeof($vStates); $i++)
	{
		$vNucStateArray[$vStates[$i]] = 0;
	}

	for($i = 0; $i < $iNumNuc; $i++)
	{
		array_push($vStateArray, $vNucStateArray);
	}

	$vRunStateArrays = array();





    //print_r($vStateArray);

	for($iRun = 0; $iRun < $iNumRuns; $iRun++)
    {

		$vBinStateArrays = array();
		$vLastStateArray = array();

		for($iBin = 0; $iBin < sizeof($vTimekeeper); $iBin++)
		{
			$fTime = $iBin * $fBinTime;

	        $vData = array();

			$vTMP_StateArray = $vStateArray;

			if($iNumRuns < 10)
				$file = $sDataFolder."/simout_".$iRun."/outfile.txt";
			if($iNumRuns < 100)
			{
				if($iRun < 10)
					$file = $sDataFolder."/simout_0".$iRun."/outfile.txt";
				if($iRun >= 10 && $iRun < 100)
					$file = $sDataFolder."/simout_".$iRun."/outfile.txt";
			}
			if($iNumRuns >= 100)
			{
				if($iRun < 10)
					$file = $sDataFolder."/simout_00".$iRun."/outfile.txt";
				if($iRun >= 10 && $iRun < 100)
					$file = $sDataFolder."/simout_0".$iRun."/outfile.txt";
				if($iRun >= 100)
					$file = $sDataFolder."/simout_".$iRun."/outfile.txt";
			}


			$cdata = getSimpleStateData($file);

            //echo "Bin: ".$iBin." Run: ".$iRun."\n";

			//create mean for every run
			if($iBin + 1 == sizeof($vTimekeeper))
            	$vData_raw = array_slice($cdata[1], $vTimekeeper[$iBin][$iRun], sizeof($cdata[1]) - $vTimekeeper[$iBin][$iRun]);
			else
            	$vData_raw = array_slice($cdata[1], $vTimekeeper[$iBin][$iRun], $vTimekeeper[($iBin+1)][$iRun] - $vTimekeeper[$iBin][$iRun]);




            //print_r($vData_raw);

			$iNumDataIt = count($vData_raw);

            //echo "Bin consists of ".$iNumDataIt." iterations\n";

			//now we have the data aray of the bin time period

			//what if bin empty?
			if($iNumDataIt == 0 && $iBin != 0)
			{
				//$vTMP2 = $vRunStateArrays[($iBin - 1)];
				//$vRunStateArrays[$iBin] = $vRunStateArrays[($iBin - 1)];
                array_push($vBinStateArrays, $vLastStateArray);
				//echo "Bin empty! Using previous bin.\n";
				continue;
			}
			elseif($iNumDataIt == 0 && $iBin == 0)
			{
				//insert 0-array
				//$vRunStateArrays[$iBin] = $vStateArray;

				//generate starting state
				$vStartStateArray = array();

				$vData_raw = array_slice($cdata[1], 0, 1);

				$iNumDataIt = 1;

                //$vTMP_StateArray = $vStateArray;
			    //array_push($vBinStateArrays, $vTMP_StateArray);
				//$vLastStateArray = $vTMP_StateArray;
                //print_r($vTMP_StateArray);
                //echo "First bin empty! Starting with initial state.\n";
                //continue;
			}
			//get state vectors

			for($i = 0; $i < $iNumDataIt; $i++)
			{
				$vData_raw_TMP = explode(";", $vData_raw[$i]);
                array_pop($vData_raw_TMP);
                for($j = 0; $j < sizeof($vData_raw_TMP); $j++)
                    $vData_raw_TMP[$j] = explode(":",$vData_raw_TMP[$j])[0];
                array_push($vData,$vData_raw_TMP);
			}

			//create mean over bin

            //print_r($vData);

			for($i = 0; $i < $iNumDataIt; $i++)
			{
				for($iNuc = 0; $iNuc < $iNumNuc; $iNuc++)
				{
                    //echo "It: ".$i." Nucleosome No: ".$iNuc." State: ".$vData[$i][$iNuc]."\n";

					$vTMP_StateArray[$iNuc][$vData[$i][$iNuc]] = $vTMP_StateArray[$iNuc][$vData[$i][$iNuc]] + 1/$iNumDataIt;
				}
			}

            //print_r($vTMP_StateArray);

			//push mean to bin array

			array_push($vBinStateArrays, $vTMP_StateArray);
			$vLastStateArray = $vTMP_StateArray;
		}

        //echo "Bins for Run ".$iRun.":\n";

        //print_r($vBinStateArrays);

		//push binarray to runarray

		array_push($vRunStateArrays, $vBinStateArrays);

	}

   	//print_r($vRunStateArrays);

	//create mean out of runs

	$vRunBinStateArrays = array();
	$vTMP_Nuc = array();


	for($iBin = 0; $iBin < sizeof($vTimekeeper); $iBin++)
	{
		array_push($vRunBinStateArrays, $vStateArray);
	}



	for($iBin = 0; $iBin < sizeof($vTimekeeper); $iBin++)
	{
		for($iRun = 0; $iRun < $iNumRuns; $iRun++)
		{
			for($iNuc = 0; $iNuc < $iNumNuc; $iNuc++)
			{
				foreach($vRunStateArrays[$iRun][$iBin][$iNuc] as $mod => $amount)
				{
					//echo "vRunBinStateArrays: ".$mod." Value: ".$amount." NumRuns: ".$iNumRuns."\n";
					$vRunBinStateArrays[$iBin][$iNuc][$mod] = $vRunBinStateArrays[$iBin][$iNuc][$mod] + $amount / $iNumRuns;
				}
			}
		}
	}

	//print_r($vRunBinStateArrays);



	return $vRunBinStateArrays;
}


//calculates the covariance between two vectors
function calcEqualDistCoVariance($vVar1, $vVar2)
{
	$cross_sum = 0;

	if(sizeof($vVar1) != sizeof($vVar2))
	{
		echo "ERROR!";
		die(0);
	}

	$mean_var1 = calcMeanValue($vVar1);
	$mean_var2 = calcMeanValue($vVar2);


	//echo "Mean 1: ".$mean_var1."\n";
	//echo "Mean 2: ".$mean_var2."\n";

	$numvals = sizeof($vVar1);

	for($k = 0; $k < $numvals; $k++)
	{
		$prod_var1 = ($vVar1[$k]-$mean_var1);
		$prod_var2 = ($vVar2[$k]-$mean_var2);
		$cross_sum = $cross_sum + $prod_var1*$prod_var2;
	}

	//echo "Covariation: ".$cross_sum."\n";
	//echo "Covariance: ".($cross_sum / ($numvals-1))."\n";


	return $cross_sum / ($numvals-1);
}


//calculates the (linear) Pearson correlation coefficient
function calcPearsonCoefficient($vVar1, $vVar2)
{
	return calcEqualDistCoVariance($vVar1, $vVar2)/(sqrt(calcEqualDistVariance($vVar1))*sqrt(calcEqualDistVariance($vVar2)));
}

function ShapiroTest()
{

}

//determines if a point $vPM lies in a rectangle (with edges $vPA, $cPB & $vPC) or not
//all vectors are three dimensional
function pointInRectangle($vPM, $vPA, $vPB, $vPC)
{
    $vAB = array(($vPB[0] - $vPA[0]), ($vPB[1] - $vPA[1]), ($vPB[2] - $vPA[2]));
    $vAM = array(($vPM[0] - $vPA[0]), ($vPM[1] - $vPA[1]), ($vPM[2] - $vPA[2]));
    $vBC = array(($vPC[0] - $vPB[0]), ($vPC[1] - $vPB[1]), ($vPC[2] - $vPB[2]));
    $vBM = array(($vPM[0] - $vPB[0]), ($vPM[1] - $vPB[1]), ($vPM[2] - $vPB[2]));

    $fdotABAM = dotProduct($vAB, $vAM);
    $fdotABAB = dotProduct($vAB, $vAB);
    $fdotBCBM = dotProduct($vBC, $vBM);
    $fdotBCBC = dotProduct($vBC, $vBC);

	if(0 <= $fdotABAM && $fdotABAM <= $fdotABAB && 0 <= $fdotBCBM && $fdotBCBM <= $fdotBCBC)
	    return 1;
	else
		return 0;
}
//TODO which one works?
//alternate calculation
function pointInRectangle2($vPM, $vPA, $vPB, $vPC)
{
    $vAB = array(($vPB[0] - $vPA[0]), ($vPB[1] - $vPA[1]), ($vPB[2] - $vPA[2]));
    $vAC = array(($vPC[0] - $vPA[0]), ($vPC[1] - $vPA[1]), ($vPC[2] - $vPA[2]));

	$vX = array(($vPM[0] - $vPA[0]), ($vPM[1] - $vPA[1]), ($vPM[2] - $vPA[2]));

	$fR = -1;
	$fS = -1;

    if($vPB[0] != 0)
	{
		$fS = ($vX[1] - ($vX[0] * $vPB[1] / $vPB[0])) / ($vPC[1] - $vPC[0] * $vPB[1] / $vPB[0]);
		$fR = $vX[0] - $fS * $vPC[0] / $vPB[0];
	}
	elseif($vPB[0] == 0 && $vPC[0] != 0)
	{
		$fS = $vX[0] / $vPC[0];

		if($vPB[1] != 0)
			$fR = $vX[1] - $fS * $vPC[1] / $vPB[1];
	}
	//elseif($vPC[0] != 0)
	//{
	//	$fR = ($vX[1] - ($vX[0] * $vPC[1] / $vPC[0])) / ($vPB[1] - $vPB[0] * $vPC[1] / $vPC[0]);
	//	$fS = $vX[0] - $fR * $vPB[0] / $vPC[0];
	//}
	//else
	//{
	//	echo "ERROR!\n";
	//	die(0);
	//}

	if($fR >= 0 && $fR <= 1 && $fS >= 0 && $fS <= 1) {
		return 1;
	}
	else {
		return 0;
	}
}

//calculates the dot product of two vectors
function dotProduct($vU, $vV)
{
    return $vU[0] * $vV[0] + $vU[1] * $vV[1] + $vU[2] * $vV[2];
}

?>
