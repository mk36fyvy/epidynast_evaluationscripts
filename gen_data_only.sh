#!/bin/bash

# NUMSIMS: Number of rulefiles generated with which 50 simulations are run respectively
NUMSIMS=100

#MODE:
	# 0: the simulation will output a small file outfile_short.txt which contains only the end state of the system
	# 1: (default) the simulation will output a big file outfile.txt which contains information about every step throughout the run
MODE=1

# COMPRESS:
	# 0: the simulation output files will not be compressed
	# 1: (default) If MODE is 1 the simulation output files will be compressed
COMPRESS=1



#get epidynast
if [ -d "histsim" ]; then
	cd histsim
	git pull
	cd ..
else
	git clone https://gitlab.com/mathemathilda/histsim.git
fi
#compile

cmake histsim

cd histsim
make
# cp HistSim ..
cd ..

rm -R CMakeFiles
rm Makefile
rm cmake_install*
rm CMakeCache.txt


#if [ ! -d "cfg" ]; then
#	mkdir cfg
#fi

# TODO really delete this?
if [ -d "evaluation" ]; then
	rm -R evaluation
fi
mkdir evaluation
mkdir evaluation/cfg


#create config files
cd generate_data
php create_cfgs.php $NUMSIMS
cd ..

RUNS=0


if [[ MODE -eq "0" ]]; then
    # generating small output files

	if [ -d "evaluation/completed_short" ]; then
		rm -R evaluation/completed_short
	fi
	mkdir evaluation/completed_short

	for filename in ./cfg/*.cfg; do
		if [[ RUNS -eq $NUMSIMS ]]; then
			break
		fi

		./histsim/HistSim  --rf "$filename" --sf sample_files/test-sf --pf sample_files/test-pf --nondyn -r 50 -o 0 -d completed_short/"$(basename "$filename" .cfg)"

		let RUNS++

	done

	#params: folder with sims and target time
	php gen_tri_shortfiles.php "./completed_short" 3

else
    # generating big output files

	if [ -d "evaluation/completed" ]; then
		rm -R evaluation/completed
	fi
	mkdir evaluation/completed

	for filename in ./evaluation/cfg/*.cfg; do
		if [[ RUNS -eq $NUMSIMS ]]; then
			break
		fi

		if [[ COMPRESS -eq "1" ]]; then

			./histsim/HistSim  --rf "$filename" --sf sample_files/test-sf --pf sample_files/test-pf --nondyn -r 50 -o 1 -d "$(basename "$filename" .cfg)"

			tar -czf "$(basename "$filename" .cfg)".tar.gz "$(basename "$filename" .cfg)"/
			rm -R "$(basename "$filename" .cfg)"/
			mv "$(basename "$filename" .cfg)".tar.gz ./evaluation/completed/"$(basename "$filename" .cfg)".tar.gz
		else
			./histsim/HistSim  --rf "$filename" --sf sample_files/test-sf --pf sample_files/test-pf --nondyn -r 50 -o 1 -d "$(basename "$filename" .cfg)"
		fi

		let RUNS++

	done


	#params: folder with sims and target time if uncompressed
	if [[ COMPRESS -eq "1" ]]; then
		cd generate_data
		php get_time_state.php "../evaluation/completed/" 3
		cd ..
	else
		cd generate_data
		php get_time_state_uc.php "../evaluation/completed/" 3
		cd ..
	fi

	cd generate_data
	php overview.php "../evaluation/completed" 3
	cd ..

	cd generate_data
    php gen_tri_state.php 3
	cd ..

fi

