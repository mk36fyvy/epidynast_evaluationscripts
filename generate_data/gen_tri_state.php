<?php

ini_set('memory_limit', '6G');
include("../includes/inc_matrix_func.php");
include("../includes/inc_get_data.php");
include("../includes/inc_data_func.php");
include("../includes/inc_fs_tools.php");
include("../includes/inc_eval_func.php");

$fScale = 300;
//$fScale = 1;
$iNumNuc = 60;

$vDataPoints = array();

$fTime = $argv[1];

$fhState = fopen("../evaluation/data/triangle_states_".$fTime,"r");
$fhCoord = fopen("../evaluation/data/triangle_coordinates_".$fTime,"w");

$sTargetState = "{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}";

while (($buffer = fgets($fhState)) !== false)
{
	$vTMP = explode("\t",$buffer);

	$iNumRuns = sizeof($vTMP) - 1;

	$vMeanData = array(0,0,0);
	$vDist = array();
	$vMidDist = array();
	$filename = $vTMP[0];

	$fTriDistance = 0;
	$fMidDistance = 0;
	$fMeanTriSTD = 0;
	//print_r($buffer);

	//print_r($vTMP);

	//for($l=0;$l<sizeof($vTMP);$l++)
	//{
	//	echo sizeof(explode("{",$vTMP[$l]))."\t";
	//
	//}
	//die(0);

	for($i = 1; $i < sizeof($vTMP); $i++)
	{
		$vData = getRightWrong($vTMP[$i], $sTargetState);


		$vMeanData[0] = $vMeanData[0] + $vData[0] / $iNumRuns;
		$vMeanData[1] = $vMeanData[1] + $vData[1] / $iNumRuns;
		$vMeanData[2] = $vMeanData[2] + $vData[2] / $iNumRuns;

//		$fTriDistance = sqrt((1 - $vData[0]) * (1 - $vData[0]) + ($vData[1]) * ($vData[1]) + ($vData[2]) * ($vData[2])) / $iNumNuc * $fScale;

//		echo "sqrt((60 - ".$vData[0].") * (60 - ".$vData[0].") + (".$vData[1].") * (".$vData[1].") + (".$vData[2].") * (".$vData[2]."))\n\n";

//		max distance: 60*sqrt(2) = 84.852813

		$fTriDistance = sqrt((60 - $vData[0]) * (60 - $vData[0]) + ($vData[1]) * ($vData[1]) + ($vData[2]) * ($vData[2]));

		array_push($vDist, $fTriDistance);

	}



	//print_r($vDist);

	//echo "\n".calcSampleSTD($vDist)."\n";

	//die(0);
	$fMeanTriSTD = calcSampleSTD($vDist);

	$fMeanTriDistance = calcMeanValue($vDist);	//scaled and/or normalized


	//echo ($vMeanData[0] / $iNumNuc)."\t".($vMeanData[1] / $iNumNuc)."\t".($vMeanData[2] / $iNumNuc)."\n";

	$vVector = array(array($vMeanData[0] / $iNumNuc * $fScale), array($vMeanData[1] / $iNumNuc * $fScale), array($vMeanData[2] / $iNumNuc * $fScale));


	for($i = 1; $i < sizeof($vTMP); $i++)
	{
		$vData = getRightWrong($vTMP[$i], $sTargetState);

		$fMidDistance = sqrt(($vMeanData[0] - $vData[0]) * ($vMeanData[0] - $vData[0]) + ($vMeanData[1] - $vData[1]) * ($vMeanData[1] - $vData[1]) + ($vMeanData[2] - $vData[2]) * ($vMeanData[2] - $vData[2]));

		array_push($vMidDist, $fMidDistance);

	}

	$fMeanMidSTD = calcSampleSTD($vMidDist);

	$fMeanMidDistance = calcMeanValue($vMidDist);

	$vVector = RotateZ($vVector,deg2rad(-45));
	$vVector = RotateY($vVector,deg2rad(-54.73));
	$vVector = RotateZ($vVector,deg2rad(-90));

	fputs($fhCoord, $filename."\t".$vVector[0][0]."\t".$vVector[1][0]."\t".$vMeanData[0]."\t".$vMeanData[1]."\t".$vMeanData[2]."\t".$fMeanTriDistance."\t".$fMeanTriSTD."\t".$fMeanMidDistance."\t".$fMeanMidSTD."\n");


}

fclose($fhState);

fclose($fhCoord);

?>