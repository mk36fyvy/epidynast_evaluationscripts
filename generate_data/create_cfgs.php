<?php

ini_set('memory_limit', '6G');
//include("../includes/inc_get_data.php");
//include("../includes/inc_eval_func.php");
include("../includes/inc_fs_tools.php");
//include("../includes/inc_vis_func.php");

$files = scandir_ext("../evaluation/cfg","cfg");

$numfiles = sizeof($files);

$maxcfg = $argv[1];

$assocset = array(0,1,2,4,5,10,20,50,100,200,500,1000);
$dissocset = array(0,1,2,4,5,10,20,50,100,200,500,1000);

//echo $maxcfg."\n";
//echo $numfiles."\n";

//for($file = 0; $file < $maxcfg - $numfiles; $file++)
for($file = 0; $file < $maxcfg; $file++)
{


  for($i = 0; $i < 14; $i++)
  {
    $vParameters[$i]["assocrate"] = $assocset[mt_rand(0, count($assocset) - 1)];
    $vParameters[$i]["dissocrate"] = $dissocset[mt_rand(0, count($assocset) - 1)];
  }


	//print_r($vParameters);

	//$filehandle = fopen($filename."_td.cfg","w");

	$filecontent = '
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<enzymeSet>
  <enzymeDef>
    <name enabled="1" value="Noise"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac_rem"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.ac]})"/>
      <rule value="({H3[K4.un]})"/>
      <rate value="'.$vParameters[0]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[0]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Noise"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me_rem"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.me]})"/>
      <rule value="({H3[K4.un]})"/>
      <rate value="'.$vParameters[1]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[1]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Noise"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac_add"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.un]})"/>
      <rule value="({H3[K4.ac]})"/>
      <rate value="'.$vParameters[2]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[2]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Noise"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me_rem"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.un]})"/>
      <rule value="({H3[K4.me]})"/>
      <rate value="'.$vParameters[3]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[3]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Extender"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.un]})"/>
      <rule value="{H3[K4.ac]}({H3[K4.ac]})"/>
      <rate value="'.$vParameters[4]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[4]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="ac_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.un]}){H3[K4.ac]}"/>
      <rule value="({H3[K4.ac]}){H3[K4.ac]}"/>
      <rate value="'.$vParameters[5]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[5]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Extender"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.un]})"/>
      <rule value="{H3[K4.me]}({H3[K4.me]})"/>
      <rate value="'.$vParameters[6]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[6]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="me_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="({H3[K4.un]}){H3[K4.me]}"/>
      <rule value="({H3[K4.me]}){H3[K4.me]}"/>
      <rate value="'.$vParameters[7]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[7]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Truncater"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.ac]}){H3[K4.me]}"/>
      <rule value="{H3[K4.ac]}({H3[K4.un]}){H3[K4.me]}"/>
      <rate value="'.$vParameters[8]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[8]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="ac_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.ac]}){H3[K4.ac]}"/>
      <rule value="{H3[K4.me]}({H3[K4.un]}){H3[K4.ac]}"/>
      <rate value="'.$vParameters[9]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[9]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Truncater"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me_right"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.me]}){H3[K4.ac]}"/>
      <rule value="{H3[K4.me]}({H3[K4.un]}){H3[K4.ac]}"/>
      <rate value="'.$vParameters[10]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[10]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
    <bindingRatesRuleSet>
      <name value="me_left"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.me]}){H3[K4.me]}"/>
      <rule value="{H3[K4.ac]}({H3[K4.un]}){H3[K4.me]}"/>
      <rate value="'.$vParameters[11]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[11]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Corrector"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="ac"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.me]}({H3[K4.ac]}){H3[K4.me]}"/>
      <rule value="{H3[K4.me]}({H3[K4.un]}){H3[K4.me]}"/>
      <rate value="'.$vParameters[12]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[12]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
  <enzymeDef>
    <name enabled="1" value="Corrector"/>
    <size type="symmetric" value="1"/>
    <concentration type="absolute" value="1000"/>
    <bindingRatesRuleSet>
      <name value="me"/>
      <type value="explicitRate"/>
      <enabled value="1"/>
      <target value="{H3[K4.ac]}({H3[K4.me]}){H3[K4.ac]}"/>
      <rule value="{H3[K4.ac]}({H3[K4.un]}){H3[K4.ac]}"/>
      <rate value="'.$vParameters[13]["assocrate"].'"/>
      <dissociationRate type="absolute" value="'.($vParameters[13]["dissocrate"]).'"/>
    </bindingRatesRuleSet>
  </enzymeDef>
</enzymeSet>';

  $tohash = "";

  for($k = 0; $k < 14; $k++)
  {
    $tohash = $tohash.$vParameters[$k]["assocrate"]."_".$vParameters[$k]["dissocrate"]."-";
  }
  $filename = md5($tohash);

  if(file_exists("../evaluation/cfg/".$filename.".cfg"))
    $file--;
  else
    file_put_contents ("../evaluation/cfg/".$filename.".cfg", $filecontent);
	//fclose($filehandle);

	//die(0);
}
?>
