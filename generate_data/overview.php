<?php

ini_set('memory_limit', '6G');
include_once("../includes/inc_get_data.php");
include_once("../includes/inc_eval_func.php");
include_once("../includes/inc_fs_tools.php");
include_once("../includes/inc_vis_func.php");
include_once("../includes/inc_matrix_func.php");
include_once("../includes/inc_data_func.php");

$sPath = $argv[1];

$vFiles = array();
$vFiles = scandir_ext($sPath,"gz");

$bUncompressed = 0;

//find out if uncompressed sims
if(!isset($vFiles) || sizeof($vFiles) == 0)
{
	$vTMP = scandir($sPath);

	for ($i=0; $i<count($vTMP); $i++)
	{
		if($vTMP[$i] == "." || $vTMP[$i] == "..")
		{
			unset($vTMP[$i]);
			continue;
		}

		//echo $sPath."/".$vTMP[$i]."/bash_parameters";

		if(file_exists($sPath."/".$vTMP[$i]."/bash_parameters"))
		{
			echo "Found uncompressed simulations.\n";
			$bUncompressed = 1;
			break;
		}
	}
	if($bUncompressed) {
		$vFiles = array_values($vTMP);
	}
}

sort($vFiles);

$numsims = sizeof($vFiles);


if(isset($argv[1])) {
	$fTargetTime = $argv[2];
}
else {
	$fTargetTime = 3;
}


$fhOverviewHandleComplete = fopen("../evaluation/data/summary_complete_".$fTargetTime,"w");
$fhOverviewHandle = fopen("../evaluation/data/summary_".$fTargetTime,"w");

$vDataPoints = array();

$sTargetState = "{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}";

$vTData = getRightWrong($sTargetState, $sTargetState);

$vT = array(array($vTData[0]), array($vTData[1]), array($vTData[2]));

$vT = RotateZ($vT,deg2rad(-45));
$vT = RotateY($vT,deg2rad(-54.73));
$vT = RotateZ($vT,deg2rad(-90));


for($sim = 0; $sim < $numsims; $sim++)
{

	$sFileName = explode(".",$vFiles[$sim])[0];


	//$fhOverviewHandle = fopen("summary","w");

	echo "Processing ".$sFileName."\n";

	$vSimFolder = array();
	$sRuleFile = "";

	if($bUncompressed)
	{
		$vScan = scandir("./".$sPath."/".$sFileName);

		for ($i=0; $i<count($vScan); $i++)
		{
			//if ($vScan[$i] != '.' && $vScan[$i] != '..' && $vScan[$i] == is_dir($vScan[$i]))
			//	array_push($vSimFolder, $vScan[$i]);
			if(!($vScan[$i] == "bash_parameters" || $vScan[$i] == "outfile_short.txt" || $vScan[$i] == "paramfile" || $vScan[$i] == "rulefile" || $vScan[$i] == "statefile" || $vScan[$i] == "channels.txt" || $vScan[$i] == '.' || $vScan[$i] == '..')) {
				array_push($vSimFolder, $vScan[$i]);
			}
  		}

		$vSimFolder = array_values($vSimFolder);
		$sRuleFile = $sPath."/".$sFileName."/rulefile";
	}
	else
	{
		echo "Extracting ".$vFiles[$sim]."\n";
		system("mkdir ".$sFileName);
		system("tar -xzf ".$sPath."/".$sFileName.".tar.gz -C ./");

		$vScan = scandir("./".$sFileName);

		for ($i=0; $i<count($vScan); $i++)
		{
			//if ($vScan[$i] != '.' && $vScan[$i] != '..' && $vScan[$i] == is_dir($vScan[$i]))
			//	array_push($vSimFolder, $vScan[$i]);
			if(!($vScan[$i] == "bash_parameters" || $vScan[$i] == "outfile_short.txt" || $vScan[$i] == "paramfile" || $vScan[$i] == "rulefile" || $vScan[$i] == "statefile" || $vScan[$i] == "channels.txt" || $vScan[$i] == '.' || $vScan[$i] == '..')) {
				array_push($vSimFolder, $vScan[$i]);
			}
  		}
/*
		for ($i=0; $i<count($vSimFolder); $i++)
		{
			if($vSimFolder[$i] == "bash_parameters" || $vSimFolder[$i] == "outfile_short.txt" || $vSimFolder[$i] == "paramfile" || $vSimFolder[$i] == "rulefile" || $vSimFolder[$i] == "statefile" || $vSimFolder[$i] == "channels.txt")
				unset($vSimFolder[$i]);
		}
*/
		$vSimFolder = array_values($vSimFolder);
		$sRuleFile = "./".$sFileName."/rulefile";


		unset($vSimFolder[(sizeof($vSimFolder)-1)]);

	}

    $iNumRuns = sizeof($vSimFolder);



	echo $sRuleFile;

	if(!file_exists($sRuleFile))
    {
		$sRuleFile = $sPath."/".$sFileName."/".$vSimFolder[0]."/rulefile";
		if(!file_exists($sRuleFile))
    	{
			echo "Error! No outfile found! (Path: ".$sRuleFile.")\n";
			die(0);
		}
	}

	$vParamArray = array();

	$vParameters = parseEnzymes($sRuleFile);



	for($i = 0; $i < sizeof($vParameters); $i++)
	{
		array_push($vParamArray, $vParameters[$i][0]["assocrate"]);
		array_push($vParamArray, $vParameters[$i][0]["dissocrate"]);

		if(isset($vParameters[$i][1]))
		{
			array_push($vParamArray, $vParameters[$i][1]["assocrate"]);
			array_push($vParamArray, $vParameters[$i][1]["dissocrate"]);
		}
	}


//	system("mkdir ".$filename."_enzyme_performance");
//	system("mkdir ".$filename."_triangle");

//print_r($vParameters);
//die(0);
	$vDistances = array();
	$vTriDistances = array();
	$vManDistances = array();

	echo "\n\nProcessing: ".$sFileName."\n";

	$vMeanData = array(0,0,0);
	$vRightWrongData = array();
	$vTimes = array();
	for($run = 0; $run < $iNumRuns; $run++)
	{

		$file = "./".$sFileName."/".$vSimFolder[$run]."/outfile.txt";

		if(!file_exists($file))
        {
			echo "Error! No outfile found! (Path: ".$file.")\n";
			die(0);
		}





  	    $cdata = getSimpleStateData($file);

		$vItData = $cdata[0];


		//get final state (only)
		//targettime support

		$iFinalIt = 0;

		for($k = 0; $k < sizeof($vItData); $k++)
		{
			$iFinalIt = $k-1;
			if(explode(" ",$vItData[$k])[1] >= $fTargetTime)
			{
				$iFinalIt = $k-1;
				break;
			}
		}


		$fTotalTime = explode(" ",$cdata[0][(sizeof($cdata[0])-1)])[1];



		$finalstatedata = $cdata[1][($iFinalIt-1)];
		//$finalstatedata = $cdata[1][(sizeof($cdata[1])-1)];
		$finalstatevec = explode(";",$finalstatedata);

		$finalstatedata = convertOutfileStateToString($finalstatedata);
		$vData = getRightWrong($finalstatedata, $sTargetState);

		array_push($vRightWrongData, $vData);

		$vMeanData[0] = $vMeanData[0] + $vData[0] / $iNumRuns;
		$vMeanData[1] = $vMeanData[1] + $vData[1] / $iNumRuns;
		$vMeanData[2] = $vMeanData[2] + $vData[2] / $iNumRuns;

		$finalstate = "";

		for($l = 0; $l < sizeof($finalstatevec); $l++)
		{
			$finalstate = $finalstate.explode(":",$finalstatevec[$l])[0];
		}



		$vTriDistVec = getDistanceTri($sTargetState, $finalstate);
		$distance = getDistanceLev($sTargetState, $finalstate);

		$iManDistance = getDistanceStep($vData, array(60,0,0));

		$fTriDist = sqrt(($vTriDistVec[0] - 60)*($vTriDistVec[0] - 60) + ($vTriDistVec[1])*($vTriDistVec[1]) + ($vTriDistVec[2])*($vTriDistVec[2]));

		//echo $distance."\n";
		//max tri dist = 84.8528137424
		array_push($vDistances, $distance);
		array_push($vTriDistances, $fTriDist);
		array_push($vManDistances, $iManDistance);
		array_push($vTimes, $fTotalTime);
		//$vStates = array("{}","{H3[K4.me]}","{H3[K4.ac]}");



	}

	$fMeanDist = calcMeanValue($vDistances);
	$fDistSTD = calcSampleSTD($vDistances);
	$fMeanTriDist = calcMeanValue($vTriDistances);
	$fTriDistSTD = calcSampleSTD($vTriDistances);
	$fMeanManDist = calcMeanValue($vManDistances);
	$fManDistSTD = calcSampleSTD($vManDistances);
	$fMeanTime = calcMeanValue($vTimes);


	$vMeanRightWrongData = array(0,0,0,);

	for($k = 0; $k < 3; $k++)
	{
		$vTMP = array();
		for($l = 0; $l < sizeof($vRightWrongData); $l++) {
			array_push($vTMP, $vRightWrongData[$l][$k]);
		}

		$vMeanRightWrongData[$k] = calcMeanValue($vTMP);
	}


	//short hash test

	$hash = "";

	//print_r($vParamArray);


	//not that simple because of coupling of parameters
	for($i = 0; $i < sizeof($vParamArray); $i=$i+2)
	{
		$hash = $hash.$vParamArray[$i]."_".$vParamArray[($i+1)]."-";
	}




	$md5hash = md5($hash);

	fputs($fhOverviewHandle,$md5hash."\t".$fMeanTime."\t".$vMeanRightWrongData[0]."\t".$vMeanRightWrongData[1]."\t".$vMeanRightWrongData[2]."\t".$fMeanDist."\t".$fDistSTD."\t".$fMeanTriDist."\t".$fTriDistSTD."\t".$fMeanManDist."\t".$fManDistSTD);
	fputs($fhOverviewHandleComplete,$md5hash."\t".$fMeanTime."\t".$vMeanRightWrongData[0]."\t".$vMeanRightWrongData[1]."\t".$vMeanRightWrongData[2]."\t".$fMeanDist."\t".$fDistSTD."\t".$fMeanTriDist."\t".$fTriDistSTD."\t".$fMeanManDist."\t".$fManDistSTD);



	for($i = 0; $i < sizeof($vParamArray); $i=$i+2)
	{
		fputs($fhOverviewHandle,"\t".$vParamArray[$i]."\t".$vParamArray[($i+1)]);
		fputs($fhOverviewHandleComplete,"\t".$vParamArray[$i]."\t".$vParamArray[($i+1)]);
	}

	fputs($fhOverviewHandleComplete,"\n");
	fputs($fhOverviewHandle,"\n");

	for($run = 0; $run < $iNumRuns; $run++)
	{
		fputs($fhOverviewHandleComplete,"Run ".$run."\t".$vTimes[$run]."\t".$vRightWrongData[$run][0]."\t".$vRightWrongData[$run][1]."\t".$vRightWrongData[$run][2]."\t".$vDistances[$run]."\t".$vTriDistances[$run]."\t".$vManDistances[$run]."\n");
	}






	if(!$bUncompressed)
	{
    	echo "Removing ".$sFileName."\n";
    	system("rm -R ".$sFileName);
	}
}

?>