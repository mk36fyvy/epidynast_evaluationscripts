<?php
// Is needed by best_worst AND 3_region_param_dist

ini_set('memory_limit', '6G');
include("../includes/inc_get_data.php");

$vSummaryData = readDataFile("../evaluation/data/triangle_coordinates_3"); # TODO: where does this file come from?


//go along neutral axis

$iEnd = 0;

$vSelectedSims = array();

$vSelectedN_RangeSims = array();
$vSelectedT_RangeSims = array();
$vSelectedF_RangeSims = array();

$iTotalOps = 3600;

//echo "Progress: ";

for($N = 60; $N >= 0; $N--)
{

	if($iEnd == 1)
		break;

	for($T = 0; $T < 60; $T++)
	{
		$F = 60 - $N - $T;
		if($F < 0 || $T < 0)
			continue;

		if($iEnd == 1)
			break;
/*
			echo "
Check if N = ".$N." >= ".$vSummaryData[$i][5]." && N + 1 < ".$vSummaryData[$i][5]." && ".$T." >= ".$vSummaryData[$i][3]." && ".$T." < ".$vSummaryData[$i][3]." && ".$F." >= ".$vSummaryData[$i][4]." && ".$F." < ".$vSummaryData[$i][4]."\n
			";

*/
	//		echo "\nN: ".$N."\tT: ".$T."\tF: ".$F."\n";

		//if(((60 - $N) * $T * 100 / 3600) % 10 == 0 )
		//	echo "...".((60 - $N) * $T * 100 / 3600)."%... ";

		for($i = 0; $i < sizeof($vSummaryData); $i++)
		{

			if($iEnd == 1)
				break;




			if($N >= $vSummaryData[$i][5] && ($N - 1) < $vSummaryData[$i][5])
			{
				if(!array_search($vSummaryData[$i][0], $vSelectedN_RangeSims))
					$vSelectedN_RangeSims[] = array($vSummaryData[$i][0],$vSummaryData[$i][3],$vSummaryData[$i][4],$vSummaryData[$i][5]);

				if($T >= $vSummaryData[$i][3] && ($T + 1) < $vSummaryData[$i][3] && $F >= $vSummaryData[$i][4] && ($F + 1) < $vSummaryData[$i][4])
				{
					// echo "yes\t";
					$vSelectedSims[] = array($vSummaryData[$i][0],$vSummaryData[$i][3],$vSummaryData[$i][4],$vSummaryData[$i][5]);
				}
				//else
//					echo "no\t";

				if(sizeof($vSelectedN_RangeSims) == 3000)
				{
					$iEnd = 1;
				}
				if(sizeof($vSelectedSims) == 3000)
				{
					$iEnd = 1;
				}
			}
		}
	}

}



// print_r($vSelectedSims);
// print_r($vSelectedN_RangeSims);


system("mkdir ../evaluation/data/best_worst");

$fhNAxisSelFile = fopen("../evaluation/data/best_worst/worst10_N_sel","w");
$fhNAxisFile = fopen("../evaluation/data/best_worst/worst10_N","w");
$fhDistFile = fopen("../evaluation/data/best_worst/worst10_N_dist","w");

for($i = 0; $i < sizeof($vSelectedN_RangeSims); $i++)
{
	fputs($fhNAxisSelFile,$vSelectedN_RangeSims[$i][0]."\tblack\n");
	fputs($fhNAxisFile,$vSelectedN_RangeSims[$i][0]."\t".$vSelectedN_RangeSims[$i][1]."\t".$vSelectedN_RangeSims[$i][2]."\t".$vSelectedN_RangeSims[$i][3]."\n");

	$fDistance = $vSelectedN_RangeSims[$i][2]*2 + $vSelectedN_RangeSims[$i][3];
	fputs($fhDistFile, $fDistance."\n");
}
fclose($fhNAxisFile);
fclose($fhNAxisSelFile);
fclose($fhDistFile);

//go along right axis

$iEnd = 0;
$vSelectedSims = array();

for($T = 60; $T >= 0; $T--)
{

	if($iEnd == 1)
		break;

	for($F = 0; $F < 60; $F++)
	{
		$N = 60 - $T - $F;
		if($F < 0 || $N < 0)
			continue;

		if($iEnd == 1)
			break;


		for($i = 0; $i < sizeof($vSummaryData); $i++)
		{

			if($iEnd == 1)
				break;

			//echo $T." >= ".$vSummaryData[$i][3]." && ".($T - 1)." < ".$vSummaryData[$i][3]."\n";

			if($T >= $vSummaryData[$i][3] && ($T - 1) < $vSummaryData[$i][3])
			{
				if(!array_search($vSummaryData[$i][0], $vSelectedT_RangeSims))
					$vSelectedT_RangeSims[] = array($vSummaryData[$i][0],$vSummaryData[$i][3],$vSummaryData[$i][4],$vSummaryData[$i][5]);

				if($N >= $vSummaryData[$i][5] && ($N - 1) < $vSummaryData[$i][5] && ($F + 1) > $vSummaryData[$i][4] && $F <= $vSummaryData[$i][4])
				{

					$vSelectedSims[] = array($vSummaryData[$i][0],$vSummaryData[$i][3],$vSummaryData[$i][4],$vSummaryData[$i][5]);
				}

				if(sizeof($vSelectedT_RangeSims) == 3000)
				{
					$iEnd = 1;
				}
				if(sizeof($vSelectedSims) == 3000)
				{
					$iEnd = 1;
				}
			}
		}
	}

}

// print_r($vSelectedSims);
// print_r($vSelectedT_RangeSims);

$fhNAxisSelFile = fopen("../evaluation/data/best_worst/best10_T_sel","w");
$fhNAxisFile = fopen("../evaluation/data/best_worst/best10_T","w");
$fhDistFile = fopen("../evaluation/data/best_worst/best10_T_dist","w");


for($i = 0; $i < sizeof($vSelectedT_RangeSims); $i++)
{
	fputs($fhNAxisSelFile,$vSelectedT_RangeSims[$i][0]."\tblack\n");
	fputs($fhNAxisFile,$vSelectedT_RangeSims[$i][0]."\t".$vSelectedT_RangeSims[$i][1]."\t".$vSelectedT_RangeSims[$i][2]."\t".$vSelectedT_RangeSims[$i][3]."\n");

	$fDistance = $vSelectedT_RangeSims[$i][2]*2 + $vSelectedT_RangeSims[$i][3];
	fputs($fhDistFile, $fDistance."\n");
}
fclose($fhNAxisFile);
fclose($fhNAxisSelFile);
fclose($fhDistFile);
//go along false axis

$iEnd = 0;

for($F = 60; $F >= 0; $F--)
{

	if($iEnd == 1)
		break;

	for($T = 60; $T >= 0; $T--)
	{
		$N = 60 - $F - $T;
		if($N < 0 || $T < 0)
			continue;

		if($iEnd == 1)
			break;


		for($i = 0; $i < sizeof($vSummaryData); $i++)
		{

			if($iEnd == 1)
				break;


			if($F >= $vSummaryData[$i][4] && ($F - 1) < $vSummaryData[$i][4])
			{
				if(!array_search($vSummaryData[$i][0], $vSelectedF_RangeSims))
					$vSelectedF_RangeSims[] = array($vSummaryData[$i][0],$vSummaryData[$i][3],$vSummaryData[$i][4],$vSummaryData[$i][5]);

				if(($N + 1) > $vSummaryData[$i][5] && $N <= $vSummaryData[$i][5] && $T >= $vSummaryData[$i][3] && ($T - 1) < $vSummaryData[$i][3])
				{
					$vSelectedSims[] = array($vSummaryData[$i][0],$vSummaryData[$i][3],$vSummaryData[$i][4],$vSummaryData[$i][5]);
				}

				if(sizeof($vSelectedF_RangeSims) == 3000)
				{
					$iEnd = 1;
				}
				if(sizeof($vSelectedSims) == 3000)
				{
					$iEnd = 1;
				}
			}
		}
	}
}

// print_r($vSelectedSims);
// print_r($vSelectedF_RangeSims);


$fhNAxisSelFile = fopen("../evaluation/data/best_worst/worst10_F_sel","w");
$fhNAxisFile = fopen("../evaluation/data/best_worst/worst10_F","w");
$fhDistFile = fopen("../evaluation/data/best_worst/worst10_F_dist","w");


for($i = 0; $i < sizeof($vSelectedF_RangeSims); $i++)
{
	fputs($fhNAxisSelFile,$vSelectedF_RangeSims[$i][0]."\tblack\n");
	fputs($fhNAxisFile,$vSelectedF_RangeSims[$i][0]."\t".$vSelectedF_RangeSims[$i][1]."\t".$vSelectedF_RangeSims[$i][2]."\t".$vSelectedF_RangeSims[$i][3]."\n");

	$fDistance = $vSelectedF_RangeSims[$i][2]*2 + $vSelectedF_RangeSims[$i][3];
	fputs($fhDistFile, $fDistance."\n");
}
fclose($fhNAxisFile);
fclose($fhNAxisSelFile);
fclose($fhDistFile);
?>
