<?php

ini_set('memory_limit', '6G');
include("../includes/inc_matrix_func.php");
include("../includes/inc_get_data.php");
include("../includes/inc_data_func.php");
include("../includes/inc_fs_tools.php");


//$files = scandir_ext(".","gz");

$sPath = $argv[1];

$vFiles = scandir($sPath);
$vDirs = array();

for($i = 0; $i < sizeof($vFiles); $i++)
{
	if(is_dir($sPath."/".$vFiles[$i]) && $vFiles[$i] != "." && $vFiles[$i] != "..")
	{
		array_push($vDirs, $vFiles[$i]);
		unset($vFiles[$i]);
	}
}

$vFiles = array_values($vFiles);


$numsims = sizeof($vDirs);

$fScale = 300;

$vDataPoints = array();


$fTargetTime = $argv[2];
//$fTargetTime = 2;

if(!$fTargetTime || $fTargetTime == "") {
	die(0);
}


system("mkdir ../evaluation");
system("mkdir ../evaluation/data");
$fhCoord = fopen("evaluation/data/triangle_states_".$fTargetTime,"w");




for($sim = 0; $sim < $numsims; $sim++)
{
	$filename = $vDirs[$sim];

	$hash = explode("_", $vDirs[$sim])[0];

	echo "Processing ".$vDirs[$sim]."\n";



	$scan = scandir($sPath."/".$vDirs[$sim]);



	$path2 = "";

	$vSimFolder = array();

	for ($i=0; $i<count($scan); $i++)
	{
		if ($scan[$i] != '.' && $scan[$i] != '..' && $scan[$i] != 'bash_parameters' && $scan[$i] != 'channels.txt' && $scan[$i] != 'outfile_short.txt' && $scan[$i] != 'paramfile' && $scan[$i] != 'rulefile' && $scan[$i] != 'statefile')// && $scan[$i] == is_dir($scan[$i]))
		{
			//echo $scan[$i];
			$path2 = $scan[$i];
			array_push($vSimFolder, $scan[$i]);

		}

	}

	if($path2 == "") {
		die(0);
	}


    $iNumRuns = sizeof($vSimFolder);


	$vMeanData = array(0,0,0);


	fputs($fhCoord, $filename);



	for($run = 0; $run < $iNumRuns; $run++)
	{

		//triangle rotation (expects points in vector list)

		$sTargetState = "{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}";


		//prepare mean data

		//get time

		$vAllData = getSimpleStateData($sPath."/".$vDirs[$sim]."/".$vSimFolder[$run]."/outfile.txt");

		//echo "datafile: ".$sPath."/".$vDirs[$sim]."/".$vSimFolder[$run]."/outfile.txt\n";

		$vData = $vAllData[0];
		$fin = -1;

		for($it = 0; $it < sizeof($vData); $it++)
		{
			if(explode(" ",$vData[$it])[1] > $fTargetTime)
			{
				$fin = $it;
				break;
			}

		}



		$vData = $vAllData[1];

		if($fin == -1) {
			$fin = sizeof($vData) - 1;
		}



		$sData = $vData[$fin];

//		echo $sData."\n\n";

		$sData = convertOutfileStateToString($sData);

		$sData = str_replace(array("\n", "\t", "\r"), '', $sData);

		$sData = preg_replace('~[[:cntrl:]]~', '', $sData);

		$sData = trim($sData);

		fputs($fhCoord, "\t".$sData);


	}

	fputs($fhCoord, "\n");




}

fclose($fhCoord);


?>
