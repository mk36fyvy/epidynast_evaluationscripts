<?php

ini_set('memory_limit', '6G');
include("../includes/inc_matrix_func.php");
include("../includes/inc_get_data.php");
include("../includes/inc_data_func.php");
include("../includes/inc_fs_tools.php");




$sPath = $argv[1];

$vFiles = scandir_ext($sPath,"gz");



$numsims = sizeof($vFiles);

$fScale = 300;

$vDataPoints = array();


$fTargetTime = $argv[2];
//$fTargetTime = 2;

if(!$fTargetTime || $fTargetTime == "") {
	die(0);
}

//print_r($argv);
//die(0);

// system("mkdir ../evaluation");
system("mkdir ../evaluation/data");
$fhCoord = fopen("../evaluation/data/triangle_states_".$fTargetTime,"w");




for($sim = 0; $sim < $numsims; $sim++)
{
	$filename = explode(".",$vFiles[$sim])[0];

	// $hash = explode("_", $vFiles[$sim])[0];

	echo "Processing ".$vFiles[$sim]."\n";

	echo "Extracting ".$vFiles[$sim]."\n";
	system("mkdir ".$filename);
	system("tar -xzf ".$sPath."/".$filename.".tar.gz -C ./");

	$scan = scandir("./".$filename);

	$path2 = "";

	// will contain path of structure hash/run_X/run_outfile (unpacked)
	$vSimFolder = array();

	for ($i=0; $i<count($scan); $i++)
	{
		if ($scan[$i] != '.' && $scan[$i] != '..')// && $scan[$i] == is_dir($scan[$i]))
		{
			if ($scan[$i] != '.' && $scan[$i] != '..' && $scan[$i] != 'bash_parameters' && $scan[$i] != 'channels.txt' && $scan[$i] != 'outfile_short.txt' && $scan[$i] != 'paramfile' && $scan[$i] != 'rulefile' && $scan[$i] != 'statefile')// && $scan[$i] == is_dir($scan[$i]))
			{
				//echo $scan[$i];
				$path2 = $scan[$i];
				array_push($vSimFolder, $scan[$i]);
			}
		}

	}

	if($path2 == "") {
		die(0);
	}


    $iNumRuns = sizeof($vSimFolder);


	$vMeanData = array(0,0,0);


	fputs($fhCoord, $filename);



	for($run = 0; $run < $iNumRuns; $run++)
	{

		//triangle rotation (expects points in vector list)

		$sTargetState = "{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.me]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}{H3[K4.ac]}";


		//prepare mean data

		//get time

		$vData = getSimpleStateData("./".$filename."/".$vSimFolder[$run]."/outfile.txt");

		$vItData = $vData[0]; // outfile headers
		$fin = -1;

		for($it = 0; $it < sizeof($vItData); $it++)
		{
			if(explode(" ",$vItData[$it])[1] > $fTargetTime)
			{
				$fin = $it;
				break;
			}

		}



		//$cData = getSimpleData($sPath."/".$filename."/".$vSimFolder[$run]."/outfile.txt");

		if($fin == -1)
		{
			$fin = sizeof($vItData) - 1;
			//$sData = $vData[1][$fin];
		}
		//else
		$sData = $vData[1][$fin]; // contains last iteration

//		echo $sData."\n\n";

		$sData = convertOutfileStateToString($sData); // disposes of bound enzymes info in order to get same format as target state

		$sData = str_replace(array("\n", "\t", "\r"), '', $sData);

		$sData = preg_replace('~[[:cntrl:]]~', '', $sData);

		$sData = trim($sData);

		fputs($fhCoord, "\t".$sData);


	}

	fputs($fhCoord, "\n");


    system("rm -R ".$filename);

}

fclose($fhCoord);


?>
