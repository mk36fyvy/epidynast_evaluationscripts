# _EpiDynaST_: Evaluation Scripts

This repository contains several scripts that evaluate [_EpiDynaST_](https://gitlab.com/mathemathilda/histsim) (working title: HistSim) output.

## Table of contents

-   [_EpiDynaST_: Evaluation Scripts](#EpiDynaST-evaluation-scripts)
    -   [Table of contents](#table-of-contents)
    -   [What is _EpiDynaST_?](#what-is-EpiDynaST)
    -   [How to use this repo](#how-to-use-this-repo)
    -   [General Script Details](#script-details)
        -   [run_all.sh](#run_allsh)
        -   [gen_data_only.sh](#gen_data_onlysh)
        -   [run_script.sh](#runscriptsh)
        -   [gen_tri_state.php](#gen_tri_statephp)
        -   [get_best_worst.php](#get_best_worstphp)
        -   [create_cfgs.php](#create_cfgsphp)
        -   [get_time_state.php](#get_time_statephp)
        -   [overview.php](#overviewphp)
    -   [Evaluation Script Details](#evaluation-script-details)
        -   [gen_tri_plot](#gen_tri_plot)
        -   [3_region_param_distribution](#3_region_param_distribution)
        -   [best_worst](#best_worst)

## What is _EpiDynaST_?

_EpiDynaST_ stands for **Epi**genetic **Dyna**mics **S**imulation **T**ool. It is a histone post-translational modification (PTM) simulation software which, at its core, uses Gillespie's algorithm in order to compute the dynamic changes of modification patterns on a chain of nucleosomes as a function of time.

The main working mechanism of _EpiDynaST_ may be outlined as follows: First, one defines an enzyme rule set ([rulefile](sample_files/sample-rf)), a parameter file ([paramfile](sample_files/test-pf)) and, lastly, a starting nucleosome string ([statefile](sample_files/test-sf)) which is defined as an array of nucleosomes reduced to their PTMs. Then, _EpiDynaST_ simulates the stochastic time-dependent change of said modifications on the string, exactly one event every time step. The two events that can occur for each enzyme are either an association step or a reaction step that immediately entails the enzyme’s dissociation from the nucleosome. The enzyme rule sets simply describe a pattern (context) on the string, that is then changed according to the rule.

## How to use this repo

### Requirements

For running _EpiDynaST_, you need

-   a C++ compiler like [`gcc`](https://gcc.gnu.org/install/)
-   the [boost library](https://www.boost.org/)

In order to run the evaluation scripts, you need

-   [php](https://www.php.net/)
-   [gnuplot](http://www.gnuplot.info/)

installed on your machine.

### Run the scripts

There are multiple files in the outer directory which facilitate the use of this repository. You can either execute the command [`run_all.sh`](#run_allsh)

```
./run_all.sh
```

in order to download and compile _EpiDynaST_ (if not already present on your machine), generate data and running all evaluation scripts featured in this repo.

Alternatively, you can execute the commands [`gen_data_only.sh`](#gen_data_onlysh) and [`run_script.sh`](#run_scriptsh) like so

```
./gen_data_only.sh
./run_script.sh {SCRIPT}
```

[`run_script.sh`](#run_scriptsh) takes exactly one argument `{SCRIPT}` which is the name of one evaluation script from [this](#run_scriptsh) list.

## Script Details

### run_all.sh

[Jump to file](./run_all.sh 'See the file in the repo')

Runs [`gen_data_only.sh`](#gen_data_onlysh) followed by [`run_script.sh`](#run_scriptsh) executed for every evaluation script available in the repo.

### gen_data_only.sh

[Jump to file](./gen_data_only.sh 'See the file in the repo')

Installs and compiles _EpiDynaST_ if not already installed on your system. Then, [`create_cfgs.php`](#create_cfgsphp) creates `NUMSIMS` (default: 100) enzyme rulefiles. After that, these generated rulefiles are used together with a [statefile](sample_files/test-sf) and this [paramfile](sample_files/test-pf) to run 50\*`NUMSIMS` _EpiDynaST_ simulations.

Finally, calls [`get_time_state.php`](#get_time_statephp) (or [`get_time_state_uc.php`](#get_time_statephp) if outfiles uncompressed) and [`overview.php`](#overviewphp).

Important parameters (hardcoded) in the file:

-   `NUMSIMS`: Number of rulefiles generated with which 50 simulations are run respectively
-   `MODE`:
    -   `0` : the simulation will output a small file `outfile_short.txt` which contains only the end state of the system
    -   `1` : (**default**) the simulation will output a big file `outfile.txt` which contains meta information about every step throughout the run (this meta info is needed for many evaluation scripts. If you intend to use [`run_all.sh`](#run_allsh), leave this at default.)
-   `COMPRESS`:
    -   `0` : the simulation output files will not be compressed
    -   `1` : (**default**) If `MODE` is `1` the simulation output files will be compressed

### run_script.sh

[Jump to file](./run_script.sh 'See the file in the repo')

Command:

```bash
./run_script.sh {SCRIPT}
```

**Note**: Please run [`gen_data_only.sh`](#gen_data_onlysh) first before running this script.

Runs the evaluation script specified by `{SCRIPT}`. The following evaluation scripts are featured in this repo:

-   [`triplot`](#gen_tri_plot)
-   [`param_dist`](#3_region_param_distribution)
-   [`best_worst`](#best_worst)

### gen_tri_state.php

[Jump to file](./generate_data/gen_tri_state.php 'See the file in the repo')

Is called by [`gen_data_only.sh`](#gen_data_onlysh) and does not need to be run by the user.

Takes data from [get_time_state.php](#get_time_statephp) and outputs a list of the simulations with their coordinates on the triplot. Additionally, outputs the number of right, wrong and neutral nucleosomes on the string and their distance to the triplot corners.

### get_best_worst.php

[Jump to file](./generate_data/get_best_worst.php 'See the file in the repo')

Is called when using [`param_dist`](#3_region_param_distribution) or [`best_worst`](#best_worst) and does not need to be run by the user.

Outputs list of hashes of the 10 percent best, worst and neutral (3 corners of the triplot) simulations.

### create_cfgs.php

[Jump to file](./generate_data/create_cfgs.php 'See the file in the repo')

Is called by [`gen_data_only.sh`](#gen_data_onlysh) and does not need to be run by the user.

This script generates [rulefile](sample_files/sample-rf))s that are used for the simulation runs. By default, the rulefile contains the following enzymes (for acetylation as well as methylation of K4 respectively):

-   Noise enzymes (adders, removers)
-   Extenders (adders, removers)
-   Truncators
-   Correctors

The association and dissociation rates are chosen randomly.

### get_time_state.php

[Jump to file for compressed outfiles](./generate_data/get_time_state.php 'See the file in the repo')

[Jump to file for uncompressed outfiles](./generate_data/get_time_state_uc.php 'See the file in the repo')

Is called by [`gen_data_only.sh`](#gen_data_onlysh) and does not need to be run by the user.

Writes all the end states from all the simulations at a given time.

### overview.php

[Jump to file](./generate_data/gen_tri_state.php 'See the file in the repo')

Is called by [`gen_data_only.sh`](#gen_data_onlysh) and does not need to be run by the user.

Includes all simulations and indicates the number of right, wrong, neutral nucleosome positions, the run time and all parameter values of every simulation.

Outputs 2 files:

-   `summary_X`: Includes the aforementionned values averaged over all runs of one simulation. `X`is the time parameter of the simulation.
-   `summary_complete_X`: Includes all data from all simulations without taking the runs' mean.

## Evaluation Script Details

### gen_tri_plot

[Jump to directory](./eval_scripts/gen_tri_plot/ 'See the directory in the repo')

Command:

```bash
./run_script.sh triplot
```

Generates a triplot.

### 3_region_param_distribution

[Jump to directory](./eval_scripts/3_region_param_distribution/ 'See the directory in the repo')

Command:

```bash
./run_script.sh param_dist ../../evaluation/data/best_worst/best10_T_sel 0 1
```

This script...

### best_worst

[Jump to directory](./eval_scripts/best_worst/ 'See the directory in the repo')

Command:

```bash
./run_script.sh best_worst
```

This script...
