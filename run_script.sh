#!/bin/bash

# Transforming to lower case in order to make matching case-insensitive
name=$(echo $1 | tr '[:upper:]' '[:lower:]')

list=(
    "all"
    "triplot"
    "param_dist"
    "best_worst"
    "new_script" # ALL LOWER CASE
)

if [ ! -d "evaluation" ]; then
	printf "Please run\n\n\t./gen_data_only.sh\n\nfirst\n"
    exit
fi

# cd's are needed because of the relative includes in the php files
case $name in
    all)
        cd eval_scripts/gen_tri_plot
        php gen_tri_plot.php 3

        cd ../../generate_data
        php get_best_worst.php  # triplot

        cd ../eval_scripts/3_region_param_distribution
        php ra_vs_rd_paperplot.php best10_T_sel 0 1  # best_worst

        cd ../best_worst
        php best_worst_param_dist.php  # param_dist

        cd ../..
        # ADD EVERY NEW SCRIPT EXECUTION HERE # SCRIPT NAME
        ;;
    triplot)
        cd eval_scripts/gen_tri_plot/
        php gen_tri_plot.php 3
        cd ../..
        ;;
    param_dist)
        cd ./generate_data
        php get_best_worst.php

        cd ../eval_scripts/3_region_param_distribution
        php ra_vs_rd_paperplot.php 3 $1 $2 $3
        cd ../..
        ;;
    best_worst)
        cd generate_data
        php get_best_worst.php

        cd ../eval_scripts/best_worst
        php best_worst_param_dist.php
        cd ../..
        ;;
    new_script)
        # HOW IS THE NEW SCRIPT EXECUTED?
        echo 'This new script is yet to be implemented'
        ;;
    *)
        echo "Please enter one of the following as your first (and only) argument: ${list[@]}"
        ;;
esac
