<?php

$iPicWidth = 1980;
$iPicHeight = 1980;
$iMarginX = 40;
$iMarginY = 40;
$iLegendWidth = 60; 
$iCaptionX = 60;
$iCaptionY = 60;

$svg = '

<svg width="100%" height="100%" viewBox="0 0 '.(4*$iPicWidth + 3*$iMarginX + 2*$iCaptionX + 2*$iLegendWidth).' '.(4*$iPicHeight + 3*$iMarginY + 2*$iCaptionY).'" version="1.1"
     xmlns="http://www.w3.org/2000/svg"
     xmlns:xlink="http://www.w3.org/1999/xlink" viewport-fill="black">
<rect x="0" y="0" width="7920" height="7920" style="fill: white;"/>
  <image x="0" y="0" width="'.(2*$iPicWidth).'" height="'.(2*$iPicWidth).'" xlink:href="regions.svg" />

<rect x="'.(2*$iCaptionX + 2*$iPicWidth + $iLegendWidth + 2*$iMarginX).'" y="'.$iCaptionY.'" width="'.(2*$iPicWidth + $iMarginX).'" height="'.(2*$iPicWidth + $iMarginY).'" style="fill: #10aa10; fill-opacity: 0.2;"/>

  <image x="'.(2*$iCaptionX + 2*$iPicWidth + $iLegendWidth + 2*$iMarginX).'" y="'.$iCaptionY.'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par1_vs_par0_best10_T_sel.svg" />
  <image x="'.(2*$iCaptionX + 3*$iPicWidth + $iLegendWidth + 3*$iMarginX).'" y="'.$iCaptionY.'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par5_vs_par0_best10_T_sel.svg" />
  <image x="'.(2*$iCaptionX + 2*$iPicWidth + $iLegendWidth + 2*$iMarginX).'" y="'.($iCaptionY + $iPicWidth).'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par1_vs_par4_best10_T_sel.svg" />
  <image x="'.(2*$iCaptionX + 3*$iPicWidth + $iLegendWidth + 3*$iMarginX).'" y="'.($iCaptionY + $iPicWidth).'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par5_vs_par4_best10_T_sel.svg" />

  <image x="'.(4*$iPicWidth + $iLegendWidth + 3*$iMarginX).'" y="'.($iCaptionY).'" width="'.$iLegendWidth.'" height="'.(2*$iPicWidth).'" xlink:href="legend.svg" />
  <rect x="'.(4*$iPicWidth + $iLegendWidth + 3*$iMarginX).'" y="'.($iCaptionY).'" width="'.$iLegendWidth.'" height="'.(2*$iPicWidth).'" style="fill: #ffd42a; fill-opacity: 0.2;"/>


  <rect x="'.$iCaptionX.'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.(2*$iPicWidth + $iMarginX).'" height="'.(2*$iPicWidth + $iMarginY).'" style="fill: #0000cd; fill-opacity: 0.2;"/>

  <image x="'.$iCaptionX.'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par1_vs_par0_worst10_F_sel.svg" />
  <image x="'.($iPicWidth + $iMarginX + $iCaptionX).'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par5_vs_par0_worst10_F_sel.svg" />
  <image x="'.$iCaptionX.'" y="'.(2*$iCaptionY + 3 * $iPicHeight + $iMarginY).'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par1_vs_par4_worst10_F_sel.svg" />
  <image x="'.($iPicWidth + $iMarginX + $iCaptionX).'" y="'.(2*$iCaptionY + 3 * $iPicHeight + $iMarginY).'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par5_vs_par4_worst10_F_sel.svg" />

  <image x="'.(2*$iPicWidth + $iMarginX + $iCaptionX).'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.$iLegendWidth.'" height="'.(2*$iPicWidth + $iMarginY).'" xlink:href="legend.svg" />
  <rect x="'.(2*$iPicWidth + $iMarginX + $iCaptionX).'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.$iLegendWidth.'" height="'.(2*$iPicWidth + $iMarginY).'" style="fill: #ffd42a; fill-opacity: 0.2;"/>


<rect x="'.(2*$iPicWidth + $iLegendWidth + 2*$iMarginX + 2*$iCaptionX).'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.(2*$iPicWidth + $iMarginX).'" height="'.(2*$iPicWidth + $iMarginY).'" style="fill: #8b0000; fill-opacity: 0.2;"/>

  <image x="'.(2*$iCaptionX + 2*$iPicWidth + $iLegendWidth + 2*$iMarginX).'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par1_vs_par0_worst10_N_sel.svg" />
  <image x="'.(2*$iCaptionX + 3*$iPicWidth + $iLegendWidth + 3*$iMarginX).'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par5_vs_par0_worst10_N_sel.svg" />
  <image x="'.(2*$iCaptionX + 2*$iPicWidth + $iLegendWidth + 2*$iMarginX).'" y="'.(2*$iCaptionY + 3 * $iPicHeight + $iMarginY).'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par1_vs_par4_worst10_N_sel.svg" />
  <image x="'.(2*$iCaptionX + 3*$iPicWidth + $iLegendWidth + 3*$iMarginX).'" y="'.(2*$iCaptionY + 3 * $iPicHeight + $iMarginY).'" width="'.$iPicWidth.'" height="'.$iPicWidth.'" xlink:href="par5_vs_par4_worst10_N_sel.svg" />

  <image x="'.(4*$iPicWidth + $iLegendWidth + 3*$iMarginX).'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.$iLegendWidth.'" height="'.(2*$iPicWidth + $iMarginY).'" xlink:href="legend.svg" />
  <rect x="'.(4*$iPicWidth + $iLegendWidth + 3*$iMarginX).'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.$iLegendWidth.'" height="'.(2*$iPicWidth + $iMarginY).'" style="fill: #ffd42a; fill-opacity: 0.2;"/>


<!-- caption -->
<rect x="0" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.($iCaptionX).'" height="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" style="fill: #aaff55; fill-opacity: 0.4;"/>
<rect x="'.($iCaptionX + 2*$iPicWidth + $iLegendWidth + 2*$iMarginX).'" y="0" width="'.($iCaptionX).'" height="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" style="fill: #aaff55; fill-opacity: 0.4;"/>
<rect x="'.($iCaptionX + 2*$iPicWidth + $iLegendWidth + 2*$iMarginX).'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.($iCaptionX).'" height="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" style="fill: #aaff55; fill-opacity: 0.4;"/>

<rect x="0" y="0" width="'.($iCaptionX).'" height="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" style="fill: #aaff55; fill-opacity: 0.4;"/>
<rect x="'.(2*$iPicWidth + $iLegendWidth + 2*$iMarginX + $iCaptionX).'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.($iCaptionX).'" height="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" style="fill: #aaff55; fill-opacity: 0.4;"/>
<rect x="'.(2*$iPicWidth + $iLegendWidth + 2*$iMarginX + $iCaptionX).'" y="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" width="'.($iCaptionX).'" height="'.(2*$iCaptionY + 2 * $iPicHeight + $iMarginY).'" style="fill: #aaff55; fill-opacity: 0.4;"/>


<!-- margin -->
<rect x="'.($iCaptionX + $iPicWidth).'" y="0" width="'.($iMarginX).'" height="'.(4*$iPicHeight + 3*$iMarginY + 2*$iCaptionY).'" style="fill: #aa8855; fill-opacity: 0.2;"/>
<rect x="'.($iCaptionX + 2*$iPicWidth + $iLegendWidth + $iMarginX).'" y="0" width="'.($iMarginX).'" height="'.(4*$iPicHeight + 3*$iMarginY + 2*$iCaptionY).'" style="fill: #aa8855; fill-opacity: 0.2;"/>
<rect x="'.(2*$iCaptionX + 3*$iPicWidth + $iLegendWidth + 2*$iMarginX).'" y="0" width="'.($iMarginX).'" height="'.(4*$iPicHeight + 3*$iMarginY + 2*$iCaptionY).'" style="fill: #aa8855; fill-opacity: 0.2;"/>

<rect x="0" y="'.($iPicHeight).'" width="'.(4*$iPicWidth + 3*$iMarginX + 2*$iCaptionX + 2*$iLegendWidth).'" height="'.($iMarginY).'" style="fill: #aa8855; fill-opacity: 0.2;"/>
<rect x="0" y="'.(2*$iPicHeight + $iMarginY + $iCaptionY).'" width="'.(4*$iPicWidth + 3*$iMarginX + 2*$iCaptionX + 2*$iLegendWidth).'" height="'.($iMarginY).'" style="fill: #aa8855; fill-opacity: 0.2;"/>
<rect x="0" y="'.(3*$iPicHeight + 2*$iMarginY + 2*$iCaptionY).'" width="'.(4*$iPicWidth + 3*$iMarginX + 2*$iCaptionX + 2*$iLegendWidth).'" height="'.($iMarginY).'" style="fill: #aa8855; fill-opacity: 0.2;"/>


  <text x = "10" y = "25" font-size = "20"><tspan font-family="Arial" >remover r<tspan font-size = "12" baseline-shift = "sub">d</tspan></tspan></text>
  <text x = "10" y = "25" font-size = "20"><tspan font-family="Arial" >extender r<tspan font-size = "12" baseline-shift = "sub">d</tspan></tspan></text>
  <text x = "10" y = "25" font-size = "20"><tspan font-family="Arial" >remover r<tspan font-size = "12" baseline-shift = "sub">a</tspan></tspan></text>
  <text x = "10" y = "25" font-size = "20"><tspan font-family="Arial" >extender r<tspan font-size = "12" baseline-shift = "sub">a</tspan></tspan></text>
</svg>

';

$fhSVG = fopen("bestworst10.svg","w");
fputs($fhSVG, $svg);
fclose($fhSVG);

?>
