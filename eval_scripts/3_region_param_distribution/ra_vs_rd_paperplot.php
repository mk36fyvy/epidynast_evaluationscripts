#!/usr/bin/php
<?php
ini_set('memory_limit', '6G');
include("../../includes/inc_matrix_func.php");
include("../../includes/inc_get_data.php");
include("../../includes/inc_data_func.php");
include("../../includes/inc_fs_tools.php");



$fTargetTime = $argv[1];

$fhOverviewHandle = fopen("../../evaluation/data/summary_".$fTargetTime,"r");

$vSims = array();
$vSelectedSims = array();
$vAllSims = array();

$iThreshold = 884;

$sSelectionFile = $argv[2];

$fhSelectionHandle = fopen($sSelectionFile,"r");

while (($buffer = fgets($fhSelectionHandle, 4096)) !== false)
{
	$vTMP = explode("\t",$buffer);

	$vSelectedSims[] = $vTMP[0];

}

fclose($fhSelectionHandle);


while (($buffer = fgets($fhOverviewHandle, 4096)) !== false)
{
	$vTMP = explode("\t",$buffer);

	$vAllSims[] = $vTMP;
}

fclose($fhOverviewHandle);
//if(array_search($vTMP[0], $vSelectedSims))



//due to strange search error

for($i = 0; $i < sizeof($vSelectedSims); $i++)
{
	$iCheck = 0;
	for($j = 0; $j < sizeof($vAllSims); $j++)
	{
		if($vAllSims[$j][0] == $vSelectedSims[$i])
		{
			$vSims[] = $vAllSims[$j];
			$iCheck = 1;
			break;
		}
	}

	if($iCheck == 0)
	{
		echo "Error: ".$vSelectedSims[$i]." not found!";
		die(0);
	}
}

//print_r($vSims);

//compare with array_search:
/*$vSims = array();

for($j = 0; $j < sizeof($vAllSims); $j++)
{
	if(array_search($vAllSims[$j][0],$vSelectedSims) !== FALSE)
		$vSims[] = $vAllSims[$j];
}


print_r($vSims);

die(0);
*/

array_multisort (array_column($vSims, 1), SORT_ASC, $vSims);


//print_r($vSims);
//print_r($vBad);
//ac rem

//die(0);
//build matrix
//0	1	2	3	4	5	6	7	8	9	10	11
//0	1	2	4	5	10	20	50	100	200	500	1000

$vCols = array_fill(0,12,0);

$vSimsValueMatrix = array_fill(0,12,$vCols);



$par1 = $argv[3] + 11;
$par2 = $argv[4] + 11;


//ac
for($i = 0; $i < sizeof($vSims); $i++)
{
	$r_a = -1;
	$r_d = -1;

	if($vSims[$i][$par1] == 0)
		$r_a = 0;
	elseif($vSims[$i][$par1] == 1)
		$r_a = 1;
	elseif($vSims[$i][$par1] == 2)
		$r_a = 2;
	elseif($vSims[$i][$par1] == 4)
		$r_a = 3;
	elseif($vSims[$i][$par1] == 5)
		$r_a = 4;
	elseif($vSims[$i][$par1] == 10)
		$r_a = 5;
	elseif($vSims[$i][$par1] == 20)
		$r_a = 6;
	elseif($vSims[$i][$par1] == 50)
		$r_a = 7;
	elseif($vSims[$i][$par1] == 100)
		$r_a = 8;
	elseif($vSims[$i][$par1] == 200)
		$r_a = 9;
	elseif($vSims[$i][$par1] == 500)
		$r_a = 10;
	elseif($vSims[$i][$par1] == 1000)
		$r_a = 11;

	if($vSims[$i][$par2] == 0)
		$r_d = 0;
	elseif($vSims[$i][$par2] == 1)
		$r_d = 1;
	elseif($vSims[$i][$par2] == 2)
		$r_d = 2;
	elseif($vSims[$i][$par2] == 4)
		$r_d = 3;
	elseif($vSims[$i][$par2] == 5)
		$r_d = 4;
	elseif($vSims[$i][$par2] == 10)
		$r_d = 5;
	elseif($vSims[$i][$par2] == 20)
		$r_d = 6;
	elseif($vSims[$i][$par2] == 50)
		$r_d = 7;
	elseif($vSims[$i][$par2] == 100)
		$r_d = 8;
	elseif($vSims[$i][$par2] == 200)
		$r_d = 9;
	elseif($vSims[$i][$par2] == 500)
		$r_d = 10;
	elseif($vSims[$i][$par2] == 1000)
		$r_d = 11;

	//$vSimsValueMatrix[$r_a][$r_d]++;
	$vSimsValueMatrix[$r_d][$r_a]++;
}



$vValTranslate[0] = 0;
$vValTranslate[1] = 1;
$vValTranslate[2] = 2;
$vValTranslate[3] = 4;
$vValTranslate[4] = 5;
$vValTranslate[5] = 10;
$vValTranslate[6] = 20;
$vValTranslate[7] = 50;
$vValTranslate[8] = 100;
$vValTranslate[9] = 200;
$vValTranslate[10] = 500;
$vValTranslate[11] = 1000;


$fhGoodMatHandle = fopen("ra_rd_data","w");

$iSum = 0;

fputs($fhGoodMatHandle, "\t");
for($i = 0; $i < sizeof($vSimsValueMatrix); $i++)
{
	fputs($fhGoodMatHandle,$vValTranslate[$i]);
	if($i != 11)
		fputs($fhGoodMatHandle, "\t");
	else
		fputs($fhGoodMatHandle, "\n");
}

for($i = 0; $i < sizeof($vSimsValueMatrix); $i++)
{
	fputs($fhGoodMatHandle, $vValTranslate[$i]."\t");
	for($j = 0; $j < sizeof($vSimsValueMatrix[$i]); $j++)
	{
		if($j != 11)
			fputs($fhGoodMatHandle, $vSimsValueMatrix[$i][$j]."\t");
		else
			fputs($fhGoodMatHandle, $vSimsValueMatrix[$i][$j]."\n");
		if($j == 0)
			$iSum = $iSum + $vSimsValueMatrix[$i][$j];
	}
}

//echo sizeof($vSims)."\n";
//echo $iSum."\n";

fclose($fhGoodMatHandle);

$iMax = 0;

for($i = 0; $i < sizeof($vSimsValueMatrix); $i++)
{
	for($j = 0; $j < sizeof($vSimsValueMatrix[$i]); $j++)
	{
		if($vSimsValueMatrix[$i][$j] > $iMax)
			$iMax = $vSimsValueMatrix[$i][$j];
	}
}

$fhPlotHandle = fopen("ra_vs_rd.gplt","w");

$sSelectionFileName = array_pop(explode("/", $sSelectionFile));


$gplt = '

set datafile separator "\t"
set autoscale fix
#set key outside right center
set terminal svg size 1080,1080 font ",20"
set output "par'.($par1-11).'_vs_par'.($par2-11).'_'.$sSelectionFileName.'.svg"

set object 1 rect from screen 0, 0, 0 to screen 1, 1, 0 behind
set object 1 rect fc  rgb "white"  fillstyle solid 1.0


set key font ",20"
set xtics font ",20"
set ytics font ",20"
set xlabel font ",40"
set ylabel font ",40"


#set multiplot layout 1,2

';

if(($par2-11) % 2 == 0)
{
	//$gplt = $gplt.'set xlabel "r_a" font ",20"
//';
	$gplt = $gplt.'set xlabel "r_d" font ",20"
';
	$titletext_par2 = 'association rate';
}
else
{
	$gplt = $gplt.'set xlabel "r_a" font ",20"
';
	$titletext_par2 = 'dissociation rate';
}
if(($par1-11) % 2 == 0)
{
	$gplt = $gplt.'set ylabel "r_d" font ",20" rotate by 0
';
	$titletext_par1 = 'Association rate';
}
else
{
	$gplt = $gplt.'set ylabel "r_a" font ",20"
';
	$titletext_par1 = 'Dissociation rate';
}





$gplt = $gplt.'


unset key
set size square



#set cbrange[0:200]

#set pal gray
#set palette defined (0 "blue", 0.5 "white", 1 "red")
#set palette defined (0 "red", 0.5 "white", 1 "green")

threshold = '.($iMax*0.9).'

#set nokey
plot "ra_rd_data" matrix rowheaders columnheaders with image pixels
';
//, "" matrix rowheaders columnheaders using 1:2:($3 > threshold ? sprintf("%d", $3) : sprintf("")) with labels tc "black" , "" matrix rowheaders columnheaders using 1:2:($3 < threshold ? sprintf("%d", $3) : sprintf("")) with labels tc "white"';
//, "" matrix using 1:2:(sprintf("%.2f", $3)) with labels tc "white" font ",10"

//set title "'.$titletext_par1.' of enzyme '.(floor(($par1-11)/2)).' vs. '.$titletext_par2.' of enzyme '.(floor(($par2-11)/2)).'"
/*

'' matrix using 1:2:($3 > threshold ? sprintf('%1.2f', $3) : sprintf("")) with labels tc "white" ,\
     '' matrix using 1:2:($3 < threshold ? sprintf('%1.2f', $3) : sprintf("")) with labels tc "black"

*/

fputs($fhPlotHandle, $gplt);
fclose($fhPlotHandle);




system('/usr/bin/gnuplot ra_vs_rd.gplt');
system("rm -R ra_vs_rd.gplt");
system('mv ra_rd_data ../../evaluation/data');
system('mv par'.($par1-11).'_vs_par'.($par2-11).'_'.$sSelectionFileName.'.svg ../../evaluation/plots/');



?>
