<?php

ini_set('memory_limit', '6G');

$vBest10_T_Sims = array();
$vWorst10_N_Sims = array();
$vWorst10_F_Sims = array();

$vBest10_T_SimData = array();
$vWorst10_N_SimData = array();
$vWorst10_F_SimData = array();

system('mkdir ../../evaluation/plots/best_worst/');

$fhSel = fopen("../../evaluation/data/best_worst/best10_T_sel","r");

while(($buffer = fgets($fhSel, 4096)) !== false)
{
	$sSim = explode("\t",$buffer)[0];

	$vBest10_T_Sims[] = $sSim;
}

fclose($fhSel);

$fhSel = fopen("../../evaluation/data/best_worst/worst10_N_sel","r");

while(($buffer = fgets($fhSel, 4096)) !== false)
{
	$sSim = explode("\t",$buffer)[0];

	$vWorst10_N_Sims[] = $sSim;
}

fclose($fhSel);

$fhSel = fopen("../../evaluation/data/best_worst/worst10_F_sel","r");

while(($buffer = fgets($fhSel, 4096)) !== false)
{
	$sSim = explode("\t",$buffer)[0];

	$vWorst10_F_Sims[] = $sSim;
}

fclose($fhSel);

$fhSumHandle = fopen("../../evaluation/data/summary_3","r");
/*
while(($buffer = fgets($fhSumHandle, 4096)) !== false)
{
	$vData = explode("\t",$buffer);
	if(array_search($vData[0], $vBest10_T_Sims))
		$vBest10_T_SimData[] = $vData;
	elseif(array_search($vData[0], $vWorst10_N_Sims))
		$vWorst10_N_SimData[] = $vData;
	elseif(array_search($vData[0], $vWorst10_F_Sims))
		$vWorst10_F_SimData[] = $vData;

}
*/

$vSummaryData = array();

while(($buffer = fgets($fhSumHandle, 4096)) !== false)
{
	$vSim = explode("\t",$buffer);

	$iEnd = sizeof($vSim) - 1;

	$vSim[$iEnd] = str_replace(array("\n", "\t", "\r"), '', $vSim[$iEnd]);

	$vSim[$iEnd] = preg_replace('~[[:cntrl:]]~', '', $vSim[$iEnd]);

	$vSim[$iEnd] = trim($vSim[$iEnd]);

	$vSummaryData[] = $vSim;

}

fclose($fhSumHandle);


for($i = 0; $i < sizeof($vBest10_T_Sims); $i++)
{
	$iCheck = 0;
	for($j = 0; $j < sizeof($vSummaryData); $j++)
	{
		if($vSummaryData[$j][0] == $vBest10_T_Sims[$i])
		{
			$vBest10_T_SimData[] = $vSummaryData[$j];
			$iCheck = 1;
			break;
		}
	}

	if($iCheck == 0)
	{
		echo "Error: ".$vBest10_T_Sims[$i]." not found!";
		die(0);
	}
}

for($i = 0; $i < sizeof($vWorst10_N_Sims); $i++)
{
	for($j = 0; $j < sizeof($vSummaryData); $j++)
	{
		if($vSummaryData[$j][0] == $vWorst10_N_Sims[$i])
		{
			$vWorst10_N_SimData[] = $vSummaryData[$j];
		}
	}

	if($iCheck == 0)
	{
		echo "Error: ".$vWorst10_N_Sims[$i]." not found!";
		die(0);
	}
}

for($i = 0; $i < sizeof($vWorst10_F_Sims); $i++)
{
	for($j = 0; $j < sizeof($vSummaryData); $j++)
	{
		if($vSummaryData[$j][0] == $vWorst10_F_Sims[$i])
		{
			$vWorst10_F_SimData[] = $vSummaryData[$j];
		}
	}

	if($iCheck == 0)
	{
		echo "Error: ".$vWorst10_F_Sims[$i]." not found!";
		die(0);
	}
}

//print_r($vWorst10_N_SimData);


$vValueVector = array(0 => 0, 1 => 0, 2 => 0, 4 => 0, 5 => 0, 10 => 0, 20 => 0, 50 => 0, 100 => 0, 200 => 0, 500 => 0, 1000 => 0);


$vReactionText[0] = "* |ac| *  ->  * |un| *";
$vReactionText[1] = "* |ac| *  ->  * |un| *";
$vReactionText[2] = "* |me| *  ->  * |un| *";
$vReactionText[3] = "* |me| *  ->  * |un| *";
$vReactionText[4] = "ac|un| *  ->  ac|ac| *";
$vReactionText[5] = "ac|un| *  ->  ac|ac| *";
$vReactionText[6] = "* |un|ac| *  ->  * |ac|ac|";
$vReactionText[7] = "* |un|ac| *  ->  * |ac|ac|";
$vReactionText[8] = "me|un| *  ->  me|me| *";
$vReactionText[9] = "me|un| *  ->  me|me| *";
$vReactionText[10] = "* |un|me| *  ->  * |me|me|";
$vReactionText[11] = "* |un|me| *  ->  * |me|me|";
$vReactionText[12] = "ac|ac|me  ->  ac|un|me";
$vReactionText[13] = "ac|ac|me  ->  ac|un|me";
$vReactionText[14] = "me|ac|ac  ->  me|un|ac";
$vReactionText[15] = "me|ac|ac  ->  me|un|ac";
$vReactionText[16] = "me|me|ac  ->  me|un|ac";
$vReactionText[17] = "me|me|ac  ->  me|un|ac";
$vReactionText[18] = "ac|me|me  ->  ac|un|me";
$vReactionText[19] = "ac|me|me  ->  ac|un|me";
$vReactionText[20] = "ac|un|ac  ->  ac|ac|ac";
$vReactionText[21] = "ac|un|ac  ->  ac|ac|ac";
$vReactionText[22] = "me|un|me  ->  me|me|me";
$vReactionText[23] = "me|un|me  ->  me|me|me";
$vReactionText[24] = "me|ac|me  ->  me|un|me";
$vReactionText[25] = "me|ac|me  ->  me|un|me";
$vReactionText[26] = "ac|me|ac  ->  ac|un|ac";
$vReactionText[27] = "ac|me|ac  ->  ac|un|ac";


 //die(0);
for($par = 0; $par < 28; $par++)
{


	$fh2 = fopen("b10t_".$par."_di","w");

	$vDist = array();

	//$vDist = array_fill(0, 28, $vValueVector);

	$vDist = $vValueVector;

	for($i = 0; $i < sizeof($vBest10_T_SimData); $i++)
	{
		$vDist[$vBest10_T_SimData[$i][($par+11)]]++;
	}

//print_r($vDist);
//die(0);

	ksort($vDist);

	foreach($vDist as $parval => $num)
	{
		fputs($fh2,$parval."\t".$num."\n");
	}

	fclose($fh2);



	$fh2 = fopen("w10n_".$par."_di","w");

	$vDist = array();

	//$vDist = array_fill(0, 28, $vValueVector);

	$vDist = $vValueVector;

	for($i = 0; $i < sizeof($vWorst10_N_SimData); $i++)
	{
		$vDist[$vWorst10_N_SimData[$i][($par+11)]]++;
	}


	ksort($vDist);

	foreach($vDist as $parval => $num)
	{
		fputs($fh2,$parval."\t".$num."\n");
	}

	fclose($fh2);


	$fh2 = fopen("w10f_".$par."_di","w");

	$vDist = array();

	$vDist = $vValueVector;

	//$vDist = array_fill(0, 28, $vValueVector);

	for($i = 0; $i < sizeof($vWorst10_F_SimData); $i++)
	{
		$vDist[$vWorst10_F_SimData[$i][($par+11)]]++;
	}


	ksort($vDist);

	foreach($vDist as $parval => $num)
	{
		fputs($fh2,$parval."\t".$num."\n");
	}

	fclose($fh2);

	$gplt = "
set key off\n
set border 3\n

set terminal svg size 1920,1080\n
set output 'gb_".$par."_di.svg'\n

set style data histograms\n

set style fill solid noborder

set boxwidth 0.8\n

set title font ',28'\n
#set title '\shortstack{Value distribution of parameter ".($par+1)."\\Enzyme reaction: ".$vReactionText[$par]."}' offset -40,0\n

#unset key
set key right top\n
set key at 12,700\n
set key font ',28'\n
set xtics font ',20'\n
set xtics scale 0 offset '2,-10'\n
set ytics font ',20'\n
set yrange[0:700]\n
set xlabel font ',28'\n
set ylabel font ',28'\n

#set label font ',28'\n
#set label 1 at first 1, first 650 'Value distribution of parameter ".($par+1)."' front
set label font ',28'\n
set label 1 at first 1, first 680 'Enzyme reaction (".(floor($par/2))."): ".$vReactionText[$par]."' front
";

if($par % 2 == 0)
	$gplt = $gplt."set xlabel 'Association rate (12 fixed values)' offset 0,-1\nset ylabel 'Frequency (3000 simulations)' offset -3,0\n";
else
	$gplt = $gplt."set xlabel 'Dissociation rate (12 fixed values)' offset 0,-1\nset ylabel 'Frequency (3000 simulations)' offset -3,0\n";
$gplt = $gplt."
plot 'b10t_".$par."_di' using 2:xtic(1) lt rgb '#10AA10' title '10% best cases (correct modifications)', 'w10n_".$par."_di' using 2:xtic(1) lt rgb '#8B0000' title '10% worst cases (unmodified nucleosomes)', 'w10f_".$par."_di' using 2:xtic(1) lt rgb '#0000CD' title '10% worst cases (incorrect modifications)'\n
";

	$handle = fopen("gb_".$par."_di.gplt","w");
	fputs($handle, $gplt);
	fclose($handle);

	system('/usr/bin/gnuplot gb_'.$par.'_di.gplt');

	system('mv gb_'.$par.'_di.svg ../../evaluation/plots/best_worst/');

	system("rm gb_".$par."_di.gplt");
	system("rm b10t_".$par."_di");
	system("rm w10n_".$par."_di");
	system("rm w10f_".$par."_di");
}

?>
